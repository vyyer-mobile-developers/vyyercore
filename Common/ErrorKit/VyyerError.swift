//
//  Created by Антон Лобанов on 18.08.2021.
//

import Foundation

public enum VyyerError: Error, Hashable {
	case keychainValueNotFound
	case microblinkWrongResult
	case userIdNotFound
	case serverReturnedWrongScanId
	case persistentHasNoChanges
}

extension VyyerError: LocalizedError {
	public var errorDescription: String? {
		switch self {
		case .keychainValueNotFound: return "Keychain value not found"
		case .microblinkWrongResult: return "Scanner wrong result"
		case .userIdNotFound: return "UserId is not correct"
		case .serverReturnedWrongScanId: return "Server returned wrong scan id"
		case .persistentHasNoChanges: return "Persistent has no changes"
		}
	}
}
