//
//  Created by Антон Лобанов on 09.11.2021.
//

import Foundation

// swiftlint:disable nesting
public struct ServerError: Error, Hashable {
	public struct ModelDTO: Codable {
		public enum CodingKeys: String, CodingKey {
			case errors = "Errors"
		}

		let errors: [ErrorDTO]?
	}

	public struct ErrorDTO: Codable, Hashable {
		public enum CodingKeys: String, CodingKey {
			case code = "Code"
			case context = "Context"
			case description = "Description"
			case message = "Message"
		}

		/// Code of the error
		public let code: Int?
		/// Context of the error
		public let context: String?
		/// Description of the error
		public let description: String?
		/// Message within the error
		public let message: String?
	}

	public let networkError: NetworkError
	public let errors: [ErrorDTO]

	public init(networkError: NetworkError, message: String) {
		self.networkError = networkError
		self.errors = [.init(code: nil, context: nil, description: nil, message: message)]
	}
}

public extension ServerError {
	init(_ data: Data?, networkError: NetworkError) throws {
		guard let data = data else { throw networkError }
		let model = try JSONDecoder().decode(ModelDTO.self, from: data)
		guard let errors = model.errors, errors.isEmpty == false else { throw networkError }
		self.networkError = networkError
		self.errors = errors
	}
}

extension ServerError: LocalizedError {
	public var errorDescription: String? {
		self.errors.first?.message.map { "Server error: \($0)" } ?? self.networkError.errorDescription
	}
}
