//
//  Created by Антон Лобанов on 09.11.2021.
//

import Foundation
import mobile_multiplatform

public enum MobileMultiplatformError: Error, Hashable {
	case userManagementData(UserManagementDataError)
	case serverError(ServerError)
	case networkError(NetworkError)
}

extension MobileMultiplatformError: LocalizedError {
	public var errorDescription: String? {
		switch self {
		case let .userManagementData(error): return error.localizedDescription
		case let .serverError(error): return error.localizedDescription
		case let .networkError(error): return error.localizedDescription
		}
	}
}

public extension MobileMultiplatformError {
	enum UserManagementDataError: Error, Hashable {
		case topOrganizationDataNotFound
		case currentOrganizationDataNotFound
	}
}

extension MobileMultiplatformError.UserManagementDataError: LocalizedError {
	public var errorDescription: String? {
		switch self {
		case .topOrganizationDataNotFound: return "Top organization data not found"
		case .currentOrganizationDataNotFound: return "Current organization data not found"
		}
	}
}
