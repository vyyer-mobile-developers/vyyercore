//
//  Created by Антон Лобанов on 29.08.2021.
//

import Foundation

public extension Collection {
	subscript(safe index: Index) -> Element? {
		self.indices.contains(index) ? self[index] : nil
	}
}

public extension Set {
	mutating func remove(_ element: Element) {
		let _: Element? = self.remove(element)
	}

	mutating func insert(_ element: Element) {
		let _: (inserted: Bool, memberAfterInsert: Element) = self.insert(element)
	}
}

public extension MutableCollection {
	mutating func updateEach(_ update: (inout Element) -> Void) {
		for index in self.indices {
			update(&self[index])
		}
	}
}
