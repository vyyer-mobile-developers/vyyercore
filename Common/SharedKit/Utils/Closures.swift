//
//  Created by Антон Лобанов on 25.10.2021.
//

import Foundation

public typealias EmptyClosure = () -> Void
public typealias Closure<T> = (T) -> Void
