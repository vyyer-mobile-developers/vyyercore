//
//  Created by Антон Лобанов on 26.03.2021.
//

import Foundation

public final class Weak<Value> {
	public var element: Value? {
		self.value as? Value
	}

	private weak var value: AnyObject?

	public init(_ value: Value) {
		self.value = value as AnyObject
	}
}
