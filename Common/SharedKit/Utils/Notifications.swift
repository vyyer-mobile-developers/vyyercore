//
//  Created by Антон Лобанов on 25.10.2021.
//

import Foundation

/// only major notification can be put there
public enum Notifications {
	public static let userDidChangeAuth = Event<Bool>()
	public static let refreshTokenDidUpdate = Event<Void>()
}
