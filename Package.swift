// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
// swiftlint:disable prohibited_global_constants

import PackageDescription

let common: [Target.Dependency] = ["ErrorKit", "LogKit", "SharedKit"]

let package = Package(
	name: "VyyerCore",
	platforms: [
		.iOS("11.0"),
        .macOS(.v10_11),
	],
	products: [
		.library(
			name: "VyyerCore",
			type: .static,
			targets: [
				"Domain",
				"AnalyticsKit",
				"NetworkKit",
				"ErrorKit",
				"LogKit",
				"SharedKit",
			]
		),
		.library(
			name: "Domain",
			type: .static,
			targets: [
				"Domain",
			]
		),
		.library(
			name: "AnalyticsKit",
			type: .static,
			targets: [
				"AnalyticsKit",
			]
		),		
		.library(
			name: "NetworkKit",
			type: .static,
			targets: [
				"NetworkKit",
			]
		),
		.library(
			name: "ErrorKit",
			type: .static,
			targets: [
				"ErrorKit",
			]
		),
		.library(
			name: "LogKit",
			type: .static,
			targets: [
				"LogKit",
			]
		),
		.library(
			name: "SharedKit",
			type: .static,
			targets: [
				"SharedKit",
			]
		),
	],
	dependencies: [
		.package(url: "https://github.com/mxcl/PromiseKit.git", from: "6.15.3"),
		.package(name: "JWTDecode", url: "https://github.com/auth0/JWTDecode.swift.git", from: "2.6.3"),
		.package(name: "BlinkID", url: "https://github.com/BlinkID/blinkid-swift-package", .upToNextMajor(from: "5.9.0")),
		.package(url: "https://github.com/ShawnMoore/XMLParsing.git", from: "0.0.3"),
	],
	targets: [
		// MARK: - Domain

		.target(name: "Domain", dependencies: ["NetworkKit", "BlinkID", "JWTDecode", "XMLParsing", "scan_processing", "mobile_multiplatform"] + common, path: "Domain"),

		// MARK: - Platform

		.target(name: "AnalyticsKit", dependencies: common, path: "Platform/AnalyticsKit"),
		.target(name: "NetworkKit", dependencies: ["PromiseKit", "XMLParsing"] + common, path: "Platform/NetworkKit"),

		// MARK: - Common

		.target(name: "ErrorKit", dependencies: [], path: "Common/ErrorKit"),
		.target(name: "LogKit", dependencies: [], path: "Common/LogKit"),
		.target(name: "SharedKit", dependencies: [], path: "Common/SharedKit"),

		// MARK: - Kotlin

		.binaryTarget(name: "scan_processing", path: "scan_processing.xcframework"),
		.binaryTarget(name: "mobile_multiplatform", path: "mobile_multiplatform.xcframework"),
	]
)
