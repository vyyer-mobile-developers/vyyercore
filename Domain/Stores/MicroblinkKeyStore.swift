//
//  Created by Антон Лобанов on 15.10.2021.
//

import Foundation
import SharedKit

public protocol IMicroblinkKeyStore: AnyObject {
	var key: String? { get set }
	var expiresAt: Date? { get set }

	func clear()
}

public final class MicroblinkKeyStore: IMicroblinkKeyStore {
	@KeychainStore(key: "microblink_license_key") public var key: String?
	@KeychainStore(key: "microblink_license_expires_at") public var expiresAt: Date?

	public init() {}

	public func clear() {
		self.key = nil
		self.expiresAt = nil
	}
}
