//
//  Created by Антон Лобанов on 15.10.2021.
//

import Foundation
import SharedKit

public protocol IUserSessionStore: AnyObject {
	var accessToken: String? { get set }
	var refreshToken: String? { get set }
	var audienceURL: String? { get set }
	var userId: String? { get set }
	var organizationId: String? { get set }
	var organizationPermissions: OrgPermission? { get set }
	var expirationDate: Date? { get set }
	var tokenType: String? { get set }

	func clear()
}

public final class UserSessionStore: IUserSessionStore {
	public var organizationPermissions: OrgPermission? {
		get {
			self.organizationPermissionsValue.map { OrgPermission(rawValue: $0) }
		}
		set {
			self.organizationPermissionsValue = newValue?.rawValue
		}
	}

	@KeychainStore(key: "access_token") public var accessToken: String?
	@KeychainStore(key: "refresh_token") public var refreshToken: String?
	@KeychainStore(key: "audience_url") public var audienceURL: String?
	@KeychainStore(key: "user_od") public var userId: String?
	@KeychainStore(key: "organization_id") public var organizationId: String?
	@KeychainStore(key: "organization_permissions") public var organizationPermissionsValue: Int?
	@KeychainStore(key: "expiration_date") public var expirationDate: Date?
	@KeychainStore(key: "token_type") public var tokenType: String?

	public init() {}

	public func clear() {
		self.accessToken = nil
		self.refreshToken = nil
		self.audienceURL = nil
		self.userId = nil
		self.organizationId = nil
		self.organizationPermissionsValue = nil
		self.expirationDate = nil
		self.tokenType = nil
	}
}
