//
//  Created by Антон Лобанов on 15.10.2021.
//

import Foundation
import SharedKit

public protocol IFirstLaunchStore: AnyObject {
	var isLaunchedBefore: Bool? { get set }
}

public final class FirstLaunchStore: IFirstLaunchStore {
	@UserDefaultsStore(key: "isLaunchedBefore") public var isLaunchedBefore: Bool?

	public init() {}
}
