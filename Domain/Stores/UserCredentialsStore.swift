//
//  Created by Антон Лобанов on 15.10.2021.
//

import Foundation
import SharedKit

public protocol IUserCredentialsStore: AnyObject {
	var username: String? { get set }
	var password: String? { get set }

	func clear()
}

public final class UserCredentialsStore: IUserCredentialsStore {
	@KeychainStore(key: "username") public var username: String?
	@KeychainStore(key: "password") public var password: String?

	public init() {}

	public func clear() {
		self.username = nil
		self.password = nil
	}
}
