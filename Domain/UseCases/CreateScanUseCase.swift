//
//  Created by Антон Лобанов on 01.11.2021.
//

import ErrorKit
import Microblink
import mobile_multiplatform
import NetworkKit
import PromiseKit
import scan_processing
import SharedKit

public protocol ICreateScanUseCase: AnyObject {
	/// Async create scan
	func create(with result: MBIdBarcodeRecognizerResult, imageData: Data?) -> Promise<(IdentityDTO, ScanDTO, VerdictDTO?)>
	/// Sync create only result from mibroblink
	func microblinkScan(with result: MBIdBarcodeRecognizerResult) -> MicroblinkScan?
	/// Face matching request
	func checkFaceMatching(with livePhotoBase64: String, idPhotoBase64: String) -> Promise<FaceMatchingResultDTO>
}

public final class CreateScanUseCase: ICreateScanUseCase {
	private let network: INetwork
	private let deobfuscateUseCase: DeobfuscateIdentityForHistory

	public init(network: INetwork, deobfuscateUseCase: DeobfuscateIdentityForHistory) {
		self.network = network
		self.deobfuscateUseCase = deobfuscateUseCase
	}

	public func create(with result: MBIdBarcodeRecognizerResult, imageData: Data?) -> Promise<(IdentityDTO, ScanDTO, VerdictDTO?)> {
		Promise { seal in
			guard let createScanQuery = self.createScanQuery(with: result),
			      let microblinkScan = self.microblinkScan(with: result)
			else {
				seal.reject(VyyerError.microblinkWrongResult)
				return
			}
			self.network.dispatch(CreateScanRequest(query: createScanQuery))
				.done { resource in
					guard var identity = resource.identity,
					      let scan = resource.scan
					else {
						seal.reject(NetworkError.emptyData)
						return
					}

					identity = self.deobfuscateIdentity(identity, with: microblinkScan)

					if let imageData = imageData {
						self.network.dispatch(UploadScanImageRequest(data: imageData, scanID: scan.id, type: 4))
							.catch { _ in }
							.finally {
								seal.fulfill((identity, scan, resource.verdict))
							}
					}
					else {
						seal.fulfill((identity, scan, resource.verdict))
					}
				}
				.catch { seal.reject($0) }
		}
	}

	public func microblinkScan(with result: MBIdBarcodeRecognizerResult) -> MicroblinkScan? {
		guard let rawData = result.rawData else { return nil }
		let kotlinByteArray = self.kotlinByteArray(with: rawData)
		let usdlBarcode = UsdlBarcode.Companion().fromByteArray(rawData: kotlinByteArray)
		return MicroblinkScan(
			age: result.age,
			fullName: {
				if usdlBarcode.fullName == "Unknown" {
					let fullNameParts = result.fullName.components(separatedBy: ",")
					var fullName = ""
					for name in fullNameParts {
						fullName += name.capFirstLetter
						if fullNameParts.last != name {
							fullName += " "
						}
					}
					return fullName
				}
				return usdlBarcode.fullName
			}(),
			dob: (result.dateOfBirth.year, result.dateOfBirth.month, result.dateOfBirth.day),
			issue: (result.dateOfIssue.year, result.dateOfIssue.month, result.dateOfIssue.day),
			expiry: (result.dateOfExpiry.year, result.dateOfExpiry.month, result.dateOfExpiry.day),
			height: usdlBarcode.height,
			eyes: usdlBarcode.eyes,
			sex: usdlBarcode.gender,
			address: usdlBarcode.address,
			city: result.city,
			zip: result.postalCode,
			state: nil,
			fullAddress: result.address
		)
	}

	public func checkFaceMatching(with livePhotoBase64: String, idPhotoBase64: String) -> Promise<FaceMatchingResultDTO> {
		Promise { seal in
			self.network.dispatch(FaceMatchingRequest(livePhotoBase64: livePhotoBase64, idPhotoBase64: idPhotoBase64))
				.done { resource in
					seal.fulfill(resource)
				}
				.catch { seal.reject($0) }
		}
	}
}

private extension CreateScanUseCase {
	func createScanQuery(with result: MBIdBarcodeRecognizerResult) -> CreateScanQueryDTO? {
		guard let barcode = self.barcode(with: result) else { return nil }
		return CreateScanQueryDTO(validate: 511, barcodes: [barcode])
	}

	func barcode(with result: MBIdBarcodeRecognizerResult) -> BarcodeDTO? {
		guard let rawData = result.rawData else { return nil }

		let kotlinByteArray = self.kotlinByteArray(with: rawData)
		let usdlBarcode = UsdlBarcode.Companion().fromByteArray(rawData: kotlinByteArray)
		let uid = UsdlUIDCalculator().calculate(usdlBarcode: usdlBarcode)
		let obfuscatedBarcodeKByteArray = UsdlObfuscator().obfuscate(rawBarcodeDataArray: kotlinByteArray)

		var obsfucatedSwiftByteArray = [UInt8]()
		for index: Int32 in 0 ..< obfuscatedBarcodeKByteArray.size {
			let element = UInt8(bitPattern: obfuscatedBarcodeKByteArray.get(index: index))
			obsfucatedSwiftByteArray.append(element)
		}

		let obfuscatedData = Data(bytes: &obsfucatedSwiftByteArray, count: obsfucatedSwiftByteArray.count)
		let obsfucatedStringBase64 = obfuscatedData.base64EncodedString()
		let md5 = obsfucatedStringBase64.md5

		return BarcodeDTO(
			type: "PDF417",
			uid: uid,
			columns: -1,
			data: obsfucatedStringBase64,
			ecl: -1,
			md5: md5,
			orientation: 2,
			rows: -1
		)
	}

	func kotlinByteArray(with rawData: Data) -> scan_processing.KotlinByteArray {
		let swiftByteArray = [UInt8](rawData)
		let intArray = swiftByteArray.map { Int8(bitPattern: $0) }
		let kotlinByteArray = scan_processing.KotlinByteArray(size: Int32(swiftByteArray.count))
		for (index, element) in intArray.enumerated() {
			kotlinByteArray.set(index: Int32(index), value: element)
		}
		return kotlinByteArray
	}

	func deobfuscateIdentity(_ identity: IdentityDTO, with scan: MicroblinkScan) -> IdentityDTO {
		let result = self.deobfuscateUseCase.invoke(
			sourceIdentity: ApiIdentityBase(identity: identity),
			extractedInfo: CommonUsdlExtractedInfo(scan: scan)
		)
		return ApiIdentityBase(entity: result).map()
	}
}
