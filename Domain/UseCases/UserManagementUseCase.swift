//
//  Created by Антон Лобанов on 21.12.2021.
//

import ErrorKit
import Foundation
import LogKit
import mobile_multiplatform
import NetworkKit
import PromiseKit
import SharedKit

public protocol IUserManagementUseCase: AnyObject {
	func pricePerUserMonth(_ users: Int) -> Promise<Double>
	func userManagment() -> Promise<UserManagementData>
	func reassignUsers(_ users: Int, from sourceOrgId: String, to targetOrgId: String) -> Promise<Void>
}

public final class UserManagementUseCase: IUserManagementUseCase {
	private let getSubscriptionPrice: GetSubscriptionPrice
	private let getUserManagementData: GetUserManagementData
	private let reassignUsers: ReassignUsers

	public init(
		getSubscriptionPrice: GetSubscriptionPrice,
		getUserManagementData: GetUserManagementData,
		reassignUsers: ReassignUsers
	) {
		self.getSubscriptionPrice = getSubscriptionPrice
		self.getUserManagementData = getUserManagementData
		self.reassignUsers = reassignUsers
	}

	public func pricePerUserMonth(_ users: Int) -> Promise<Double> {
		Promise { seal in
			self.getSubscriptionPrice.invoke(usersQuantity: Int32(users)) { float, error in
				Log {
					"Get get subscription price"
					float
					error
				}
				if let error = error {
					seal.reject(error)
				}
				else if let value = float as? Double {
					seal.fulfill(value)
				}
				else {
					seal.reject(NetworkError.unknownError)
				}
			}
		}
	}

	public func userManagment() -> Promise<UserManagementData> {
		Promise { seal in
			self.getUserManagementData.invoke { result, error in
				Log {
					"Get user management data"
					result
					error
				}
				if let error = error {
					seal.reject(error)
				}
				else if let success = result as? mobile_multiplatform.UserManagementData.Success {
					seal.fulfill(.init(
						topOrg: .init(model: success.topOrg),
						currentOrg: .init(model: success.currentOrg),
						allOrgs: success.allOrgs.map { .init(model: $0) }
					))
				}
				else {
					seal.reject({
						if result is mobile_multiplatform.UserManagementData.ErrorCurrentOrganizationDataNotFound {
							return MobileMultiplatformError.userManagementData(.currentOrganizationDataNotFound)
						}
						else if result is mobile_multiplatform.UserManagementData.ErrorTopOrganizationDataNotFound {
							return MobileMultiplatformError.userManagementData(.topOrganizationDataNotFound)
						}
						else if let networkError = result as? mobile_multiplatform.UserManagementData.ErrorNetworkError {
							return MobileMultiplatfromNetworkProblemMapper.map(networkError.error)
						}
						else {
							return NetworkError.unknownError
						}
					}())
				}
			}
		}
	}

	public func reassignUsers(
		_ users: Int,
		from sourceOrgId: String,
		to targetOrgId: String
	) -> Promise<Void> {
		Promise { seal in
			self.reassignUsers.invoke(
				sourceOrgId: sourceOrgId,
				targetOrgId: targetOrgId,
				usersToReassign: Int32(users)
			) { result, error in
				Log {
					"Reassign users"
					result
					error
				}
				if let error = error {
					seal.reject(error)
				}
				else if result is mobile_multiplatform.ReassignmentResult.Success {
					seal.fulfill(())
				}
				else {
					seal.reject({
						if let networkError = result as? mobile_multiplatform.ReassignmentResult.ErrorNetworkError {
							return MobileMultiplatfromNetworkProblemMapper.map(networkError.error)
						}
						else {
							return NetworkError.unknownError
						}
					}())
				}
			}
		}
	}
}
