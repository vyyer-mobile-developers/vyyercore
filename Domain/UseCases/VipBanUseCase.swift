//
//  Created by Антон Лобанов on 18.10.2021.
//

import ErrorKit
import Foundation
import LogKit
import NetworkKit
import PromiseKit
import SharedKit

public protocol IVipBanUseCase: AnyObject {
	var hasPermission: Bool { get }

	func isIdentityHasBan(_ identity: IdentityDTO) -> Bool
	func isIdentityHasVip(_ identity: IdentityDTO) -> Bool
	func updateVip(for identity: IdentityDTO, startDate: Date?, endDate: Date?) -> Promise<IdentityDTO>
	func updateBan(for identity: IdentityDTO, startDate: Date?, endDate: Date?) -> Promise<IdentityDTO>
}

public final class VipBanUseCase {
	private let network: INetwork
	private let userSessionStore: IUserSessionStore

	public init(
		network: INetwork,
		userSessionStore: IUserSessionStore
	) {
		self.network = network
		self.userSessionStore = userSessionStore
	}
}

extension VipBanUseCase: IVipBanUseCase {
	public var hasPermission: Bool {
		self.userSessionStore.organizationPermissions?.contains(.historyVipBan) == true
	}

	public func isIdentityHasBan(_ identity: IdentityDTO) -> Bool {
		let now = Date()
		guard let ban = identity.ban,
		      let startAt = identity.banStartAt,
		      let endAt = identity.banEndAt,
		      let banStartAt = DateFormatter.lastTimeScanned.date(from: startAt),
		      let banEndAt = DateFormatter.lastTimeScanned.date(from: endAt)
		else {
			return false
		}
		return ban == 1 || (banStartAt <= now && now <= banEndAt)
	}

	public func isIdentityHasVip(_ identity: IdentityDTO) -> Bool {
		let now = Date()
		guard let vip = identity.vip,
		      let startAt = identity.vipStartAt,
		      let endAt = identity.vipEndAt,
		      let banStartAt = DateFormatter.lastTimeScanned.date(from: startAt),
		      let banEndAt = DateFormatter.lastTimeScanned.date(from: endAt)
		else {
			return false
		}
		return vip == 1 || (banStartAt <= now && now <= banEndAt)
	}

	public func updateVip(for identity: IdentityDTO, startDate: Date?, endDate: Date?) -> Promise<IdentityDTO> {
		Promise { seal in
			guard let userId = self.userSessionStore.userId else {
				seal.reject(VyyerError.userIdNotFound)
				return
			}

			var identity = identity

			if let startDate = startDate, let endDate = endDate {
				identity.vip = 1
				identity.vipStartAt = DateFormatter.lastTimeScanned.string(from: startDate)
				identity.vipEndAt = DateFormatter.lastTimeScanned.string(from: endDate)
			}
			else {
				identity.vip = 0
				identity.vipStartAt = DateFormatter.lastTimeScanned.string(from: Date().addingTimeInterval(-1))
				identity.vipEndAt = DateFormatter.lastTimeScanned.string(from: Date().addingTimeInterval(-1))
			}

			identity.vipBy = userId

			self.network.dispatch(UpdateIdentityRequest(query: .init(identity: identity)))
				.done { seal.fulfill(.init(resource: $0)) }
				.catch { seal.reject($0) }
		}
	}

	public func updateBan(for identity: IdentityDTO, startDate: Date?, endDate: Date?) -> Promise<IdentityDTO> {
		Promise { seal in
			guard let userId = self.userSessionStore.userId else {
				seal.reject(VyyerError.userIdNotFound)
				return
			}

			var identity = identity

			if let startDate = startDate, let endDate = endDate {
				identity.ban = 1
				identity.banStartAt = DateFormatter.lastTimeScanned.string(from: startDate)
				identity.banEndAt = DateFormatter.lastTimeScanned.string(from: endDate)
			}
			else {
				identity.ban = 0
				identity.banStartAt = DateFormatter.lastTimeScanned.string(from: Date().addingTimeInterval(-1))
				identity.banEndAt = DateFormatter.lastTimeScanned.string(from: Date().addingTimeInterval(-1))
			}

			identity.bannedBy = userId

			self.network.dispatch(UpdateIdentityRequest(query: .init(identity: identity)))
				.done { seal.fulfill(.init(resource: $0)) }
				.catch { seal.reject($0) }
		}
	}
}
