//
//  Created by Антон Лобанов on 01.11.2021.
//

import Foundation
import LogKit
import Microblink
import NetworkKit
import PromiseKit
import SharedKit

public protocol IMicroblinkKeyUseCase: AnyObject {
	var key: String? { get }
	var isValid: Bool { get }

	func install() -> Promise<Void>
	func refresh() -> Promise<GetLicenseResourceDTO>
	func invalidate()
}

public final class MicroblinkKeyUseCase {
	private let network: INetwork
	private let microblinkKeyStore: IMicroblinkKeyStore

	public init(
		network: INetwork,
		microblinkKeyStore: IMicroblinkKeyStore
	) {
		self.network = network
		self.microblinkKeyStore = microblinkKeyStore
	}
}

extension MicroblinkKeyUseCase: IMicroblinkKeyUseCase {
	public var isValid: Bool {
		true
	}

	public var key: String? {
		self.microblinkKeyStore.key
	}

	public func install() -> Promise<Void> {
		Promise { seal in
			let completion: Closure<String> = {
				MBMicroblinkSDK.shared().setLicenseKey($0) { error in
					Log(.error) {
						"Error microblink set license key"
						error
					}
					self.invalidate()
				}
				seal.fulfill(())
			}

			if self.isValid, let key = self.key {
				completion(key)
				return
			}

			self.refresh()
				.done { completion($0.value) }
				.catch { seal.reject($0) }
		}
	}

	public func refresh() -> Promise<GetLicenseResourceDTO> {
		self.network.dispatch(LicenseRequest())
			.get { resource in
				Log {
					"Set microblink license"
					resource
				}
				self.microblinkKeyStore.key = resource.value.isEmpty ? nil : resource.value
				self.microblinkKeyStore.expiresAt = VyyerUtil.lastTimeFromString(resource.expiresAt)
			}
	}

	public func invalidate() {
		self.microblinkKeyStore.key = nil
		self.microblinkKeyStore.expiresAt = nil
	}
}
