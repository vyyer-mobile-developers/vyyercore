//
//  Created by Антон Лобанов on 14.10.2021.
//

import ErrorKit
import Foundation
import JWTDecode
import mobile_multiplatform
import NetworkKit
import PromiseKit
import SharedKit

public protocol IAuthUseCase {
	var isUserLogged: Bool { get }

	func validate()
	func login(username: String, password: String) -> Promise<Void>
	func refreshToken() -> Promise<Void>
	func logout()
}

public struct AuthUseCase: IAuthUseCase {
	private struct JWTData {
		let audienceURL: String
		let userId: String
		let organizationId: String
		let organizationPermissions: String?
	}

	public var isUserLogged: Bool {
		self.userSessionStore.accessToken != nil
	}

	private let network: Network
	private let clientID: String
	private let userSessionStore: IUserSessionStore
	private let userCredentialsStore: IUserCredentialsStore
	private let microblinkKeyStore: IMicroblinkKeyStore
	private let tokenRefresh: TokenRefresh

	public init(
		network: Network,
		configuration: Configuration,
		userSessionStore: IUserSessionStore,
		userCredentialsStore: IUserCredentialsStore,
		microblinkKeyStore: IMicroblinkKeyStore,
		tokenRefresh: TokenRefresh
	) {
		self.network = network
		self.clientID = configuration.clientID
		self.userSessionStore = userSessionStore
		self.userCredentialsStore = userCredentialsStore
		self.microblinkKeyStore = microblinkKeyStore
		self.tokenRefresh = tokenRefresh
	}

	public func validate() {
		let expirationDate = self.userSessionStore.expirationDate ?? Date().addingTimeInterval(-10)
		guard expirationDate < Date() else { return }
		self.logout()
	}

	// swiftlint:disable function_body_length
	public func login(username: String, password: String) -> Promise<Void> {
		Promise { seal in
			let auth0TokenQuery = Auth0TokenRequest.Query(
				grantType: "password",
				username: username,
				password: password,
				clientId: self.clientID,
				scope: "openid offline_access"
			)
			self.network.dispatch(Auth0TokenRequest(auth0TokenQuery))
				.map { token throws -> JWTData in
					guard let jwt = try? decode(jwt: token.idToken),
					      let audienceURL = jwt.claim(name: "http://custom/api_url").string,
					      let organizationId = jwt.claim(name: "http://custom/organization_id").string,
					      let userId = jwt.claim(name: "http://custom/user_id").string
					else {
						throw NetworkError.invalidJWT
					}
					let organizationPermissions = jwt.claim(name: "http://custom/organization_permissions").string
					return JWTData(
						audienceURL: audienceURL,
						userId: userId,
						organizationId: organizationId,
						organizationPermissions: organizationPermissions
					)
				}
				.then { jwtData in
					self.network.dispatch(Auth0TokenRequest(.init(
						grantType: auth0TokenQuery.grantType,
						username: auth0TokenQuery.username,
						password: auth0TokenQuery.password,
						clientId: auth0TokenQuery.clientId,
						scope: auth0TokenQuery.scope,
						audience: jwtData.audienceURL
					))).map { ($0, jwtData) }
				}
				.done { token, jwtData in
					// save credentials
					self.userCredentialsStore.username = username
					self.userCredentialsStore.password = password
					// save session
					self.userSessionStore.accessToken = token.accessToken
					self.userSessionStore.refreshToken = token.refreshToken
					self.userSessionStore.tokenType = token.tokenType
					self.userSessionStore.expirationDate = Date(timeIntervalSinceNow: TimeInterval(token.expiresIn))
					self.userSessionStore.audienceURL = jwtData.audienceURL
					self.userSessionStore.userId = jwtData.userId
					self.userSessionStore.organizationId = jwtData.organizationId
					self.userSessionStore.organizationPermissions = jwtData.organizationPermissions.map { OrgPermission(string: $0) }
					// finish
					seal.fulfill(())
					// Notify
					Notifications.userDidChangeAuth.invoke(with: true)
				}
				.catch {
					seal.reject($0)
				}
		}
	}

	public func refreshToken() -> Promise<Void> {
		guard let audience = self.userSessionStore.audienceURL,
		      let refreshToken = self.userSessionStore.refreshToken
		else {
			return .init(error: NetworkError.badRequest)
		}

		let tokenRefreshData = TokenRefreshData(
			clientId: self.clientID,
			audience: audience,
			refreshToken: refreshToken
		)

		return Promise { seal in
			self.tokenRefresh.invoke(refreshData: tokenRefreshData) { result, error in
				if let error = error {
					seal.reject(error)
					return
				}

				guard let result = result else {
					seal.reject(NetworkError.emptyData)
					return
				}

				self.userSessionStore.accessToken = result.accessToken

				seal.fulfill(())

				Notifications.refreshTokenDidUpdate.invoke()
			}
		}
	}

	public func logout() {
		self.userCredentialsStore.clear()
		self.userSessionStore.clear()
		self.microblinkKeyStore.clear()
		Notifications.userDidChangeAuth.invoke(with: false)
	}
}
