//
//  Created by Антон Лобанов on 26.02.2021.
//

import Foundation

@propertyWrapper
public struct UserDefaultsStore<T: Codable> {
	private let key: String
	private let defaultValue: T?

	public init(key: String, defaultValue: T? = nil) {
		self.key = key
		self.defaultValue = defaultValue
	}

	public var wrappedValue: T? {
		get {
			if let data = UserDefaults.standard.object(forKey: key) as? Data,
			   let value = try? JSONDecoder().decode(T.self, from: data)
			{
				return value
			}
			return self.defaultValue
		}
		set {
			if let encoded = try? JSONEncoder().encode(newValue) {
				UserDefaults.standard.set(encoded, forKey: self.key)
			}
			else {
				UserDefaults.standard.removeObject(forKey: self.key)
			}
		}
	}
}
