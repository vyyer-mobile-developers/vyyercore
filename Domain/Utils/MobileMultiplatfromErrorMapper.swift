//
//  Created by Антон Лобанов on 03.01.2022.
//

import ErrorKit
import Foundation
import mobile_multiplatform

enum MobileMultiplatfromNetworkProblemMapper {
	static func map(_ problem: mobile_multiplatform.NetworkProblem) -> MobileMultiplatformError {
		if problem is mobile_multiplatform.NetworkProblem.ConnectionProblem {
			return .networkError(.urlSessionFailed(URLError(.networkConnectionLost)))
		}
		else if problem is mobile_multiplatform.NetworkProblem.Unauthorized {
			return .networkError(.unauthorized)
		}
		else if problem is mobile_multiplatform.NetworkProblem.Forbidden {
			return .networkError(.forbidden)
		}
		else if let error = problem as? mobile_multiplatform.NetworkProblem.OtherProblem {
			return .serverError(.init(
				networkError: .init(code: Int(error.httpCode)),
				message: error.message
			))
		}
		else {
			return .networkError(.unknownError)
		}
	}
}
