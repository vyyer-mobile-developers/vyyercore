//
// Generated from Swagger
//

import Foundation

public struct BarcodeDataEntryDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case type = "Type"
		case name = "Name"
		case value = "Value"
	}

	/// Type of barcode data entry
	public var type: String
	/// Name of barcode data entry
	public var name: String
	/// Value of barcode data entry
	public var value: String?

	init(
		type: String,
		name: String,
		value: String?
	) {
		self.type = type
		self.name = name
		self.value = value
	}
}
