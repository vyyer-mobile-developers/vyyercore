//
// Generated from Swagger
//

import Foundation

public struct VerdictDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case id = "ID"
		case type = "Type"
		case name = "Name"
		case order = "Order"
		case result = "Result"
		case value = "Value"
	}

	/// Verdict ID
	public var id: Int
	/// Verdict type (valid, caution, invalid, or fake) in * verdict constants
	public var type: Int
	/// Verdict name
	public var name: String
	/// Order of magnitude amongst types
	public var order: Int
	/// Verdict final result (valid OR caution OR invalid OR * fake)
	public var result: Int?
	/// Verdict value
	public var value: String

	init(
		id: Int,
		type: Int,
		name: String,
		order: Int,
		result: Int?,
		value: String
	) {
		self.id = id
		self.type = type
		self.name = name
		self.order = order
		self.result = result
		self.value = value
	}
}
