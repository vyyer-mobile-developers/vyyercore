//
// Generated from Swagger
//

import Foundation

public struct CreateScanQueryDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case validate = "Validate"
		case barcodes = "Barcodes"
	}

	/// Flag to return validation information about the * data
	public var validate: Int?
	/// List of * barcodes to extract data from
	public var barcodes: [BarcodeDTO]?

	init(
		validate: Int?,
		barcodes: [BarcodeDTO]?
	) {
		self.validate = validate
		self.barcodes = barcodes
	}
}
