//
// Generated from Swagger
//

import Foundation

public struct ValidateResultEntryDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case id = "ID"
		case type = "Type"
		case valid = "Valid"
		case certainty = "Certainty"
		case value = "Value"
	}

	/// Validation entry result unique identifier
	public var id: Int
	/// Type of validation result entry
	public var type: String
	/// Is validation successful or not
	public var valid: Int
	/// Certainty of the validation result
	public var certainty: Double?
	/// Validation value that was being checked
	public var value: String?

	init(
		id: Int,
		type: String,
		valid: Int,
		certainty: Double?,
		value: String?
	) {
		self.id = id
		self.type = type
		self.valid = valid
		self.certainty = certainty
		self.value = value
	}
}
