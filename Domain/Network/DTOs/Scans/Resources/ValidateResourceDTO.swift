//
// Generated from Swagger
//

import Foundation

public struct ValidateResourceDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case flags = "Flags"
		case results = "Results"
		case errors = "Errors"
	}

	/// Flags for validation and identity
	public var flags: Int?
	/// A list of validation results for each data entry
	public var results: [ValidateResultEntryDTO]?
	/// List of * errors for the validation request
	public var errors: [ServerErrorDTO]?

	init(
		flags: Int?,
		results: [ValidateResultEntryDTO]?,
		errors: [ServerErrorDTO]?
	) {
		self.flags = flags
		self.results = results
		self.errors = errors
	}
}
