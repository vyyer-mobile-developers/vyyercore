//
// Generated from Swagger
//

import Foundation

public struct CreateScanResourceDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case identity = "Identity"
		case validateResource = "ValidateRS"
		case extractResource = "ExtractRS"
		case scan = "Scan"
		case verdict = "Verdict"
		case verdicts = "Verdicts"
		case errors = "Errors"
	}

	public var identity: IdentityDTO?
	public var validateResource: ValidateResourceDTO?
	public var extractResource: ExtractResourceDTO?
	public var scan: ScanDTO?
	public var verdict: VerdictDTO?
	/// Verdict(s) information
	public var verdicts: [VerdictDTO]?
	/// List of * errors for the scan request
	public var errors: [ServerErrorDTO]?

	init(
		identity: IdentityDTO?,
		validateResource: ValidateResourceDTO?,
		extractResource: ExtractResourceDTO?,
		scan: ScanDTO?,
		verdict: VerdictDTO?,
		verdicts: [VerdictDTO]?,
		errors: [ServerErrorDTO]?
	) {
		self.identity = identity
		self.validateResource = validateResource
		self.extractResource = extractResource
		self.scan = scan
		self.verdict = verdict
		self.verdicts = verdicts
		self.errors = errors
	}
}
