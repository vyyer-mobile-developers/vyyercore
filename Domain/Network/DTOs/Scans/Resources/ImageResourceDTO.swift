//
// Generated from Swagger
//

import Foundation

public struct ImageResourceDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case filename = "Filename"
		case errors = "Errors"
	}

	/// Filename of saved image
	public var filename: String?
	/// A list of * validation errors for the request
	public var errors: [ServerErrorDTO]?

	init(
		filename: String?,
		errors: [ServerErrorDTO]?
	) {
		self.filename = filename
		self.errors = errors
	}
}
