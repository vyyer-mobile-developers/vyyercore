//
// Generated from Swagger
//

import Foundation

public struct ExtractResourceDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case identityId = "IdentityID"
		case scanId = "ScanID"
		case barcodes = "Barcodes"
		case errors = "Errors"
	}

	/// An identity identifier deduced from the barcode * data (new or existing)
	public var identityId: Int
	/// Scan unique identifier
	public var scanId: Int?
	/// A * list of data extracted from barcodes
	public var barcodes: [BarcodeResultDTO]?
	/// List of * errors for the extraction request
	public var errors: [ServerErrorDTO]?

	init(
		identityId: Int,
		scanId: Int?,
		barcodes: [BarcodeResultDTO]?,
		errors: [ServerErrorDTO]?
	) {
		self.identityId = identityId
		self.scanId = scanId
		self.barcodes = barcodes
		self.errors = errors
	}
}
