//
//  FaceMatchingResultDTO.swift
//
//
//  Created by Daulet Tungatarov on 25.05.2022.
//

import Foundation

// swiftlint:disable identifier_name
public struct FaceMatchingResultDTO: Codable {
	public struct Status: Codable {
		public let Status_Message: String?
	}

	public struct FormDetails: Codable {
		public let ProductId: Int?
		public let State: String?
		public let Identity_Validation_and_Face_Matching: Identity?

		public struct Identity: Codable {
			public struct ImageProcessing: Codable {
				public struct CustomerPhotoProcessing: Codable {
					public let FaceVerificationStatus: String?
					public let FaceMatchingScore: Double?
				}

				public let Customer_Photo_Processing: CustomerPhotoProcessing?
			}

			public let IDmission_Image_Processing: ImageProcessing?
		}
	}

	public let Status: Status?
	public let FormDetails: FormDetails?

	public var livenessScore: Float?
}
