//
// Generated from Swagger
//

import Foundation

public struct BarcodeResultDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case type = "Type"
		case uid = "UID"
		case data = "Data"
		case md5 = "MD5"
		case errors = "Errors"
	}

	/// Type of barcode
	public var type: String
	/// Identity unique identifier
	public var uid: String?
	/// A * list of data entries for the barcode
	public var data: [BarcodeDataEntryDTO]?
	/// MD5 string for encoded data
	public var md5: String
	/// List of * errors for the data extraction request
	public var errors: [ServerErrorDTO]?

	init(
		type: String,
		uid: String?,
		data: [BarcodeDataEntryDTO]?,
		md5: String,
		errors: [ServerErrorDTO]?
	) {
		self.type = type
		self.uid = uid
		self.data = data
		self.md5 = md5
		self.errors = errors
	}
}
