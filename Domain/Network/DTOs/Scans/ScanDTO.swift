//
// Generated from Swagger
//

import Foundation

public struct ScanDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case id = "ID"
		case identityId = "IdentityID"
		case userId = "UserID"
		case verdictType = "VerdictType"
		case ban = "Ban"
		case createdAt = "CreatedAt"
		case flags = "Flags"
		case verdictName = "VerdictName"
		case verdictResult = "VerdictResult"
		case verdictValue = "VerdictValue"
		case vip = "VIP"
	}

	/// Scan unique identifier
	public var id: Int64
	/// Scan identity identifier
	public var identityId: Int64?
	/// User unique identifier
	public var userId: String
	/// Scan final verdict type
	public var verdictType: Int?
	/// Identity banned status flag at time of scan
	public var ban: Int?
	/// Scanned at in date/time format
	public var createdAt: String
	/// Flags for validation and identity
	public var flags: Int?
	/// Scan final verdict name
	public var verdictName: String?
	/// Scan final verdict result
	public var verdictResult: Int?
	/// Scan final verdict value
	public var verdictValue: String?
	/// Identity VIP status flag at time of scan
	public var vip: Int?

	public init(
		id: Int64,
		identityId: Int64?,
		userId: String,
		verdictType: Int?,
		ban: Int?,
		createdAt: String,
		flags: Int?,
		verdictName: String?,
		verdictResult: Int?,
		verdictValue: String?,
		vip: Int?
	) {
		self.id = id
		self.identityId = identityId
		self.userId = userId
		self.verdictType = verdictType
		self.ban = ban
		self.createdAt = createdAt
		self.flags = flags
		self.verdictName = verdictName
		self.verdictResult = verdictResult
		self.verdictValue = verdictValue
		self.vip = vip
	}
}
