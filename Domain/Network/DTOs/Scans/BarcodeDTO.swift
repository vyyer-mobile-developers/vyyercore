//
// Generated from Swagger
//

import Foundation

public struct BarcodeDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case type = "Type"
		case uid = "UID"
		case columns = "Columns"
		case data = "Data"
		case ecl = "ECL"
		case md5 = "MD5"
		case orientation = "Orientation"
		case rows = "Rows"
	}

	/// Type of barcode
	public var type: String
	/// Identity unique identifier
	public var uid: String
	/// Number of columns in the barcode
	public var columns: Int?
	/// Base64 encoded data of the barcode
	public var data: String
	/// Error correction level in the barcode
	public var ecl: Int?
	/// MD5 string for encoded data
	public var md5: String
	/// Orientation of the ID (0 - horizontal, 1 - * vertical, 2 - uncertain)
	public var orientation: Int
	/// Number of rows in the barcode
	public var rows: Int?

	init(
		type: String,
		uid: String,
		columns: Int?,
		data: String,
		ecl: Int?,
		md5: String,
		orientation: Int,
		rows: Int?
	) {
		self.type = type
		self.uid = uid
		self.columns = columns
		self.data = data
		self.ecl = ecl
		self.md5 = md5
		self.orientation = orientation
		self.rows = rows
	}
}
