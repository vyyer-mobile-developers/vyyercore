//
// Generated from Swagger
//

import Foundation

public struct UpdateIdentityQueryDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case id = "ID"
		case middleName = "MiddleName"
		case orgId = "OrgID"
		case remoteId = "RemoteID"
		case uid = "UID"
		case userId = "UserID"
		case address = "Address"
		case ban = "Ban"
		case banEndAt = "BanEndAt"
		case banStartAt = "BanStartAt"
		case bannedBy = "BannedBy"
		case birthday = "Birthday"
		case city = "City"
		case createdAt = "CreatedAt"
		case date = "Date"
		case expiresAt = "ExpiresAt"
		case eyeColor = "EyeColor"
		case firstName = "FirstName"
		case fullName = "FullName"
		case gender = "Gender"
		case hairColor = "HairColor"
		case height = "Height"
		case issuedAt = "IssuedAt"
		case lastName = "LastName"
		case lastScannedAt = "LastScannedAt"
		case licenseNumber = "LicenseNumber"
		case orientation = "Orientation"
		case postalCode = "PostalCode"
		case scansInPeriod = "ScansInPeriod"
		case state = "State"
		case status = "Status"
		case street = "Street"
		case vip = "VIP"
		case vipBy = "VIPBy"
		case vipEndAt = "VIPEndAt"
		case vipStartAt = "VIPStartAt"
		case visits = "Visits"
		case weight = "Weight"
	}

	/// Identity unique identifier
	public var id: Int64
	/// Identity middle name
	public var middleName: String?
	/// Organization (owner) identifier
	public var orgId: String?
	/// Identity remote unique identifier
	public var remoteId: Int?
	/// Identity UID identifier
	public var uid: String
	/// User (owner) identifier
	public var userId: String
	/// Full address
	public var address: String?
	/// Identity banned status flag
	public var ban: Int?
	/// Ban end date
	public var banEndAt: String?
	/// Banned start date
	public var banStartAt: String?
	/// Banned user identifier
	public var bannedBy: String?
	/// Date of birth in date/time format
	public var birthday: String?
	/// City address
	public var city: String?
	/// Identity created at in date/time format
	public var createdAt: String?
	/// Date of change in date/time format
	public var date: String?
	/// Date of expiry in date/time format
	public var expiresAt: String?
	/// Eye color
	public var eyeColor: String?
	/// Identity first name
	public var firstName: String?
	/// Identity full name
	public var fullName: String?
	/// Identity gender
	public var gender: String?
	/// Hair color
	public var hairColor: String?
	/// Height
	public var height: String?
	/// Date of issue in date/time format
	public var issuedAt: String?
	/// Identity last name
	public var lastName: String?
	/// Last date & time the identity was scanned
	public var lastScannedAt: String?
	/// License number
	public var licenseNumber: String?
	/// Orientation of the ID (0 - horizontal, 1 - vertical)
	public var orientation: Int?
	/// Postal code address
	public var postalCode: String?
	/// Number of scans identity was scanned within * period
	public var scansInPeriod: Int?
	/// Country ISO code
	public var state: String?
	/// Push resulting status
	public var status: Int?
	/// Street address
	public var street: String?
	/// Identity VIP status flag
	public var vip: Int?
	/// VIP user identifier
	public var vipBy: String?
	/// VIP end date
	public var vipEndAt: String?
	/// Vip start date
	public var vipStartAt: String?
	/// Number of visits
	public var visits: Int?
	/// Weight
	public var weight: String?

	init(
		id: Int64,
		middleName: String?,
		orgId: String?,
		remoteId: Int?,
		uid: String,
		userId: String,
		address: String?,
		ban: Int?,
		banEndAt: String?,
		banStartAt: String?,
		bannedBy: String?,
		birthday: String?,
		city: String?,
		createdAt: String?,
		date: String?,
		expiresAt: String?,
		eyeColor: String?,
		firstName: String?,
		fullName: String?,
		gender: String?,
		hairColor: String?,
		height: String?,
		issuedAt: String?,
		lastName: String?,
		lastScannedAt: String?,
		licenseNumber: String?,
		orientation: Int?,
		postalCode: String?,
		scansInPeriod: Int?,
		state: String?,
		status: Int?,
		street: String?,
		vip: Int?,
		vipBy: String?,
		vipEndAt: String?,
		vipStartAt: String?,
		visits: Int?,
		weight: String?
	) {
		self.id = id
		self.middleName = middleName
		self.orgId = orgId
		self.remoteId = remoteId
		self.uid = uid
		self.userId = userId
		self.address = address
		self.ban = ban
		self.banEndAt = banEndAt
		self.banStartAt = banStartAt
		self.bannedBy = bannedBy
		self.birthday = birthday
		self.city = city
		self.createdAt = createdAt
		self.date = date
		self.expiresAt = expiresAt
		self.eyeColor = eyeColor
		self.firstName = firstName
		self.fullName = fullName
		self.gender = gender
		self.hairColor = hairColor
		self.height = height
		self.issuedAt = issuedAt
		self.lastName = lastName
		self.lastScannedAt = lastScannedAt
		self.licenseNumber = licenseNumber
		self.orientation = orientation
		self.postalCode = postalCode
		self.scansInPeriod = scansInPeriod
		self.state = state
		self.status = status
		self.street = street
		self.vip = vip
		self.vipBy = vipBy
		self.vipEndAt = vipEndAt
		self.vipStartAt = vipStartAt
		self.visits = visits
		self.weight = weight
	}
}
