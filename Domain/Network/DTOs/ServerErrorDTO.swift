//
// Generated from Swagger
//

import Foundation

public struct ServerErrorDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case code = "Code"
		case context = "Context"
		case description = "Description"
		case message = "Message"
	}

	/// Code of the error
	public var code: Int?
	/// Context of the error
	public var context: String?
	/// Description of the error
	public var description: String?
	/// Message within the error
	public var message: String?

	init(
		code: Int?,
		context: String?,
		description: String?,
		message: String?
	) {
		self.code = code
		self.context = context
		self.description = description
		self.message = message
	}
}
