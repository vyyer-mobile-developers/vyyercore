//
// Generated from Swagger
//

import Foundation

public struct GetLicenseResourceDTO: Codable {
	public enum CodingKeys: String, CodingKey {
		case id = "ID"
		case expiresAt = "ExpiresAt"
		case name = "Name"
		case value = "Value"
		case errors = "Errors"
	}

	/// License unique identifier
	public var id: Int
	/// Date of expiry in date/time format
	public var expiresAt: String
	/// License short name
	public var name: String
	/// License value
	public var value: String
	/// List of * errors for the license retrieval request
	public var errors: [ServerErrorDTO]?

	init(
		id: Int,
		expiresAt: String,
		name: String,
		value: String,
		errors: [ServerErrorDTO]?
	) {
		self.id = id
		self.expiresAt = expiresAt
		self.name = name
		self.value = value
		self.errors = errors
	}
}
