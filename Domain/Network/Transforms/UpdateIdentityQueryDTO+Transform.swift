//
//  Created by Антон Лобанов on 09.11.2021.
//

import Foundation

extension UpdateIdentityQueryDTO {
	init(identity: IdentityDTO) {
		self.id = identity.id
		self.middleName = identity.middleName
		self.orgId = identity.orgId
		self.remoteId = nil
		self.uid = identity.uid
		self.userId = identity.userId
		self.address = identity.address
		self.ban = identity.ban
		self.banEndAt = identity.banEndAt
		self.banStartAt = identity.banStartAt
		self.bannedBy = identity.bannedBy
		self.birthday = identity.birthday
		self.city = identity.city
		self.createdAt = identity.createdAt
		self.date = DateFormatter.lastTimeScanned.string(from: Date())
		self.expiresAt = identity.expiresAt
		self.eyeColor = identity.eyeColor
		self.firstName = identity.firstName
		self.fullName = identity.fullName
		self.gender = identity.gender
		self.hairColor = identity.hairColor
		self.height = identity.height
		self.issuedAt = identity.issuedAt
		self.lastName = identity.lastName
		self.lastScannedAt = identity.lastScannedAt
		self.licenseNumber = identity.licenseNumber
		self.orientation = identity.orientation
		self.postalCode = identity.postalCode
		self.scansInPeriod = identity.scansInPeriod
		self.state = identity.state
		self.status = nil
		self.street = identity.street
		self.vip = identity.vip
		self.vipBy = identity.vipBy
		self.vipEndAt = identity.vipEndAt
		self.vipStartAt = identity.vipStartAt
		self.visits = identity.visits
		self.weight = identity.weight
	}
}
