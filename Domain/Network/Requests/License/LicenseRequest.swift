//
//  Created by Антон Лобанов on 14.10.2021.
//

import Foundation
import NetworkKit

public struct LicenseRequest: Request {
	public var api: RequestAPI {
		.internal
	}

	public init() {}

	public func parameters() -> RequestParameters<GetLicenseResourceDTO> {
		.init(
			path: "/api/v2/licenses/get/iOS",
			method: .get,
			contentType: .json,
			body: nil,
			headers: nil
		)
	}
}
