//
//  Created by Антон Лобанов on 14.10.2021.
//

import Foundation
import NetworkKit

public struct CreateScanRequest: Request {
	public var api: RequestAPI {
		.internal
	}

	private let query: CreateScanQueryDTO

	public init(
		query: CreateScanQueryDTO
	) {
		self.query = query
	}

	public func parameters() -> RequestParameters<CreateScanResourceDTO> {
		.init(
			path: "/api/v2/scans/create",
			method: .post,
			contentType: .json,
			body: self.query.asDictionary,
			headers: nil
		)
	}
}
