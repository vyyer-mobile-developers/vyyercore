//
//  FaceMatchingRequest.swift
//  Vyyer
//
//  Created by Daulet Tungatarov on 24.05.2022.
//  Copyright © 2022 Антон Лобанов. All rights reserved.
//

import Foundation
import NetworkKit

public struct FaceMatchingRequest: Request {
	public var api: RequestAPI { .idMission }

	private let xmlBody: String

	public init(livePhotoBase64: String, idPhotoBase64: String) {
		let formId = Int.random(in: 1000 ..< 100_000_000)

		self.xmlBody = """
		        <?xml version="1.0" encoding="UTF-8"?>
		        <ThirdPartyFormUpdateRQ>
		         <Security_Data>
		         <Unique_Data>
		         <Login_Data>
		         <Login_Id>ev_integ_36595</Login_Id>
		         <Application_Code>IDMAXWEB</Application_Code>
		         </Login_Data>
		         </Unique_Data>
		         <Password>IDmi#595$</Password>
		         <Merchant_Id>16066</Merchant_Id>
		         </Security_Data>
		        <FormDetails>
		         <FormKey>\(formId)</FormKey>
		         <ProductId>4130</ProductId>
		         <FormId></FormId>
		         <Identity_Validation_and_Face_Matching>
		         <IDmission_Image_Processing>
		         <ID_Image_Processing>
		         <ID_Image_Front>\(idPhotoBase64)</ID_Image_Front>
		         </ID_Image_Processing>
		         <Customer_Photo_Processing>
		         <Live_Customer_Photo>\(livePhotoBase64)</Live_Customer_Photo>
		         </Customer_Photo_Processing>
		        <Additional_Data>
		         <Service_ID>65</Service_ID>
		         <Need_Immediate_Response>N</Need_Immediate_Response>
		         </Additional_Data>
		         </IDmission_Image_Processing>
		         </Identity_Validation_and_Face_Matching>
		         <Transition>
		         <Name>Draft</Name>
		         </Transition>
		         <SearchName>IDS_FINAL_SUBMIT</SearchName>
		         </FormDetails>
		        </ThirdPartyFormUpdateRQ>
		"""
	}

	public func parameters() -> RequestParameters<FaceMatchingResultDTO> {
		.init(
			path: "/IDS/service/integ/idm/thirdparty/upsert",
			method: .post,
			contentType: .xml,
			body: nil,
			xmlBody: self.xmlBody,
			headers: nil
		)
	}
}
