//
//  Created by Антон Лобанов on 14.10.2021.
//

import Foundation
import NetworkKit

public struct UploadScanImageRequest: Request {
	public var api: RequestAPI {
		.internal
	}

	private let data: Data
	private let scanID: Int64
	private let type: Int

	public init(
		data: Data,
		scanID: Int64,
		type: Int
	) {
		self.data = data
		self.scanID = scanID
		self.type = type
	}

	public func parameters() -> RequestParameters<ImageResourceDTO> {
		.init(
			path: "/api/v2/scans/image",
			method: .post,
			contentType: .multipart([
				.init(data: self.data, name: "image", fileName: "image.png", mimeType: "image/png"),
			]),
			body: nil,
			headers: [
				"ScanID": "\(self.scanID)",
				"Type": "\(self.type)",
			]
		)
	}
}
