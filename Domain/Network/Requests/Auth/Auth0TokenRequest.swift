//
//  Created by Антон Лобанов on 14.10.2021.
//

import Foundation
import NetworkKit

public struct Auth0TokenRequest: Request {
	private let query: Query

	public var api: RequestAPI {
		.auth0
	}

	public init(_ query: Query) {
		self.query = query
	}

	public func parameters() -> RequestParameters<Auth0Token> {
		.init(
			path: "oauth/token",
			method: .post,
			contentType: .json,
			body: self.query.asDictionary,
			headers: nil
		)
	}
}

public extension Auth0TokenRequest {
	struct Query: Codable {
		public let grantType: String
		public let username: String
		public let password: String
		public let clientId: String
		public let scope: String
		public let audience: String?

		public init(
			grantType: String,
			username: String,
			password: String,
			clientId: String,
			scope: String,
			audience: String? = nil
		) {
			self.grantType = grantType
			self.username = username
			self.password = password
			self.clientId = clientId
			self.scope = scope
			self.audience = audience
		}
	}
}

public extension Auth0TokenRequest.Query {
	enum CodingKeys: String, CodingKey {
		case grantType = "grant_type"
		case username
		case password
		case scope
		case clientId = "client_id"
		case audience
	}
}
