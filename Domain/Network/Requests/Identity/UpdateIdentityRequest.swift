//
//  Created by Антон Лобанов on 14.10.2021.
//

import Foundation
import NetworkKit

public struct UpdateIdentityRequest: Request {
	private let query: UpdateIdentityQueryDTO

	public var api: RequestAPI {
		.internal
	}

	public init(
		query: UpdateIdentityQueryDTO
	) {
		self.query = query
	}

	public func parameters() -> RequestParameters<UpdateIdentityResourceDTO> {
		.init(
			path: "/api/v2/identities/\(self.query.id)/",
			method: .put,
			contentType: .json,
			body: self.query.asDictionary,
			headers: nil
		)
	}
}
