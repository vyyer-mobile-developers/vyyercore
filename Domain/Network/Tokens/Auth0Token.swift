//
//  Created by Антон Лобанов on 14.10.2021.
//

import Foundation

public struct Auth0Token: Codable {
	public enum CodingKeys: String, CodingKey {
		case accessToken = "access_token"
		case idToken = "id_token"
		case refreshToken = "refresh_token"
		case expiresIn = "expires_in"
		case tokenType = "token_type"
	}

	public let accessToken: String
	public let idToken: String
	public let refreshToken: String
	public let expiresIn: Int
	public let tokenType: String
}
