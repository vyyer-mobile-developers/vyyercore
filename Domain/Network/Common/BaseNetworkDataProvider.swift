//
//  Created by Антон Лобанов on 17.10.2021.
//

import mobile_multiplatform
import NetworkKit
import SharedKit

public final class BaseNetworkDataProvider: INetworkDataProvider {
	private let userSessionStore: IUserSessionStore
	private let configuration: Configuration

	public init(
		userSessionStore: IUserSessionStore,
		configuration: Configuration
	) {
		self.userSessionStore = userSessionStore
		self.configuration = configuration
	}

	public func baseURL(for api: RequestAPI) -> String? {
		switch api {
		case .auth0: return self.configuration.apiURL.absoluteString
		case .internal: return self.userSessionStore.audienceURL
		case .idMission: return "https://demo.idmission.com"
		default: return nil
		}
	}

	public func baseHeaders(for api: RequestAPI) -> [String: String] {
		switch api {
		case .internal:
			var headers = [String: String]()

			if let tokenType = self.userSessionStore.tokenType, let accessToken = self.userSessionStore.accessToken {
				headers["Authorization"] = tokenType + " " + accessToken
			}

			if let userId = self.userSessionStore.userId {
				headers["X-User-Id"] = userId
			}

			if let organizationId = self.userSessionStore.organizationId {
				headers["X-Org-Id"] = organizationId
			}

			return headers
		default:
			return [:]
		}
	}
}

extension BaseNetworkDataProvider: AuthDataRepository {
	public func get() -> AuthData {
		AuthData(
			accessToken: self.userSessionStore.accessToken ?? "",
			refreshToken: self.userSessionStore.refreshToken ?? "",
			audience: self.userSessionStore.audienceURL ?? "",
			userId: self.userSessionStore.userId ?? "",
			organizationId: self.userSessionStore.organizationId ?? "",
			clientId: self.configuration.clientID
		)
	}

	public func put(authData: AuthData) {
		self.userSessionStore.accessToken = authData.accessToken
		self.userSessionStore.refreshToken = authData.refreshToken
		Notifications.refreshTokenDidUpdate.invoke()
	}
}
