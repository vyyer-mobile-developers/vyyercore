//
//  Created by Антон Лобанов on 02.02.2022.
//

import NetworkKit

public extension RequestAPI {
	static let auth0 = RequestAPI(name: "Auth0")
	static let `internal` = RequestAPI(name: "Internal")
	static let idMission = RequestAPI(name: "IDMission")
}
