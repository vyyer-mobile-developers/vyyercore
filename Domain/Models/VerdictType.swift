//
//  Created by Антон Лобанов on 02.11.2021.
//

import Foundation

public enum VerdictType: Int {
	case unknown = -1
	case valid = 0
	case caution = 4
	case invalid = 8
	case fake = 16
}
