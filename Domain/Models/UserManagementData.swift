//
//  Created by Антон Лобанов on 21.12.2021.
//

import Foundation

public struct UserManagementData {
	public let topOrg: OrgData
	public let currentOrg: OrgData
	public let allOrgs: [OrgData]
}
