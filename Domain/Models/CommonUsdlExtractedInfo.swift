//
//  Created by Антон Лобанов on 07.03.2022.
//

import Foundation
import mobile_multiplatform

final class CommonUsdlExtractedInfo: mobile_multiplatform.CommonUsdlExtractedInfo {
	var dateOfBirthComponents: mobile_multiplatform.DateComponents?
	var dateOfExpiryComponents: mobile_multiplatform.DateComponents?
	var dateOfIssueComponents: mobile_multiplatform.DateComponents?
	var eyes: String?
	var fullName: String
	var height: String?
	var sex: String?

	init(scan: MicroblinkScan) {
		self.dateOfBirthComponents = .init(year: Int32(scan.dob.0), month: Int32(scan.dob.1), day: Int32(scan.dob.2))
		self.dateOfExpiryComponents = .init(year: Int32(scan.expiry.0), month: Int32(scan.expiry.1), day: Int32(scan.expiry.2))
		self.dateOfIssueComponents = .init(year: Int32(scan.issue.0), month: Int32(scan.issue.1), day: Int32(scan.issue.2))
		self.eyes = scan.eyes
		self.fullName = scan.fullName
		self.height = scan.height
		self.sex = scan.sex
	}
}
