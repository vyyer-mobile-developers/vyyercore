//
//  Created by Антон Лобанов on 21.12.2021.
//

import Foundation
import mobile_multiplatform

public struct OrgData {
	public let orgId: String
	public let name: String
	public let users: Int
}

extension OrgData {
	init(model: mobile_multiplatform.OrgData) {
		self.orgId = model.orgId
		self.name = model.name
		self.users = Int(model.users)
	}
}
