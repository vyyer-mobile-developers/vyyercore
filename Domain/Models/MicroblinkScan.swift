//
//  Created by Антон Лобанов on 17.10.2021.
//

import Foundation

public struct MicroblinkScan {
	public let age: Int
	public let fullName: String
	public let dob: (Int, Int, Int)
	public let issue: (Int, Int, Int)
	public let expiry: (Int, Int, Int)
	public let height: String
	public let eyes: String
	public let sex: String
	public let address: String
	public let city: String
	public let zip: String
	public let state: String?
	public let fullAddress: String

	public init(
		age: Int,
		fullName: String,
		dob: (Int, Int, Int),
		issue: (Int, Int, Int),
		expiry: (Int, Int, Int),
		height: String,
		eyes: String,
		sex: String,
		address: String,
		city: String,
		zip: String,
		state: String?,
		fullAddress: String
	) {
		self.age = age
		self.fullName = fullName
		self.dob = dob
		self.issue = issue
		self.expiry = expiry
		self.height = height
		self.eyes = eyes
		self.sex = sex
		self.address = address
		self.city = city
		self.zip = zip
		self.state = state
		self.fullAddress = fullAddress
	}
}
