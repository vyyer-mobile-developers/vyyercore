//
//
//  Created by Антон Лобанов on 07.03.2022.
//

import Foundation
import mobile_multiplatform

final class ApiIdentityBase: mobile_multiplatform.ApiIdentityBase {
	var address: String?
	var ban: KotlinInt?
	var banEndAt: String?
	var banStartAt: String?
	var bannedBy: String?
	var birthday: String?
	var city: String?
	var createdAt: String?
	var expiresAt: String?
	var eyeColor: String?
	var firstName: String?
	var fullName: String?
	var gender: String?
	var hairColor: String?
	var height: String?
	var id: Int32 = -1
	var issuedAt: String?
	var lastName: String?
	var lastScannedAt: String?
	var licenseNumber: String?
	var middleName: String?
	var orgID: String?
	var orientation: KotlinInt?
	var postalCode: String?
	var scansInPeriod: KotlinInt?
	var state: String?
	var street: String?
	var uid: String?
	var userID: String = ""
	var vip: KotlinInt?
	var vipBy: String?
	var vipEndAt: String?
	var vipStartAt: String?
	var visits: KotlinInt?
	var weight: String?

	init(identity: IdentityDTO) {
		self.address = identity.address
		self.ban = identity.ban.map { KotlinInt(int: Int32($0)) }
		self.banEndAt = identity.banEndAt
		self.banStartAt = identity.banStartAt
		self.bannedBy = identity.bannedBy
		self.birthday = identity.birthday
		self.city = identity.city
		self.createdAt = identity.createdAt
		self.expiresAt = identity.expiresAt
		self.eyeColor = identity.eyeColor
		self.firstName = identity.firstName
		self.fullName = identity.fullName
		self.gender = identity.gender
		self.hairColor = identity.hairColor
		self.height = identity.height
		self.id = Int32(identity.id)
		self.issuedAt = identity.issuedAt
		self.lastName = identity.lastName
		self.lastScannedAt = identity.lastScannedAt
		self.licenseNumber = identity.licenseNumber
		self.middleName = identity.middleName
		self.orgID = identity.orgId
		self.orientation = identity.orientation.map { KotlinInt(int: Int32($0)) }
		self.postalCode = identity.postalCode
		self.scansInPeriod = identity.scansInPeriod.map { KotlinInt(int: Int32($0)) }
		self.state = identity.state
		self.street = identity.street
		self.uid = identity.uid
		self.userID = identity.userId
		self.vip = identity.vip.map { KotlinInt(int: Int32($0)) }
		self.vipBy = identity.vipBy
		self.vipEndAt = identity.vipEndAt
		self.vipStartAt = identity.vipStartAt
		self.visits = identity.visits.map { KotlinInt(int: Int32($0)) }
		self.weight = identity.weight
	}

	init(entity: mobile_multiplatform.ApiIdentityBase) {
		self.address = entity.address
		self.ban = entity.ban
		self.banEndAt = entity.banEndAt
		self.banStartAt = entity.banStartAt
		self.bannedBy = entity.bannedBy
		self.birthday = entity.birthday
		self.city = entity.city
		self.createdAt = entity.createdAt
		self.expiresAt = entity.expiresAt
		self.eyeColor = entity.eyeColor
		self.firstName = entity.firstName
		self.fullName = entity.fullName
		self.gender = entity.gender
		self.hairColor = entity.hairColor
		self.height = entity.height
		self.id = entity.id
		self.issuedAt = entity.issuedAt
		self.lastName = entity.lastName
		self.lastScannedAt = entity.lastScannedAt
		self.licenseNumber = entity.licenseNumber
		self.middleName = entity.middleName
		self.orgID = entity.orgID
		self.orientation = entity.orientation
		self.postalCode = entity.postalCode
		self.scansInPeriod = entity.scansInPeriod
		self.state = entity.state
		self.street = entity.street
		self.uid = entity.uid
		self.userID = entity.userID
		self.vip = entity.vip
		self.vipBy = entity.vipBy
		self.vipEndAt = entity.vipEndAt
		self.vipStartAt = entity.vipStartAt
		self.visits = entity.visits
		self.weight = entity.weight
	}

	func map() -> IdentityDTO {
		.init(
			id: Int64(self.id),
			middleName: self.middleName,
			orgId: self.orgID,
			uid: self.uid!,
			userId: self.userID,
			address: self.address,
			ban: self.ban?.intValue,
			banEndAt: self.banEndAt,
			banStartAt: self.banStartAt,
			bannedBy: self.bannedBy,
			birthday: self.birthday,
			city: self.city,
			createdAt: self.createdAt,
			expiresAt: self.expiresAt,
			eyeColor: self.eyeColor,
			firstName: self.firstName,
			fullName: self.fullName,
			gender: self.gender,
			hairColor: self.hairColor,
			height: self.height,
			issuedAt: self.issuedAt,
			lastName: self.lastName,
			lastScannedAt: self.lastScannedAt,
			licenseNumber: self.licenseNumber,
			orientation: self.orientation?.intValue,
			postalCode: self.postalCode,
			scansInPeriod: self.scansInPeriod?.intValue,
			state: self.state,
			street: self.street,
			vip: self.vip?.intValue,
			vipBy: self.vipBy,
			vipEndAt: self.vipEndAt,
			vipStartAt: self.vipStartAt,
			visits: self.visits?.intValue,
			weight: self.weight
		)
	}
}
