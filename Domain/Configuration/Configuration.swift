//
//  Created by Антон Лобанов on 06.09.2021.
//

import Foundation

public protocol Configuration {
	var apiURL: URL { get }
	var clientID: String { get }
}
