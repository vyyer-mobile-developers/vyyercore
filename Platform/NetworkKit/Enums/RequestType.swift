//
//  Created by Антон Лобанов on 02.02.2022.
//

import Foundation

public enum RequestTask {
	case xml(URLRequest)
	case data(URLRequest)
	case upload(URLRequest, Data)
}
