//
//  Created by Антон Лобанов on 02.02.2022.
//

import Foundation
import PromiseKit

public protocol INetworkInterceptor: AnyObject {
	func process(_ error: Error) -> Promise<Void>?
	func proccessFailure()
}
