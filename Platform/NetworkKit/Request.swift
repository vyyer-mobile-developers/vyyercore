//
//  Created by Антон Лобанов on 14.10.2021.
//

import Foundation
import LogKit
import SharedKit

public protocol Request {
	associatedtype ReturnType: Codable

	var api: RequestAPI { get }

	func parameters() -> RequestParameters<ReturnType>
}

public extension Request {
	var api: RequestAPI {
		.default
	}
}

public struct RequestParameters<ReturnType: Codable> {
	public var boundary: String {
		Data(self.path.utf8).base64EncodedString()
	}

	public let path: String
	public let method: HTTPMethod
	public let contentType: ContentType
	public let body: [String: Any]?
	public let xmlBody: String?
	public let headers: [String: String]?

	public init(
		path: String,
		method: HTTPMethod,
		contentType: ContentType,
		body: [String: Any]?,
		xmlBody: String? = nil,
		headers: [String: String]?
	) {
		self.path = path
		self.method = method
		self.contentType = contentType
		self.body = body
		self.xmlBody = xmlBody
		self.headers = headers
	}
}

internal extension Request {
	func urlRequest(baseURL: String, baseHeaders: [String: String]) -> URLRequest? {
		let requestInfo = self.parameters()

		guard var urlComponents = URLComponents(string: baseURL) else { return nil }
		urlComponents.path = "\(urlComponents.path)\(requestInfo.path)"

		guard let finalURL = urlComponents.url else { return nil }

		var headers = baseHeaders
		headers += (requestInfo.headers ?? [:])

		var request = URLRequest(url: finalURL)

		request.httpMethod = requestInfo.method.rawValue
		request.allHTTPHeaderFields = headers

		switch requestInfo.contentType {
		case .xml:
			request.httpBody = requestInfo.xmlBody?.data(using: .utf8)
		default:
			request.httpBody = self.requestBodyFrom(params: requestInfo.body)
		}

		request.setValue({ () -> String in
			switch requestInfo.contentType {
			case .multipart: return "\(requestInfo.contentType.value); boundary=\(requestInfo.boundary)"
			default: return requestInfo.contentType.value
			}
		}(), forHTTPHeaderField: "Content-Type")

		Log {
			"Path: \(requestInfo.path)"
			request.allHTTPHeaderFields
			requestInfo.body
			requestInfo.contentType.value
			requestInfo.method.rawValue
		}

		return request
	}

	func dataToUpload() -> Data? {
		let requestInfo = self.parameters()
		guard case let .multipart(objects) = requestInfo.contentType, objects.isEmpty == false else { return nil }

		var data = Data()

		objects.forEach { object in
			"\r\n--\(requestInfo.boundary)\r\n"
				.data(using: .utf8).map { data.append($0) }

			"Content-Disposition: form-data; name=\"\(object.name)\"; filename=\"\(object.fileName)\"\r\n"
				.data(using: .utf8).map { data.append($0) }

			"Content-Type: \(object.mimeType)\r\n\r\n"
				.data(using: .utf8).map { data.append($0) }

			data.append(object.data)

			"\r\n--\(requestInfo.boundary)--\r\n"
				.data(using: .utf8).map { data.append($0) }
		}

		return data
	}

	func requestBodyFrom(params: [String: Any]?) -> Data? {
		guard let params = params else { return nil }
		guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
			return nil
		}
		return httpBody
	}
}
