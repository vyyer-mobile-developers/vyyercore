//
//  Created by Антон Лобанов on 02.02.2022.
//

import ErrorKit
import Foundation
import PromiseKit

public protocol INetwork: AnyObject {
	func dispatch<R: Request>(
		_ request: R
	) -> Promise<R.ReturnType>
}

public final class Network: INetwork {
	public var interceptor: INetworkInterceptor?

	private let dataProvider: INetworkDataProvider
	private let dispatcher: INetworkDispatcher

	public init(
		dataProvider: INetworkDataProvider,
		dispatcher: INetworkDispatcher
	) {
		self.dataProvider = dataProvider
		self.dispatcher = dispatcher
	}

	public func dispatch<R: Request>(_ request: R) -> Promise<R.ReturnType> {
		guard let requestTask = self.makeRequestTask(request) else {
			return .init(error: NetworkError.badRequest)
		}

		if let interceptor = self.interceptor {
			return Promise { seal in
				self.dispatcher.dispatch(requestTask)
					.done { seal.fulfill($0) }
					.catch {
						guard let proccessPromise = interceptor.process($0) else {
							seal.reject($0)
							return
						}
						proccessPromise
							.done {
								guard let newRequestTask = self.makeRequestTask(request) else {
									seal.reject(NetworkError.badRequest)
									return
								}
								self.dispatcher.dispatch(newRequestTask)
									.done { seal.fulfill($0) }
									.catch {
										seal.reject($0)
										interceptor.proccessFailure()
									}
							}
							.catch {
								seal.reject($0)
								interceptor.proccessFailure()
							}
					}
			}
		}

		return self.dispatcher.dispatch(requestTask)
	}

	private func makeRequestTask<R: Request>(_ request: R) -> RequestTask? {
		let baseHeaders = self.dataProvider.baseHeaders(for: request.api)
		guard let baseURL = self.dataProvider.baseURL(for: request.api),
		      let urlRequest = request.urlRequest(baseURL: baseURL, baseHeaders: baseHeaders)
		else {
			return nil
		}

		switch request.parameters().contentType {
		case .xml:
			return .xml(urlRequest)
		default:
			return request.dataToUpload().map { .upload(urlRequest, $0) } ?? .data(urlRequest)
		}
	}
}
