//
//  Created by Антон Лобанов on 14.10.2021.
//

import ErrorKit
import Foundation
import LogKit
import PromiseKit
import XMLParsing

public protocol INetworkDispatcher: AnyObject {
	/// Dispatches an URLRequest and returns a publisher
	/// - Parameter request: URLRequest
	/// - Returns: A publisher with the provided decoded data or an error
	func dispatch<ReturnType: Codable>(_ task: RequestTask) -> Promise<ReturnType>
}

public final class NetworkDispatcher {
	private let urlSession: URLSession

	public init(urlSession: URLSession = .shared) {
		self.urlSession = urlSession
	}
}

extension NetworkDispatcher: INetworkDispatcher {
	public func dispatch<ReturnType: Codable>(_ task: RequestTask) -> Promise<ReturnType> {
		Promise { seal in
			let sessionTask: URLSessionDataTask

			switch task {
			case let .data(request):
				sessionTask = self.urlSession.dataTask(with: request) {
					self.handleRequest(seal, request: request, data: $0, response: $1, error: $2)
				}
			case let .upload(request, data):
				sessionTask = self.urlSession.uploadTask(with: request, from: data) {
					self.handleRequest(seal, request: request, data: $0, response: $1, error: $2)
				}
			case let .xml(request):
				sessionTask = self.urlSession.dataTask(with: request) {
					self.handleXMLRequest(seal, request: request, data: $0, response: $1, error: $2)
				}
			}

			sessionTask.resume()
		}
	}
}

private extension NetworkDispatcher {
	// swiftlint:disable line_length
	func handleRequest<ReturnType: Codable>(_ seal: Resolver<ReturnType>, request: URLRequest, data: Data?, response: URLResponse?, error: Error?) {
		Log {
			"Finish request - \(request.url?.absoluteString ?? "#")"
			"Code - \((response as? HTTPURLResponse)?.statusCode ?? 0)"
			"Error - \(String(describing: error))"
			"Data:"
			data.map { String(data: $0, encoding: .utf8) ?? "#" } ?? "#"
		}

		if let error = error {
			seal.reject(error)
			return
		}

		// If the response is invalid, throw an error
		if let response = response as? HTTPURLResponse, (200 ... 299).contains(response.statusCode) == false {
			do {
				let serverError = try ServerError(data, networkError: NetworkError(code: response.statusCode))
				seal.reject(serverError)
			}
			catch {
				seal.reject(NetworkError(code: response.statusCode))
			}
			return
		}

		guard let data = data else {
			seal.reject(NetworkError.emptyData)
			return
		}

		do {
			let result = try JSONDecoder().decode(ReturnType.self, from: data)
			seal.fulfill(result)
		}
		catch {
			seal.reject(error)
		}
	}

	// swiftlint:disable line_length
	func handleXMLRequest<ReturnType: Codable>(_ seal: Resolver<ReturnType>, request: URLRequest, data: Data?, response: URLResponse?, error: Error?) {
		Log {
			"Finish request - \(request.url?.absoluteString ?? "#")"
			"Code - \((response as? HTTPURLResponse)?.statusCode ?? 0)"
			"Error - \(String(describing: error))"
			"Data:"
			data.map { String(data: $0, encoding: .utf8) ?? "#" } ?? "#"
		}

		if let error = error {
			seal.reject(error)
			return
		}

		// If the response is invalid, throw an error
		if let response = response as? HTTPURLResponse, (200 ... 299).contains(response.statusCode) == false {
			do {
				let serverError = try ServerError(data, networkError: NetworkError(code: response.statusCode))
				seal.reject(serverError)
			}
			catch {
				seal.reject(NetworkError(code: response.statusCode))
			}
			return
		}

		guard let data = data else {
			seal.reject(NetworkError.emptyData)
			return
		}

		do {
			let result = try XMLDecoder().decode(ReturnType.self, from: data)
			seal.fulfill(result)
		}
		catch {
			seal.reject(error)
		}
	}
}
