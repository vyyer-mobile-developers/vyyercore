#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class Mobile_multiplatformAntilogProvider, Mobile_multiplatformNapierAntilog, Mobile_multiplatformAuthData, Mobile_multiplatformDateComponents, Mobile_multiplatformExposingThings, Mobile_multiplatformNapierLogLevel, Mobile_multiplatformKotlinThrowable, Mobile_multiplatformJsonProvider, Mobile_multiplatformKotlinx_serialization_jsonJson, Mobile_multiplatformKtor_client_coreHttpClient, Mobile_multiplatformLogsSwitcher, Mobile_multiplatformNetworkExceptionsProvider, Mobile_multiplatformNetworkProblem, Mobile_multiplatformNetworkProblemConnectionProblem, Mobile_multiplatformNetworkProblemForbidden, Mobile_multiplatformNetworkProblemUnauthorized, Mobile_multiplatformPlatformInfo, Mobile_multiplatformCustomAuthFeature, Mobile_multiplatformKtor_utilsAttributeKey<T>, Mobile_multiplatformKtor_client_authAuth, Mobile_multiplatformDefaultHttpEngineProvider, Mobile_multiplatformApiError, Mobile_multiplatformApiBodyWithErrorsCompanion, Mobile_multiplatformApiBodyWithErrors, Mobile_multiplatformApiErrorCompanion, Mobile_multiplatformKotlinEnumCompanion, Mobile_multiplatformKotlinEnum<E>, Mobile_multiplatformDeleteResult, Mobile_multiplatformKotlinArray<T>, Mobile_multiplatformGetPageResults, Mobile_multiplatformGetPageResultsGotNoMorePages, Mobile_multiplatformLocalSyncStatusRepository, Mobile_multiplatformInitialSyncManagerCompanion, Mobile_multiplatformInitialSyncManagerFullSyncStatus, Mobile_multiplatformInitialSyncManagerInitialSyncStatus, Mobile_multiplatformInitialSyncManagerInitialSyncStatusCompanion, Mobile_multiplatformIdentitiesSyncStrategy, Mobile_multiplatformScansSyncStrategy, Mobile_multiplatformSetResult, Mobile_multiplatformSyncExecutorFactory, Mobile_multiplatformSyncAction<Result>, Mobile_multiplatformInitialSyncManager, Mobile_multiplatformSyncPeriodsManager, Mobile_multiplatformSyncActionsManager, Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher, Mobile_multiplatformSyncExecutor, Mobile_multiplatformSyncPeriodsManagerCompanion, Mobile_multiplatformSyncPeriodsManagerSyncPeriod, Mobile_multiplatformKotlinUnit, Mobile_multiplatformKtorProvider, Mobile_multiplatformApiEntity, Mobile_multiplatformApiGetRequest, Mobile_multiplatformApiGetResponse<T>, Mobile_multiplatformKtor_client_coreHttpRequestBuilder, Mobile_multiplatformApiPullRequest, Mobile_multiplatformApiPullResponse<T>, Mobile_multiplatformApiPushResponse<T>, Mobile_multiplatformGetFromApi<T>, Mobile_multiplatformApiIdentity, Mobile_multiplatformPullFromApi<T>, Mobile_multiplatformApiIdentitySyncEntry, Mobile_multiplatformPushToApi<T>, Mobile_multiplatformApiScan, Mobile_multiplatformApiScanSyncEntry, Mobile_multiplatformLocalRepositoryAdapter<LocalEntity, LocalEntityImpl>, Mobile_multiplatformProblemFilter, Mobile_multiplatformActionsWithEntities<LocalEntity>, Mobile_multiplatformBaseConverter<ApiEntity, SyncEntity, LocalEntity>, Mobile_multiplatformBaseSimpleConverter<ApiEntity, LocalEntity>, Mobile_multiplatformSimpleConverter<ApiEntity, SyncEntity, LocalEntity>, Mobile_multiplatformBaseTwoWaysConverter<ApiEntity, SyncEntity, LocalEntity>, Mobile_multiplatformApiDateTimeConverter, Mobile_multiplatformApiDateOnlyConverter, Mobile_multiplatformDateTimeConverter, Mobile_multiplatformApiDatesModule, Mobile_multiplatformApiTimeOnlyConverter, Mobile_multiplatformIdentityConverter, Mobile_multiplatformScanConverter, Mobile_multiplatformConvertersModule, Mobile_multiplatformGetIdentitiesFromApi, Mobile_multiplatformGetScansFromApi, Mobile_multiplatformPullIdentitiesFromApi, Mobile_multiplatformPullScansFromApi, Mobile_multiplatformPushIdentitiesToApi, Mobile_multiplatformSync, Mobile_multiplatformApiGetRequestCompanion, Mobile_multiplatformApiPagination, Mobile_multiplatformApiGetResponseCompanion, Mobile_multiplatformApiPaginationCompanion, Mobile_multiplatformApiPullRequestCompanion, Mobile_multiplatformApiPullResponseCompanion, Mobile_multiplatformApiPushResponseCompanion, Mobile_multiplatformApiIdentityCompanion, Mobile_multiplatformApiIdentitySyncEntryCompanion, Mobile_multiplatformApiPushIdentityRequestCompanion, Mobile_multiplatformApiScanCompanion, Mobile_multiplatformApiScanSyncEntryCompanion, Mobile_multiplatformUpdatingSyncStrategyCompanion, Mobile_multiplatformKotlinPair<__covariant A, __covariant B>, Mobile_multiplatformPullResult, Mobile_multiplatformPushResult, Mobile_multiplatformUpdatingSyncStrategy, Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete, Mobile_multiplatformDownloadSyncStrategy<ApiEntity, SyncEntity, LocalEntity, Converter>, Mobile_multiplatformTwoWaySyncStrategyCompanion, Mobile_multiplatformTwoWaySyncStrategy<ApiEntity, SyncEntity, LocalEntity, Converter, R>, Mobile_multiplatformApiPushIdentityRequest, Mobile_multiplatformConstants, Mobile_multiplatformSyncStrategyCompanion, Mobile_multiplatformSyncDispatcherProvider, Mobile_multiplatformSimpleTimeWithUnit, Mobile_multiplatformAuth0Tools, Mobile_multiplatformAuth0LoginDataCompanion, Mobile_multiplatformAuth0LoginData, Mobile_multiplatformAuth0TokenRefreshRequestCompanion, Mobile_multiplatformAuth0TokenRefreshRequest, Mobile_multiplatformAuth0TokenResponseCompanion, Mobile_multiplatformAuth0TokenResponse, Mobile_multiplatformLoginData, Mobile_multiplatformLoginResult, Mobile_multiplatformTokenRefreshData, Mobile_multiplatformTokenRefreshResult, Mobile_multiplatformLoginFirstAuthStageData, Mobile_multiplatformApiOrganizationUsersData, Mobile_multiplatformApiGetUsersResponseCompanion, Mobile_multiplatformApiGetUsersResponse, Mobile_multiplatformApiOrganizationUsersDataCompanion, Mobile_multiplatformApiReassignmentApiRequestCompanion, Mobile_multiplatformApiReassignmentApiRequest, Mobile_multiplatformApiReassignmentResponseCompanion, Mobile_multiplatformApiReassignmentResponse, Mobile_multiplatformOrgData, Mobile_multiplatformReassignmentResult, Mobile_multiplatformReassignmentResultError, Mobile_multiplatformReassignmentResultErrorNetworkError, Mobile_multiplatformReassignmentResultSuccess, Mobile_multiplatformUserManagementData, Mobile_multiplatformUserManagementDataError, Mobile_multiplatformUserManagementDataErrorCurrentOrganizationDataNotFound, Mobile_multiplatformUserManagementDataErrorNetworkError, Mobile_multiplatformUserManagementDataErrorTopOrganizationDataNotFound, Mobile_multiplatformUserManagementDataSuccess, Mobile_multiplatformKotlinNumber, Mobile_multiplatformKotlinx_serialization_coreSerializersModule, Mobile_multiplatformKotlinx_serialization_jsonJsonConfiguration, Mobile_multiplatformKotlinx_serialization_jsonJsonDefault, Mobile_multiplatformKotlinx_serialization_jsonJsonElement, Mobile_multiplatformKtor_client_coreHttpClientEngineConfig, Mobile_multiplatformKtor_client_coreHttpClientConfig<T>, Mobile_multiplatformKtor_client_coreHttpClientCall, Mobile_multiplatformKtor_client_coreHttpReceivePipeline, Mobile_multiplatformKtor_client_coreHttpRequestPipeline, Mobile_multiplatformKtor_client_coreHttpResponsePipeline, Mobile_multiplatformKtor_client_coreHttpSendPipeline, Mobile_multiplatformKotlinException, Mobile_multiplatformKotlinRuntimeException, Mobile_multiplatformKotlinIllegalStateException, Mobile_multiplatformKtor_httpHttpAuthHeader, Mobile_multiplatformKtor_client_coreHttpResponse, Mobile_multiplatformKtor_client_authAuthFeature, Mobile_multiplatformKtor_client_coreHttpRequestData, Mobile_multiplatformKtor_client_coreHttpResponseData, Mobile_multiplatformKotlinAbstractCoroutineContextElement, Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcherKey, Mobile_multiplatformKtor_httpHeadersBuilder, Mobile_multiplatformKtor_client_coreHttpRequestBuilderCompanion, Mobile_multiplatformKtor_httpURLBuilder, Mobile_multiplatformKtor_httpHttpMethod, Mobile_multiplatformKtor_ioMemory, Mobile_multiplatformKtor_ioIoBuffer, Mobile_multiplatformKotlinByteArray, Mobile_multiplatformKtor_ioByteReadPacket, Mobile_multiplatformKtor_ioByteOrder, Mobile_multiplatformKotlinx_serialization_jsonJsonElementCompanion, Mobile_multiplatformKtor_client_coreProxyConfig, Mobile_multiplatformKotlinNothing, Mobile_multiplatformKtor_client_coreHttpClientCallCompanion, Mobile_multiplatformKtor_client_coreTypeInfo, Mobile_multiplatformKtor_utilsPipelinePhase, Mobile_multiplatformKtor_utilsPipeline<TSubject, TContext>, Mobile_multiplatformKtor_client_coreHttpReceivePipelinePhases, Mobile_multiplatformKtor_client_coreHttpRequestPipelinePhases, Mobile_multiplatformKtor_client_coreHttpResponsePipelinePhases, Mobile_multiplatformKtor_client_coreHttpResponseContainer, Mobile_multiplatformKtor_client_coreHttpSendPipelinePhases, Mobile_multiplatformKtor_httpHttpAuthHeaderCompanion, Mobile_multiplatformKtor_httpHeaderValueEncoding, Mobile_multiplatformKtor_utilsGMTDate, Mobile_multiplatformKtor_httpHttpStatusCode, Mobile_multiplatformKtor_httpHttpProtocolVersion, Mobile_multiplatformKtor_httpUrl, Mobile_multiplatformKtor_httpOutgoingContent, Mobile_multiplatformKotlinx_serialization_coreSerialKind, Mobile_multiplatformKotlinAbstractCoroutineContextKey<B, E>, Mobile_multiplatformKtor_utilsStringValuesBuilder, Mobile_multiplatformKtor_httpURLProtocol, Mobile_multiplatformKtor_httpParametersBuilder, Mobile_multiplatformKtor_httpURLBuilderCompanion, Mobile_multiplatformKotlinCancellationException, Mobile_multiplatformKtor_httpHttpMethodCompanion, Mobile_multiplatformKtor_ioMemoryCompanion, Mobile_multiplatformKtor_ioBufferCompanion, Mobile_multiplatformKtor_ioBuffer, Mobile_multiplatformKtor_ioChunkBuffer, Mobile_multiplatformKtor_ioChunkBufferCompanion, Mobile_multiplatformKotlinCharArray, Mobile_multiplatformKtor_ioIoBufferCompanion, Mobile_multiplatformKotlinByteIterator, Mobile_multiplatformKtor_ioAbstractInputCompanion, Mobile_multiplatformKtor_ioAbstractInput, Mobile_multiplatformKtor_ioByteReadPacketBaseCompanion, Mobile_multiplatformKtor_ioByteReadPacketBase, Mobile_multiplatformKtor_ioByteReadPacketPlatformBase, Mobile_multiplatformKtor_ioByteReadPacketCompanion, Mobile_multiplatformKtor_ioByteOrderCompanion, Mobile_multiplatformKtor_httpHttpAuthHeaderParameterized, Mobile_multiplatformKtor_ioCharset, Mobile_multiplatformKtor_utilsGMTDateCompanion, Mobile_multiplatformKtor_utilsWeekDay, Mobile_multiplatformKtor_utilsMonth, Mobile_multiplatformKtor_httpHttpStatusCodeCompanion, Mobile_multiplatformKtor_httpHttpProtocolVersionCompanion, Mobile_multiplatformKtor_httpUrlCompanion, Mobile_multiplatformKtor_httpContentType, Mobile_multiplatformKtor_httpURLProtocolCompanion, Mobile_multiplatformKtor_httpUrlEncodingOption, Mobile_multiplatformKotlinCharIterator, Mobile_multiplatformKotlinKTypeProjection, Mobile_multiplatformKtor_httpHeaderValueParam, Mobile_multiplatformKtor_ioCharsetCompanion, Mobile_multiplatformKtor_ioCharsetDecoder, Mobile_multiplatformKtor_ioCharsetEncoder, Mobile_multiplatformKtor_utilsWeekDayCompanion, Mobile_multiplatformKtor_utilsMonthCompanion, Mobile_multiplatformKtor_httpHeaderValueWithParametersCompanion, Mobile_multiplatformKtor_httpHeaderValueWithParameters, Mobile_multiplatformKtor_httpContentTypeCompanion, Mobile_multiplatformKotlinx_coroutines_coreAtomicDesc, Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp, Mobile_multiplatformKotlinKVariance, Mobile_multiplatformKotlinKTypeProjectionCompanion, Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<__contravariant T>, Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor, Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode, Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc, Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T>, Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T>;

@protocol Mobile_multiplatformKotlinx_coroutines_coreMutableSharedFlow, Mobile_multiplatformAuthDataRepository, Mobile_multiplatformKotlinKClass, Mobile_multiplatformKtor_client_authAuthProvider, Mobile_multiplatformKtor_client_coreHttpClientFeature, Mobile_multiplatformKtor_client_coreHttpClientEngine, Mobile_multiplatformKotlinx_serialization_coreKSerializer, Mobile_multiplatformParcelable, Mobile_multiplatformWithLastChange, Mobile_multiplatformKotlinx_coroutines_coreFlow, Mobile_multiplatformLocalIdentity, Mobile_multiplatformLocalScan, Mobile_multiplatformLocalSyncStatus, Mobile_multiplatformKotlinComparable, Mobile_multiplatformSyncStrategiesProvider, Mobile_multiplatformSyncStrategy, Mobile_multiplatformSuspendAction, Mobile_multiplatformProblemTracker, Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope, Mobile_multiplatformApiPushRequest, Mobile_multiplatformBaseEntity, Mobile_multiplatformCanFillLocalIdsAndExcludeUnchanged, Mobile_multiplatformLocalRepository, Mobile_multiplatformAbleToGetChanges, Mobile_multiplatformHasChangesFlow, Mobile_multiplatformLocalRepositoryWithChanges, Mobile_multiplatformSyncStatusStorage, Mobile_multiplatformConcreteEntitiesCreator, Mobile_multiplatformApiToLocalConverter, Mobile_multiplatformApiAndLocalConverter, Mobile_multiplatformApiIdentityBase, Mobile_multiplatformApiSyncEntry, Mobile_multiplatformIdOwner, Mobile_multiplatformRidOwner, Mobile_multiplatformCreateUpdateTimeOwner, Mobile_multiplatformWithDeletionMarkMutable, Mobile_multiplatformChangable, Mobile_multiplatformApiScanBase, Mobile_multiplatformCommonUsdlExtractedInfo, Mobile_multiplatformKtor_ioByteReadChannel, Mobile_multiplatformKotlinx_coroutines_coreStateFlow, Mobile_multiplatformKotlinx_coroutines_coreFlowCollector, Mobile_multiplatformKotlinx_coroutines_coreSharedFlow, Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy, Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy, Mobile_multiplatformKotlinx_serialization_coreSerialFormat, Mobile_multiplatformKotlinx_serialization_coreStringFormat, Mobile_multiplatformKotlinCoroutineContext, Mobile_multiplatformKtor_ioCloseable, Mobile_multiplatformKtor_client_coreHttpClientEngineCapability, Mobile_multiplatformKtor_utilsAttributes, Mobile_multiplatformKotlinKDeclarationContainer, Mobile_multiplatformKotlinKAnnotatedElement, Mobile_multiplatformKotlinKClassifier, Mobile_multiplatformKotlinx_serialization_coreEncoder, Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor, Mobile_multiplatformKotlinx_serialization_coreDecoder, Mobile_multiplatformKotlinIterator, Mobile_multiplatformKotlinCoroutineContextKey, Mobile_multiplatformKotlinCoroutineContextElement, Mobile_multiplatformKotlinContinuation, Mobile_multiplatformKotlinContinuationInterceptor, Mobile_multiplatformKotlinx_coroutines_coreRunnable, Mobile_multiplatformKtor_httpHttpMessageBuilder, Mobile_multiplatformKotlinx_coroutines_coreJob, Mobile_multiplatformKtor_ioReadSession, Mobile_multiplatformKotlinSuspendFunction1, Mobile_multiplatformKotlinAppendable, Mobile_multiplatformKotlinx_serialization_coreSerializersModuleCollector, Mobile_multiplatformKtor_utilsTypeInfo, Mobile_multiplatformKtor_client_coreHttpRequest, Mobile_multiplatformKotlinSuspendFunction2, Mobile_multiplatformKtor_httpHeaders, Mobile_multiplatformKtor_httpHttpMessage, Mobile_multiplatformKotlinx_serialization_coreCompositeEncoder, Mobile_multiplatformKotlinAnnotation, Mobile_multiplatformKotlinx_serialization_coreCompositeDecoder, Mobile_multiplatformKtor_utilsStringValues, Mobile_multiplatformKotlinMapEntry, Mobile_multiplatformKotlinx_coroutines_coreChildHandle, Mobile_multiplatformKotlinx_coroutines_coreChildJob, Mobile_multiplatformKotlinx_coroutines_coreDisposableHandle, Mobile_multiplatformKotlinSequence, Mobile_multiplatformKotlinx_coroutines_coreSelectClause0, Mobile_multiplatformKtor_ioObjectPool, Mobile_multiplatformKtor_ioInput, Mobile_multiplatformKtor_ioOutput, Mobile_multiplatformKotlinFunction, Mobile_multiplatformKotlinKType, Mobile_multiplatformKtor_httpParameters, Mobile_multiplatformKotlinx_coroutines_coreParentJob, Mobile_multiplatformKotlinx_coroutines_coreSelectInstance, Mobile_multiplatformKotlinSuspendFunction0;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface Mobile_multiplatformBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface Mobile_multiplatformBase (Mobile_multiplatformBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface Mobile_multiplatformMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface Mobile_multiplatformMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorMobile_multiplatformKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface Mobile_multiplatformNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface Mobile_multiplatformByte : Mobile_multiplatformNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface Mobile_multiplatformUByte : Mobile_multiplatformNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface Mobile_multiplatformShort : Mobile_multiplatformNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface Mobile_multiplatformUShort : Mobile_multiplatformNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface Mobile_multiplatformInt : Mobile_multiplatformNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface Mobile_multiplatformUInt : Mobile_multiplatformNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface Mobile_multiplatformLong : Mobile_multiplatformNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface Mobile_multiplatformULong : Mobile_multiplatformNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface Mobile_multiplatformFloat : Mobile_multiplatformNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface Mobile_multiplatformDouble : Mobile_multiplatformNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface Mobile_multiplatformBoolean : Mobile_multiplatformNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AntilogProvider")))
@interface Mobile_multiplatformAntilogProvider : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)antilogProvider __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformAntilogProvider *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformNapierAntilog *)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AuthData")))
@interface Mobile_multiplatformAuthData : Mobile_multiplatformBase
- (instancetype)initWithAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken audience:(NSString *)audience userId:(NSString *)userId organizationId:(NSString *)organizationId clientId:(NSString *)clientId __attribute__((swift_name("init(accessToken:refreshToken:audience:userId:organizationId:clientId:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (Mobile_multiplatformAuthData *)doCopyAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken audience:(NSString *)audience userId:(NSString *)userId organizationId:(NSString *)organizationId clientId:(NSString *)clientId __attribute__((swift_name("doCopy(accessToken:refreshToken:audience:userId:organizationId:clientId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *accessToken __attribute__((swift_name("accessToken")));
@property (readonly) NSString *audience __attribute__((swift_name("audience")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *organizationId __attribute__((swift_name("organizationId")));
@property (readonly) NSString *refreshToken __attribute__((swift_name("refreshToken")));
@property (readonly) NSString *userId __attribute__((swift_name("userId")));
@end;

__attribute__((swift_name("AuthDataRepository")))
@protocol Mobile_multiplatformAuthDataRepository
@required
- (Mobile_multiplatformAuthData *)get __attribute__((swift_name("get()")));
- (void)putAuthData:(Mobile_multiplatformAuthData *)authData __attribute__((swift_name("put(authData:)")));
@end;

__attribute__((swift_name("CommonUsdlExtractedInfo")))
@protocol Mobile_multiplatformCommonUsdlExtractedInfo
@required
@property (readonly) Mobile_multiplatformDateComponents * _Nullable dateOfBirthComponents __attribute__((swift_name("dateOfBirthComponents")));
@property (readonly) Mobile_multiplatformDateComponents * _Nullable dateOfExpiryComponents __attribute__((swift_name("dateOfExpiryComponents")));
@property (readonly) Mobile_multiplatformDateComponents * _Nullable dateOfIssueComponents __attribute__((swift_name("dateOfIssueComponents")));
@property (readonly) NSString * _Nullable eyes __attribute__((swift_name("eyes")));
@property (readonly) NSString *fullName __attribute__((swift_name("fullName")));
@property (readonly) NSString * _Nullable height __attribute__((swift_name("height")));
@property (readonly) NSString * _Nullable sex __attribute__((swift_name("sex")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ExposingThings")))
@interface Mobile_multiplatformExposingThings : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)exposingThings __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformExposingThings *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_coroutines_coreMutableSharedFlow>)createMutableSharedFlow __attribute__((swift_name("createMutableSharedFlow()")));
@end;

__attribute__((swift_name("NapierAntilog")))
@interface Mobile_multiplatformNapierAntilog : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)isEnablePriority:(Mobile_multiplatformNapierLogLevel *)priority tag:(NSString * _Nullable)tag __attribute__((swift_name("isEnable(priority:tag:)")));
- (void)logPriority:(Mobile_multiplatformNapierLogLevel *)priority tag:(NSString * _Nullable)tag throwable:(Mobile_multiplatformKotlinThrowable * _Nullable)throwable message:(NSString * _Nullable)message __attribute__((swift_name("log(priority:tag:throwable:message:)")));
- (void)performLogPriority:(Mobile_multiplatformNapierLogLevel *)priority tag:(NSString * _Nullable)tag throwable:(Mobile_multiplatformKotlinThrowable * _Nullable)throwable message:(NSString * _Nullable)message __attribute__((swift_name("performLog(priority:tag:throwable:message:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IosAntilog")))
@interface Mobile_multiplatformIosAntilog : Mobile_multiplatformNapierAntilog
- (instancetype)initWithDefaultTag:(NSString *)defaultTag __attribute__((swift_name("init(defaultTag:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDefaultTag:(NSString *)defaultTag coroutinesSuffix:(BOOL)coroutinesSuffix __attribute__((swift_name("init(defaultTag:coroutinesSuffix:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)performLogPriority:(Mobile_multiplatformNapierLogLevel *)priority tag:(NSString * _Nullable)tag throwable:(Mobile_multiplatformKotlinThrowable * _Nullable)throwable message:(NSString * _Nullable)message __attribute__((swift_name("performLog(priority:tag:throwable:message:)")));
- (void)setDateFormatterStringFormatter:(NSString *)formatter __attribute__((swift_name("setDateFormatterString(formatter:)")));
- (void)setTagLevel:(Mobile_multiplatformNapierLogLevel *)level tag:(NSString *)tag __attribute__((swift_name("setTag(level:tag:)")));
@property BOOL crashAssert __attribute__((swift_name("crashAssert")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("JsonProvider")))
@interface Mobile_multiplatformJsonProvider : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)jsonProvider __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformJsonProvider *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKotlinx_serialization_jsonJson *)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KtorProvider")))
@interface Mobile_multiplatformKtorProvider : Mobile_multiplatformBase
- (instancetype)initWithAuth0Url:(NSString *)auth0Url authDataRepository:(id<Mobile_multiplatformAuthDataRepository>)authDataRepository __attribute__((swift_name("init(auth0Url:authDataRepository:)"))) __attribute__((objc_designated_initializer));
- (Mobile_multiplatformKtor_client_coreHttpClient *)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LogsSwitcher")))
@interface Mobile_multiplatformLogsSwitcher : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)logsSwitcher __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformLogsSwitcher *shared __attribute__((swift_name("shared")));
- (void)enable __attribute__((swift_name("enable()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkExceptionsProvider")))
@interface Mobile_multiplatformNetworkExceptionsProvider : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)networkExceptionsProvider __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformNetworkExceptionsProvider *shared __attribute__((swift_name("shared")));
- (NSArray<id<Mobile_multiplatformKotlinKClass>> *)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((swift_name("NetworkProblem")))
@interface Mobile_multiplatformNetworkProblem : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkProblem.ConnectionProblem")))
@interface Mobile_multiplatformNetworkProblemConnectionProblem : Mobile_multiplatformNetworkProblem
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)connectionProblem __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformNetworkProblemConnectionProblem *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkProblem.Forbidden")))
@interface Mobile_multiplatformNetworkProblemForbidden : Mobile_multiplatformNetworkProblem
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)forbidden __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformNetworkProblemForbidden *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkProblem.OtherProblem")))
@interface Mobile_multiplatformNetworkProblemOtherProblem : Mobile_multiplatformNetworkProblem
- (instancetype)initWithHttpCode:(int32_t)httpCode message:(NSString *)message __attribute__((swift_name("init(httpCode:message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t httpCode __attribute__((swift_name("httpCode")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkProblem.Unauthorized")))
@interface Mobile_multiplatformNetworkProblemUnauthorized : Mobile_multiplatformNetworkProblem
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)unauthorized __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformNetworkProblemUnauthorized *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PlatformInfo")))
@interface Mobile_multiplatformPlatformInfo : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)platformInfo __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformPlatformInfo *shared __attribute__((swift_name("shared")));
@property (readonly) NSString *os __attribute__((swift_name("os")));
@end;

__attribute__((swift_name("SuspendAction")))
@protocol Mobile_multiplatformSuspendAction
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeWithCompletionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomAuth")))
@interface Mobile_multiplatformCustomAuth : Mobile_multiplatformBase
- (instancetype)initWithProviders:(NSMutableArray<id<Mobile_multiplatformKtor_client_authAuthProvider>> *)providers __attribute__((swift_name("init(providers:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformCustomAuthFeature *companion __attribute__((swift_name("companion")));
@property (readonly) NSMutableArray<id<Mobile_multiplatformKtor_client_authAuthProvider>> *providers __attribute__((swift_name("providers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientFeature")))
@protocol Mobile_multiplatformKtor_client_coreHttpClientFeature
@required
- (void)installFeature:(id)feature scope:(Mobile_multiplatformKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(feature:scope:)")));
- (id)prepareBlock:(void (^)(id))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) Mobile_multiplatformKtor_utilsAttributeKey<id> *key __attribute__((swift_name("key")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CustomAuth.Feature")))
@interface Mobile_multiplatformCustomAuthFeature : Mobile_multiplatformBase <Mobile_multiplatformKtor_client_coreHttpClientFeature>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)feature __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformCustomAuthFeature *shared __attribute__((swift_name("shared")));
- (void)installFeature:(Mobile_multiplatformKtor_client_authAuth *)feature scope:(Mobile_multiplatformKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(feature:scope:)")));
- (Mobile_multiplatformKtor_client_authAuth *)prepareBlock:(void (^)(Mobile_multiplatformKtor_client_authAuth *))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) Mobile_multiplatformKtor_utilsAttributeKey<Mobile_multiplatformKtor_client_authAuth *> *key __attribute__((swift_name("key")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DefaultHttpEngineProvider")))
@interface Mobile_multiplatformDefaultHttpEngineProvider : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)defaultHttpEngineProvider __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformDefaultHttpEngineProvider *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKtor_client_coreHttpClientEngine>)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("JWTParser")))
@interface Mobile_multiplatformJWTParser : Mobile_multiplatformBase
- (instancetype)initWithIdToken:(NSString *)idToken __attribute__((swift_name("init(idToken:)"))) __attribute__((objc_designated_initializer));
- (NSString *)getClaimClaim:(NSString *)claim __attribute__((swift_name("getClaim(claim:)")));
@end;

__attribute__((swift_name("Parcelable")))
@protocol Mobile_multiplatformParcelable
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiBodyWithErrors")))
@interface Mobile_multiplatformApiBodyWithErrors : Mobile_multiplatformBase
- (instancetype)initWithErrors:(NSArray<Mobile_multiplatformApiError *> *)errors __attribute__((swift_name("init(errors:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiBodyWithErrorsCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<Mobile_multiplatformApiError *> *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformApiBodyWithErrors *)doCopyErrors:(NSArray<Mobile_multiplatformApiError *> *)errors __attribute__((swift_name("doCopy(errors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<Mobile_multiplatformApiError *> *errors __attribute__((swift_name("errors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiBodyWithErrors.Companion")))
@interface Mobile_multiplatformApiBodyWithErrorsCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiBodyWithErrorsCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiError")))
@interface Mobile_multiplatformApiError : Mobile_multiplatformBase
- (instancetype)initWithContext:(NSString *)context message:(NSString *)message description:(NSString *)description code:(int32_t)code __attribute__((swift_name("init(context:message:description:code:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiErrorCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (int32_t)component4 __attribute__((swift_name("component4()")));
- (Mobile_multiplatformApiError *)doCopyContext:(NSString *)context message:(NSString *)message description:(NSString *)description code:(int32_t)code __attribute__((swift_name("doCopy(context:message:description:code:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t code __attribute__((swift_name("code")));
@property (readonly) NSString *context __attribute__((swift_name("context")));
@property (readonly) NSString *description_ __attribute__((swift_name("description_")));
@property (readonly) NSString *message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiError.Companion")))
@interface Mobile_multiplatformApiErrorCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiErrorCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DateComponents")))
@interface Mobile_multiplatformDateComponents : Mobile_multiplatformBase <Mobile_multiplatformParcelable>
- (instancetype)initWithYear:(int32_t)year month:(int32_t)month day:(int32_t)day __attribute__((swift_name("init(year:month:day:)"))) __attribute__((objc_designated_initializer));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformDateComponents *)doCopyYear:(int32_t)year month:(int32_t)month day:(int32_t)day __attribute__((swift_name("doCopy(year:month:day:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t day __attribute__((swift_name("day")));
@property (readonly) int32_t month __attribute__((swift_name("month")));
@property (readonly) int32_t year __attribute__((swift_name("year")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiEntity")))
@interface Mobile_multiplatformApiEntity : Mobile_multiplatformBase
- (instancetype)initWithNameInUrl:(NSString *)nameInUrl __attribute__((swift_name("init(nameInUrl:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *nameInUrl __attribute__((swift_name("nameInUrl")));
@end;

__attribute__((swift_name("ApiSyncEntry")))
@protocol Mobile_multiplatformApiSyncEntry
@required
@property (readonly) NSString * _Nullable date __attribute__((swift_name("date")));
@property (readonly) Mobile_multiplatformInt * _Nullable remoteID __attribute__((swift_name("remoteID")));
@property Mobile_multiplatformInt * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ChangesMonitor")))
@interface Mobile_multiplatformChangesMonitor : Mobile_multiplatformBase
- (instancetype)initWithRepository:(id<Mobile_multiplatformWithLastChange>)repository __attribute__((swift_name("init(repository:)"))) __attribute__((objc_designated_initializer));
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow>)flow __attribute__((swift_name("flow()")));
@end;

__attribute__((swift_name("ConcreteEntitiesCreator")))
@protocol Mobile_multiplatformConcreteEntitiesCreator
@required
- (id<Mobile_multiplatformLocalIdentity>)identity __attribute__((swift_name("identity()")));
- (id<Mobile_multiplatformLocalScan>)scan __attribute__((swift_name("scan()")));
- (id<Mobile_multiplatformLocalSyncStatus>)syncStatus __attribute__((swift_name("syncStatus()")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol Mobile_multiplatformKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface Mobile_multiplatformKotlinEnum<E> : Mobile_multiplatformBase <Mobile_multiplatformKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeleteResult")))
@interface Mobile_multiplatformDeleteResult : Mobile_multiplatformKotlinEnum<Mobile_multiplatformDeleteResult *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Mobile_multiplatformDeleteResult *noChangesSent __attribute__((swift_name("noChangesSent")));
@property (class, readonly) Mobile_multiplatformDeleteResult *done __attribute__((swift_name("done")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformDeleteResult *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetAllResult")))
@interface Mobile_multiplatformGetAllResult : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("GetPageResults")))
@interface Mobile_multiplatformGetPageResults : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetPageResults.GotNoMorePages")))
@interface Mobile_multiplatformGetPageResultsGotNoMorePages : Mobile_multiplatformGetPageResults
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)gotNoMorePages __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformGetPageResultsGotNoMorePages *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetPageResults.GotPage")))
@interface Mobile_multiplatformGetPageResultsGotPage : Mobile_multiplatformGetPageResults
- (instancetype)initWithPageNumber:(int32_t)pageNumber resultsQuantity:(int32_t)resultsQuantity __attribute__((swift_name("init(pageNumber:resultsQuantity:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@property (readonly) int32_t pageNumber __attribute__((swift_name("pageNumber")));
@property (readonly) int32_t resultsQuantity __attribute__((swift_name("resultsQuantity")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitialSyncManager")))
@interface Mobile_multiplatformInitialSyncManager : Mobile_multiplatformBase
- (instancetype)initWithSyncStatusRepository:(Mobile_multiplatformLocalSyncStatusRepository *)syncStatusRepository syncStrategiesProvider:(id<Mobile_multiplatformSyncStrategiesProvider>)syncStrategiesProvider __attribute__((swift_name("init(syncStatusRepository:syncStrategiesProvider:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformInitialSyncManagerCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getStatusEntityToSyncName:(NSString *)entityToSyncName completionHandler:(void (^)(Mobile_multiplatformInitialSyncManagerFullSyncStatus * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getStatus(entityToSyncName:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)isInitialSyncDoneForEverythingWithCompletionHandler:(void (^)(Mobile_multiplatformBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("isInitialSyncDoneForEverything(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)onPageDownloadedEntityToSyncName:(NSString *)entityToSyncName pageNumber:(int32_t)pageNumber resultsQuantity:(int32_t)resultsQuantity initialSyncStatus:(Mobile_multiplatformInitialSyncManagerInitialSyncStatus *)initialSyncStatus completionHandler:(void (^)(Mobile_multiplatformBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("onPageDownloaded(entityToSyncName:pageNumber:resultsQuantity:initialSyncStatus:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitialSyncManager.Companion")))
@interface Mobile_multiplatformInitialSyncManagerCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformInitialSyncManagerCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitialSyncManager.FullSyncStatus")))
@interface Mobile_multiplatformInitialSyncManagerFullSyncStatus : Mobile_multiplatformBase
- (instancetype)initWithInitialSyncStatus:(Mobile_multiplatformInitialSyncManagerInitialSyncStatus *)initialSyncStatus syncIsFresh:(BOOL)syncIsFresh __attribute__((swift_name("init(initialSyncStatus:syncIsFresh:)"))) __attribute__((objc_designated_initializer));
- (Mobile_multiplatformInitialSyncManagerInitialSyncStatus *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformInitialSyncManagerFullSyncStatus *)doCopyInitialSyncStatus:(Mobile_multiplatformInitialSyncManagerInitialSyncStatus *)initialSyncStatus syncIsFresh:(BOOL)syncIsFresh __attribute__((swift_name("doCopy(initialSyncStatus:syncIsFresh:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformInitialSyncManagerInitialSyncStatus *initialSyncStatus __attribute__((swift_name("initialSyncStatus")));
@property (readonly) BOOL syncIsFresh __attribute__((swift_name("syncIsFresh")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitialSyncManager.InitialSyncStatus")))
@interface Mobile_multiplatformInitialSyncManagerInitialSyncStatus : Mobile_multiplatformBase
- (instancetype)initWithIsInitialSyncDone:(BOOL)isInitialSyncDone pageSize:(int32_t)pageSize lastSyncedPage:(int32_t)lastSyncedPage __attribute__((swift_name("init(isInitialSyncDone:pageSize:lastSyncedPage:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformInitialSyncManagerInitialSyncStatusCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformInitialSyncManagerInitialSyncStatus *)doCopyIsInitialSyncDone:(BOOL)isInitialSyncDone pageSize:(int32_t)pageSize lastSyncedPage:(int32_t)lastSyncedPage __attribute__((swift_name("doCopy(isInitialSyncDone:pageSize:lastSyncedPage:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isInitialSyncDone __attribute__((swift_name("isInitialSyncDone")));
@property (readonly) int32_t lastSyncedPage __attribute__((swift_name("lastSyncedPage")));
@property (readonly) int32_t pageSize __attribute__((swift_name("pageSize")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitialSyncManager.InitialSyncStatusCompanion")))
@interface Mobile_multiplatformInitialSyncManagerInitialSyncStatusCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformInitialSyncManagerInitialSyncStatusCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@property (readonly) Mobile_multiplatformInitialSyncManagerInitialSyncStatus *COMPLETE __attribute__((swift_name("COMPLETE")));
@property (readonly) Mobile_multiplatformInitialSyncManagerInitialSyncStatus *EMPTY __attribute__((swift_name("EMPTY")));
@end;

__attribute__((swift_name("SyncStrategiesProvider")))
@protocol Mobile_multiplatformSyncStrategiesProvider
@required
- (NSSet<id<Mobile_multiplatformSyncStrategy>> *)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("MainSyncStrategiesProvider")))
@interface Mobile_multiplatformMainSyncStrategiesProvider : Mobile_multiplatformBase <Mobile_multiplatformSyncStrategiesProvider>
- (instancetype)initWithIdentitiesSyncStrategy:(Mobile_multiplatformIdentitiesSyncStrategy *)identitiesSyncStrategy scansSyncStrategy:(Mobile_multiplatformScansSyncStrategy *)scansSyncStrategy __attribute__((swift_name("init(identitiesSyncStrategy:scansSyncStrategy:)"))) __attribute__((objc_designated_initializer));
- (NSSet<id<Mobile_multiplatformSyncStrategy>> *)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PullResult")))
@interface Mobile_multiplatformPullResult : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PushResult")))
@interface Mobile_multiplatformPushResult : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SetResult")))
@interface Mobile_multiplatformSetResult : Mobile_multiplatformKotlinEnum<Mobile_multiplatformSetResult *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Mobile_multiplatformSetResult *noChangesSent __attribute__((swift_name("noChangesSent")));
@property (class, readonly) Mobile_multiplatformSetResult *done __attribute__((swift_name("done")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformSetResult *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Sync")))
@interface Mobile_multiplatformSync : Mobile_multiplatformBase
- (instancetype)initWithSyncStrategiesProvider:(id<Mobile_multiplatformSyncStrategiesProvider>)syncStrategiesProvider syncExecutorFactory:(Mobile_multiplatformSyncExecutorFactory *)syncExecutorFactory syncStatusRepository:(Mobile_multiplatformLocalSyncStatusRepository *)syncStatusRepository __attribute__((swift_name("init(syncStrategiesProvider:syncExecutorFactory:syncStatusRepository:)"))) __attribute__((objc_designated_initializer));
- (void)start __attribute__((swift_name("start()")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) id<Mobile_multiplatformKotlinx_coroutines_coreFlow> initialSyncStatus __attribute__((swift_name("initialSyncStatus")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Sync.InitialSyncStatusInfo")))
@interface Mobile_multiplatformSyncInitialSyncStatusInfo : Mobile_multiplatformBase
- (instancetype)initWithOverallNormalizedProgress:(float)overallNormalizedProgress downloadedEntities:(int32_t)downloadedEntities totalEntities:(int32_t)totalEntities __attribute__((swift_name("init(overallNormalizedProgress:downloadedEntities:totalEntities:)"))) __attribute__((objc_designated_initializer));
- (BOOL)isDone __attribute__((swift_name("isDone()")));
@property (readonly) int32_t downloadedEntities __attribute__((swift_name("downloadedEntities")));
@property (readonly) float overallNormalizedProgress __attribute__((swift_name("overallNormalizedProgress")));
@property (readonly) int32_t totalEntities __attribute__((swift_name("totalEntities")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncAction")))
@interface Mobile_multiplatformSyncAction<Result> : Mobile_multiplatformBase
- (instancetype)initWithTaskName:(NSString *)taskName action:(id<Mobile_multiplatformSuspendAction>)action __attribute__((swift_name("init(taskName:action:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (id<Mobile_multiplatformSuspendAction>)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformSyncAction<Result> *)doCopyTaskName:(NSString *)taskName action:(id<Mobile_multiplatformSuspendAction>)action __attribute__((swift_name("doCopy(taskName:action:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<Mobile_multiplatformSuspendAction> action __attribute__((swift_name("action")));
@property (readonly) NSString *taskName __attribute__((swift_name("taskName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncActionsManager")))
@interface Mobile_multiplatformSyncActionsManager : Mobile_multiplatformBase
- (instancetype)initWithInitialSyncManager:(Mobile_multiplatformInitialSyncManager *)initialSyncManager syncPeriodsManager:(Mobile_multiplatformSyncPeriodsManager *)syncPeriodsManager __attribute__((swift_name("init(initialSyncManager:syncPeriodsManager:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)syncActionsForSyncStrategy:(id<Mobile_multiplatformSyncStrategy>)syncStrategy completionHandler:(void (^)(NSArray<Mobile_multiplatformSyncAction<id> *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("syncActionsFor(syncStrategy:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncExecutor")))
@interface Mobile_multiplatformSyncExecutor : Mobile_multiplatformBase
- (instancetype)initWithSyncStrategy:(id<Mobile_multiplatformSyncStrategy>)syncStrategy syncActionsManager:(Mobile_multiplatformSyncActionsManager *)syncActionsManager syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher problemTracker:(id<Mobile_multiplatformProblemTracker>)problemTracker __attribute__((swift_name("init(syncStrategy:syncActionsManager:syncDispatcher:problemTracker:)"))) __attribute__((objc_designated_initializer));
- (void)offerToStart __attribute__((swift_name("offerToStart()")));
- (void)offerToStartOnDbChangesCoroutineScope:(id<Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope>)coroutineScope __attribute__((swift_name("offerToStartOnDbChanges(coroutineScope:)")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) NSString *entityName __attribute__((swift_name("entityName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncExecutor.Factory")))
@interface Mobile_multiplatformSyncExecutorFactory : Mobile_multiplatformBase
- (instancetype)initWithSyncActionsManager:(Mobile_multiplatformSyncActionsManager *)syncActionsManager syncContext:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncContext problemTracker:(id<Mobile_multiplatformProblemTracker>)problemTracker __attribute__((swift_name("init(syncActionsManager:syncContext:problemTracker:)"))) __attribute__((objc_designated_initializer));
- (Mobile_multiplatformSyncExecutor *)createSyncStrategy:(id<Mobile_multiplatformSyncStrategy>)syncStrategy __attribute__((swift_name("create(syncStrategy:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncPeriodsManager")))
@interface Mobile_multiplatformSyncPeriodsManager : Mobile_multiplatformBase
- (instancetype)initWithSyncStatusRepository:(Mobile_multiplatformLocalSyncStatusRepository *)syncStatusRepository __attribute__((swift_name("init(syncStatusRepository:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformSyncPeriodsManagerCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getSyncPeriodEntityToSyncName:(NSString *)entityToSyncName completionHandler:(void (^)(Mobile_multiplatformSyncPeriodsManagerSyncPeriod * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getSyncPeriod(entityToSyncName:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)onSyncCompleteSyncPeriod:(Mobile_multiplatformSyncPeriodsManagerSyncPeriod *)syncPeriod completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("onSyncComplete(syncPeriod:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncPeriodsManager.Companion")))
@interface Mobile_multiplatformSyncPeriodsManagerCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformSyncPeriodsManagerCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncPeriodsManager.SyncPeriod")))
@interface Mobile_multiplatformSyncPeriodsManagerSyncPeriod : Mobile_multiplatformBase
- (instancetype)initWithStartSeconds:(int64_t)startSeconds endSeconds:(int64_t)endSeconds syncStatus:(id<Mobile_multiplatformLocalSyncStatus>)syncStatus __attribute__((swift_name("init(startSeconds:endSeconds:syncStatus:)"))) __attribute__((objc_designated_initializer));
- (NSString *)periodString __attribute__((swift_name("periodString()")));
@property (readonly) int64_t endSeconds __attribute__((swift_name("endSeconds")));
@property (readonly) int64_t startSeconds __attribute__((swift_name("startSeconds")));
@end;

__attribute__((swift_name("GetFromApi")))
@interface Mobile_multiplatformGetFromApi<T> : Mobile_multiplatformBase
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeRequest:(Mobile_multiplatformApiGetRequest *)request completionHandler:(void (^)(Mobile_multiplatformApiGetResponse<T> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(request:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiGetResponse<T> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((swift_name("PullFromApi")))
@interface Mobile_multiplatformPullFromApi<T> : Mobile_multiplatformBase
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeRequest:(Mobile_multiplatformApiPullRequest *)request completionHandler:(void (^)(Mobile_multiplatformApiPullResponse<T> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(request:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiPullResponse<T> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((swift_name("PushToApi")))
@interface Mobile_multiplatformPushToApi<T> : Mobile_multiplatformBase
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeRequest:(id<Mobile_multiplatformApiPushRequest>)request completionHandler:(void (^)(Mobile_multiplatformApiPushResponse<T> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(request:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiPushResponse<T> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetIdentitiesFromApi")))
@interface Mobile_multiplatformGetIdentitiesFromApi : Mobile_multiplatformGetFromApi<Mobile_multiplatformApiIdentity *>
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider __attribute__((swift_name("init(ktorProvider:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiGetResponse<Mobile_multiplatformApiIdentity *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PullIdentitiesFromApi")))
@interface Mobile_multiplatformPullIdentitiesFromApi : Mobile_multiplatformPullFromApi<Mobile_multiplatformApiIdentitySyncEntry *>
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider __attribute__((swift_name("init(ktorProvider:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiPullResponse<Mobile_multiplatformApiIdentitySyncEntry *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PushIdentitiesToApi")))
@interface Mobile_multiplatformPushIdentitiesToApi : Mobile_multiplatformPushToApi<Mobile_multiplatformApiIdentitySyncEntry *>
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider __attribute__((swift_name("init(ktorProvider:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiPushResponse<Mobile_multiplatformApiIdentitySyncEntry *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetScansFromApi")))
@interface Mobile_multiplatformGetScansFromApi : Mobile_multiplatformGetFromApi<Mobile_multiplatformApiScan *>
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider __attribute__((swift_name("init(ktorProvider:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiGetResponse<Mobile_multiplatformApiScan *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PullScansFromApi")))
@interface Mobile_multiplatformPullScansFromApi : Mobile_multiplatformPullFromApi<Mobile_multiplatformApiScanSyncEntry *>
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider __attribute__((swift_name("init(ktorProvider:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider entity:(Mobile_multiplatformApiEntity *)entity __attribute__((swift_name("init(ktorProvider:entity:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)requestClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client paramsApplier:(void (^)(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *))paramsApplier completionHandler:(void (^)(Mobile_multiplatformApiPullResponse<Mobile_multiplatformApiScanSyncEntry *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("request(client:paramsApplier:completionHandler:)")));
@end;

__attribute__((swift_name("AbleToGetChanges")))
@protocol Mobile_multiplatformAbleToGetChanges
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)changedWithCompletionHandler:(void (^)(NSMutableArray<id> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("changed(completionHandler:)")));
@end;

__attribute__((swift_name("CanFillLocalIdsAndExcludeUnchanged")))
@protocol Mobile_multiplatformCanFillLocalIdsAndExcludeUnchanged
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)fillLocalIdsAndExcludeSameOrLocallyChangedItems:(NSArray<id<Mobile_multiplatformBaseEntity>> *)items excludeLocallyChanged:(BOOL)excludeLocallyChanged completionHandler:(void (^)(NSArray<id<Mobile_multiplatformBaseEntity>> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("fillLocalIdsAndExcludeSameOrLocallyChanged(items:excludeLocallyChanged:completionHandler:)")));
@end;

__attribute__((swift_name("HasChangesFlow")))
@protocol Mobile_multiplatformHasChangesFlow
@required
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow>)changesFlow __attribute__((swift_name("changesFlow()")));
@end;

__attribute__((swift_name("LocalRepository")))
@protocol Mobile_multiplatformLocalRepository <Mobile_multiplatformCanFillLocalIdsAndExcludeUnchanged>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteFromDBItems:(NSArray<id<Mobile_multiplatformBaseEntity>> *)items completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("deleteFromDB(items:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteFromDBByRidItems:(NSArray<Mobile_multiplatformLong *> *)items completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("deleteFromDBByRid(items:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)findByRidsRids:(NSArray<Mobile_multiplatformLong *> *)rids completionHandler:(void (^)(NSArray<id<Mobile_multiplatformBaseEntity>> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("findByRids(rids:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)insertItems:(NSArray<id<Mobile_multiplatformBaseEntity>> *)items completionHandler:(void (^)(NSArray<Mobile_multiplatformLong *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("insert(items:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)markAsDeletedByRidsRids:(NSArray<Mobile_multiplatformLong *> *)rids completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("markAsDeletedByRids(rids:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)updateItems:(NSArray<id<Mobile_multiplatformBaseEntity>> *)items completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("update(items:completionHandler:)")));
@end;

__attribute__((swift_name("LocalRepositoryAdapter")))
@interface Mobile_multiplatformLocalRepositoryAdapter<LocalEntity, LocalEntityImpl> : Mobile_multiplatformBase <Mobile_multiplatformLocalRepository>
- (instancetype)initWithLocalImplRepository:(id<Mobile_multiplatformLocalRepository>)localImplRepository __attribute__((swift_name("init(localImplRepository:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteFromDBItems:(NSArray<LocalEntity> *)items completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("deleteFromDB(items:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteFromDBByRidItems:(NSArray<Mobile_multiplatformLong *> *)items completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("deleteFromDBByRid(items:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)findByRidsRids:(NSArray<Mobile_multiplatformLong *> *)rids completionHandler:(void (^)(NSArray<LocalEntity> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("findByRids(rids:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)insertItems:(NSArray<LocalEntity> *)items completionHandler:(void (^)(NSArray<Mobile_multiplatformLong *> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("insert(items:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)markAsDeletedByRidsRids:(NSArray<Mobile_multiplatformLong *> *)rids completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("markAsDeletedByRids(rids:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)updateItems:(NSArray<LocalEntity> *)items completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("update(items:completionHandler:)")));
@end;

__attribute__((swift_name("LocalRepositoryWithChanges")))
@protocol Mobile_multiplatformLocalRepositoryWithChanges <Mobile_multiplatformLocalRepository, Mobile_multiplatformAbleToGetChanges, Mobile_multiplatformHasChangesFlow>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LocalRepositoryWithChangesAdapter")))
@interface Mobile_multiplatformLocalRepositoryWithChangesAdapter<LocalEntity, LocalEntityImpl> : Mobile_multiplatformLocalRepositoryAdapter<LocalEntity, LocalEntityImpl> <Mobile_multiplatformLocalRepositoryWithChanges>
- (instancetype)initWithLocalImplRepository:(id<Mobile_multiplatformLocalRepositoryWithChanges>)localImplRepository __attribute__((swift_name("init(localImplRepository:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)changedWithCompletionHandler:(void (^)(NSMutableArray<LocalEntity> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("changed(completionHandler:)")));
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow>)changesFlow __attribute__((swift_name("changesFlow()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LocalSyncStatusRepository")))
@interface Mobile_multiplatformLocalSyncStatusRepository : Mobile_multiplatformBase
- (instancetype)initWithLocalSyncStatusStorage:(id<Mobile_multiplatformSyncStatusStorage>)localSyncStatusStorage concreteEntitiesCreator:(id<Mobile_multiplatformConcreteEntitiesCreator>)concreteEntitiesCreator __attribute__((swift_name("init(localSyncStatusStorage:concreteEntitiesCreator:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)clearAllWithCompletionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("clearAll(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)findByNameName:(NSString *)name completionHandler:(void (^)(id<Mobile_multiplatformLocalSyncStatus> _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("findByName(name:completionHandler:)")));
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow>)findByNamesNames:(NSArray<NSString *> *)names __attribute__((swift_name("findByNames(names:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getOrCreateName:(NSString *)name completionHandler:(void (^)(id<Mobile_multiplatformLocalSyncStatus> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getOrCreate(name:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)updateSyncStatus:(id<Mobile_multiplatformLocalSyncStatus>)syncStatus completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("update(syncStatus:completionHandler:)")));
@end;

__attribute__((swift_name("SyncStatusStorage")))
@protocol Mobile_multiplatformSyncStatusStorage
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)allWithCompletionHandler:(void (^)(NSArray<id<Mobile_multiplatformLocalSyncStatus>> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("all(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteAllWithCompletionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("deleteAll(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)insertSyncStatus:(id<Mobile_multiplatformLocalSyncStatus>)syncStatus completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("insert(syncStatus:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)updateSyncStatus:(id<Mobile_multiplatformLocalSyncStatus>)syncStatus completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("update(syncStatus:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncStatusStorageAdapter")))
@interface Mobile_multiplatformSyncStatusStorageAdapter<ConcreteSyncStatus> : Mobile_multiplatformBase <Mobile_multiplatformSyncStatusStorage>
- (instancetype)initWithConcreteSyncStatusStorage:(id<Mobile_multiplatformSyncStatusStorage>)concreteSyncStatusStorage __attribute__((swift_name("init(concreteSyncStatusStorage:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)allWithCompletionHandler:(void (^)(NSArray<id<Mobile_multiplatformLocalSyncStatus>> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("all(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)deleteAllWithCompletionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("deleteAll(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)insertSyncStatus:(id<Mobile_multiplatformLocalSyncStatus>)syncStatus completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("insert(syncStatus:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)updateSyncStatus:(id<Mobile_multiplatformLocalSyncStatus>)syncStatus completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("update(syncStatus:completionHandler:)")));
@end;

__attribute__((swift_name("WithLastChange")))
@protocol Mobile_multiplatformWithLastChange
@required
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow>)lastChangeS __attribute__((swift_name("lastChangeS()")));
@end;

__attribute__((swift_name("ProblemTracker")))
@protocol Mobile_multiplatformProblemTracker
@required
- (void)trackThrowable:(Mobile_multiplatformKotlinThrowable *)throwable __attribute__((swift_name("track(throwable:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CompositeProblemTracker")))
@interface Mobile_multiplatformCompositeProblemTracker : Mobile_multiplatformBase <Mobile_multiplatformProblemTracker>
- (instancetype)initWithTrackers:(id)trackers __attribute__((swift_name("init(trackers:)"))) __attribute__((objc_designated_initializer));
- (void)trackThrowable:(Mobile_multiplatformKotlinThrowable *)throwable __attribute__((swift_name("track(throwable:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FilteredProblemTracker")))
@interface Mobile_multiplatformFilteredProblemTracker : Mobile_multiplatformBase <Mobile_multiplatformProblemTracker>
- (instancetype)initWithTargetTracker:(id<Mobile_multiplatformProblemTracker>)targetTracker problemFilter:(Mobile_multiplatformProblemFilter *)problemFilter __attribute__((swift_name("init(targetTracker:problemFilter:)"))) __attribute__((objc_designated_initializer));
- (void)trackThrowable:(Mobile_multiplatformKotlinThrowable *)throwable __attribute__((swift_name("track(throwable:)")));
@end;

__attribute__((swift_name("ProblemFilter")))
@interface Mobile_multiplatformProblemFilter : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)needToTrackThrowable:(Mobile_multiplatformKotlinThrowable *)throwable __attribute__((swift_name("needToTrack(throwable:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ActionsWithEntities")))
@interface Mobile_multiplatformActionsWithEntities<LocalEntity> : Mobile_multiplatformBase
- (instancetype)initWithCreateUpdate:(NSArray<id> *)createUpdate delete:(NSArray<id> *)delete_ __attribute__((swift_name("init(createUpdate:delete:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSArray<id> *createUpdate __attribute__((swift_name("createUpdate")));
@property (readonly, getter=delete) NSArray<id> *delete_ __attribute__((swift_name("delete_")));
@end;

__attribute__((swift_name("ApiToLocalConverter")))
@protocol Mobile_multiplatformApiToLocalConverter
@required
- (id<Mobile_multiplatformBaseEntity>)apiToLocalApiEntity:(id _Nullable)apiEntity __attribute__((swift_name("apiToLocal(apiEntity:)")));
- (NSArray<id<Mobile_multiplatformBaseEntity>> *)apiToLocalApiEntities:(NSArray<id> *)apiEntities __attribute__((swift_name("apiToLocal(apiEntities:)")));
- (void)applyApiUpdatesToLocalApiEntity:(id _Nullable)apiEntity localEntity:(id<Mobile_multiplatformBaseEntity>)localEntity __attribute__((swift_name("applyApiUpdatesToLocal(apiEntity:localEntity:)")));
- (void)applySyncUpdatesToLocalApiEntity:(id _Nullable)apiEntity localEntity:(id<Mobile_multiplatformBaseEntity>)localEntity __attribute__((swift_name("applySyncUpdatesToLocal(apiEntity:localEntity:)")));
- (Mobile_multiplatformInt * _Nullable)getRemoteIdFromApiSyncEntityApiEntity:(id _Nullable)apiEntity __attribute__((swift_name("getRemoteIdFromApiSyncEntity(apiEntity:)")));
- (id<Mobile_multiplatformBaseEntity>)oneSyncEntityToLocalSyncEntity:(id _Nullable)syncEntity existingEntity:(id<Mobile_multiplatformBaseEntity> _Nullable)existingEntity __attribute__((swift_name("oneSyncEntityToLocal(syncEntity:existingEntity:)")));
- (Mobile_multiplatformActionsWithEntities<id<Mobile_multiplatformBaseEntity>> *)syncToLocalSyncEntities:(NSArray<id> *)syncEntities localEntities:(NSArray<id<Mobile_multiplatformBaseEntity>> *)localEntities __attribute__((swift_name("syncToLocal(syncEntities:localEntities:)")));
@end;

__attribute__((swift_name("ApiAndLocalConverter")))
@protocol Mobile_multiplatformApiAndLocalConverter <Mobile_multiplatformApiToLocalConverter>
@required
- (void)setStatusToSyncEntitySyncEntity:(id _Nullable)syncEntity status:(Mobile_multiplatformInt * _Nullable)status __attribute__((swift_name("setStatusToSyncEntity(syncEntity:status:)")));
- (id _Nullable)toSyncEntryLocalEntity:(id<Mobile_multiplatformBaseEntity>)localEntity __attribute__((swift_name("toSyncEntry(localEntity:)")));
@end;

__attribute__((swift_name("BaseConverter")))
@interface Mobile_multiplatformBaseConverter<ApiEntity, SyncEntity, LocalEntity> : Mobile_multiplatformBase <Mobile_multiplatformApiToLocalConverter>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (LocalEntity)apiToLocalApiEntity:(ApiEntity _Nullable)apiEntity __attribute__((swift_name("apiToLocal(apiEntity:)")));
- (NSArray<LocalEntity> *)apiToLocalApiEntities:(NSArray<id> *)apiEntities __attribute__((swift_name("apiToLocal(apiEntities:)")));
- (Mobile_multiplatformInt * _Nullable)getStatusFromSyncEntitySyncEntity:(SyncEntity _Nullable)syncEntity __attribute__((swift_name("getStatusFromSyncEntity(syncEntity:)")));
- (LocalEntity)doNewLocalEntity __attribute__((swift_name("doNewLocalEntity()")));
- (LocalEntity)oneSyncEntityToLocalSyncEntity:(SyncEntity _Nullable)syncEntity existingEntity:(LocalEntity _Nullable)existingEntity __attribute__((swift_name("oneSyncEntityToLocal(syncEntity:existingEntity:)")));
- (Mobile_multiplatformActionsWithEntities<LocalEntity> *)syncToLocalSyncEntities:(NSArray<id> *)syncEntities localEntities:(NSArray<LocalEntity> *)localEntities __attribute__((swift_name("syncToLocal(syncEntities:localEntities:)")));
@end;

__attribute__((swift_name("BaseSimpleConverter")))
@interface Mobile_multiplatformBaseSimpleConverter<ApiEntity, LocalEntity> : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (LocalEntity _Nullable)apiToLocalApiEntity_:(ApiEntity _Nullable)apiEntity __attribute__((swift_name("apiToLocal(apiEntity_:)")));
- (NSArray<id> *)apiToLocalApiEntities_:(NSArray<id> *)apiEntities __attribute__((swift_name("apiToLocal(apiEntities_:)")));
- (void)applyApiUpdatesToLocalApiEntity:(ApiEntity _Nullable)apiEntity localEntity_:(LocalEntity _Nullable)localEntity __attribute__((swift_name("applyApiUpdatesToLocal(apiEntity:localEntity_:)")));
- (LocalEntity _Nullable)doNewLocalEntity __attribute__((swift_name("doNewLocalEntity()")));
@end;

__attribute__((swift_name("BaseTwoWaysConverter")))
@interface Mobile_multiplatformBaseTwoWaysConverter<ApiEntity, SyncEntity, LocalEntity> : Mobile_multiplatformBaseConverter<ApiEntity, SyncEntity, LocalEntity> <Mobile_multiplatformApiAndLocalConverter>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (SyncEntity _Nullable)toSyncEntryLocalEntity:(LocalEntity)localEntity __attribute__((swift_name("toSyncEntry(localEntity:)")));
- (SyncEntity _Nullable)toSyncEntryExceptStatusLocalEntity:(LocalEntity)localEntity __attribute__((swift_name("toSyncEntryExceptStatus(localEntity:)")));
@end;

__attribute__((swift_name("BaseTwoWaysSimpleConverter")))
@interface Mobile_multiplatformBaseTwoWaysSimpleConverter<ApiEntity, LocalEntity> : Mobile_multiplatformBaseSimpleConverter<ApiEntity, LocalEntity>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (ApiEntity _Nullable)toApiEntityLocalEntity:(LocalEntity _Nullable)localEntity __attribute__((swift_name("toApiEntity(localEntity:)")));
@end;

__attribute__((swift_name("SimpleConverter")))
@interface Mobile_multiplatformSimpleConverter<ApiEntity, SyncEntity, LocalEntity> : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SimpleTwoWayConverter")))
@interface Mobile_multiplatformSimpleTwoWayConverter<ApiEntity, SyncEntity, LocalEntity> : Mobile_multiplatformSimpleConverter<ApiEntity, SyncEntity, LocalEntity>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IdentityConverter")))
@interface Mobile_multiplatformIdentityConverter : Mobile_multiplatformBaseTwoWaysConverter<Mobile_multiplatformApiIdentity *, Mobile_multiplatformApiIdentitySyncEntry *, id<Mobile_multiplatformLocalIdentity>>
- (instancetype)initWithDateTimeConverter:(Mobile_multiplatformApiDateTimeConverter *)dateTimeConverter justDateConverter:(Mobile_multiplatformApiDateOnlyConverter *)justDateConverter concreteEntitiesCreator:(id<Mobile_multiplatformConcreteEntitiesCreator>)concreteEntitiesCreator __attribute__((swift_name("init(dateTimeConverter:justDateConverter:concreteEntitiesCreator:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)applyApiUpdatesToLocalApiEntity:(Mobile_multiplatformApiIdentity *)apiEntity localEntity:(id<Mobile_multiplatformLocalIdentity>)localEntity __attribute__((swift_name("applyApiUpdatesToLocal(apiEntity:localEntity:)")));
- (void)applySyncUpdatesToLocalApiEntity:(Mobile_multiplatformApiIdentitySyncEntry *)apiEntity localEntity:(id<Mobile_multiplatformLocalIdentity>)localEntity __attribute__((swift_name("applySyncUpdatesToLocal(apiEntity:localEntity:)")));
- (Mobile_multiplatformInt * _Nullable)getRemoteIdFromApiSyncEntityApiEntity:(Mobile_multiplatformApiIdentitySyncEntry *)apiEntity __attribute__((swift_name("getRemoteIdFromApiSyncEntity(apiEntity:)")));
- (Mobile_multiplatformInt * _Nullable)getStatusFromSyncEntitySyncEntity:(Mobile_multiplatformApiIdentitySyncEntry *)syncEntity __attribute__((swift_name("getStatusFromSyncEntity(syncEntity:)")));
- (id<Mobile_multiplatformLocalIdentity>)doNewLocalEntity __attribute__((swift_name("doNewLocalEntity()")));
- (void)setStatusToSyncEntitySyncEntity:(Mobile_multiplatformApiIdentitySyncEntry *)syncEntity status:(Mobile_multiplatformInt * _Nullable)status __attribute__((swift_name("setStatusToSyncEntity(syncEntity:status:)")));
- (Mobile_multiplatformApiIdentitySyncEntry *)toSyncEntryExceptStatusLocalEntity:(id<Mobile_multiplatformLocalIdentity>)localEntity __attribute__((swift_name("toSyncEntryExceptStatus(localEntity:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ScanConverter")))
@interface Mobile_multiplatformScanConverter : Mobile_multiplatformBaseConverter<Mobile_multiplatformApiScan *, Mobile_multiplatformApiScanSyncEntry *, id<Mobile_multiplatformLocalScan>>
- (instancetype)initWithDateTimeConverter:(Mobile_multiplatformDateTimeConverter *)dateTimeConverter concreteEntitiesCreator:(id<Mobile_multiplatformConcreteEntitiesCreator>)concreteEntitiesCreator __attribute__((swift_name("init(dateTimeConverter:concreteEntitiesCreator:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)applyApiUpdatesToLocalApiEntity:(Mobile_multiplatformApiScan *)apiEntity localEntity:(id<Mobile_multiplatformLocalScan>)localEntity __attribute__((swift_name("applyApiUpdatesToLocal(apiEntity:localEntity:)")));
- (void)applySyncUpdatesToLocalApiEntity:(Mobile_multiplatformApiScanSyncEntry *)apiEntity localEntity:(id<Mobile_multiplatformLocalScan>)localEntity __attribute__((swift_name("applySyncUpdatesToLocal(apiEntity:localEntity:)")));
- (Mobile_multiplatformInt * _Nullable)getRemoteIdFromApiSyncEntityApiEntity:(Mobile_multiplatformApiScanSyncEntry *)apiEntity __attribute__((swift_name("getRemoteIdFromApiSyncEntity(apiEntity:)")));
- (Mobile_multiplatformInt * _Nullable)getStatusFromSyncEntitySyncEntity:(Mobile_multiplatformApiScanSyncEntry *)syncEntity __attribute__((swift_name("getStatusFromSyncEntity(syncEntity:)")));
- (id<Mobile_multiplatformLocalScan>)doNewLocalEntity __attribute__((swift_name("doNewLocalEntity()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiDatesModule")))
@interface Mobile_multiplatformApiDatesModule : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)apiDatesModule __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiDatesModule *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformApiDateOnlyConverter *dateOnlyConverter __attribute__((swift_name("dateOnlyConverter")));
@property (readonly) Mobile_multiplatformApiDateTimeConverter *dateTimeConverter __attribute__((swift_name("dateTimeConverter")));
@property (readonly) Mobile_multiplatformApiTimeOnlyConverter *timeOnlyConverter __attribute__((swift_name("timeOnlyConverter")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConvertersModule")))
@interface Mobile_multiplatformConvertersModule : Mobile_multiplatformBase
- (instancetype)initWithApiDatesModule:(Mobile_multiplatformApiDatesModule *)apiDatesModule concreteEntitiesCreator:(id<Mobile_multiplatformConcreteEntitiesCreator>)concreteEntitiesCreator __attribute__((swift_name("init(apiDatesModule:concreteEntitiesCreator:)"))) __attribute__((objc_designated_initializer));
@property (readonly) Mobile_multiplatformIdentityConverter *identityConverter __attribute__((swift_name("identityConverter")));
@property (readonly) Mobile_multiplatformScanConverter *scanConverter __attribute__((swift_name("scanConverter")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncComponent")))
@interface Mobile_multiplatformSyncComponent : Mobile_multiplatformBase
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider concreteEntitiesCreator:(id<Mobile_multiplatformConcreteEntitiesCreator>)concreteEntitiesCreator identitiesRepository:(id<Mobile_multiplatformLocalRepositoryWithChanges>)identitiesRepository scansRepository:(id<Mobile_multiplatformLocalRepository>)scansRepository localSyncStatusStorage:(id<Mobile_multiplatformSyncStatusStorage>)localSyncStatusStorage targetProblemTracker:(id<Mobile_multiplatformProblemTracker>)targetProblemTracker problemTrackerFilter:(Mobile_multiplatformProblemFilter *)problemTrackerFilter substitutedSyncStrategiesProvider:(id<Mobile_multiplatformSyncStrategiesProvider> _Nullable)substitutedSyncStrategiesProvider __attribute__((swift_name("init(ktorProvider:concreteEntitiesCreator:identitiesRepository:scansRepository:localSyncStatusStorage:targetProblemTracker:problemTrackerFilter:substitutedSyncStrategiesProvider:)"))) __attribute__((objc_designated_initializer));
@property (readonly) Mobile_multiplatformApiDatesModule *apiDatesModule __attribute__((swift_name("apiDatesModule")));
@property (readonly) Mobile_multiplatformConvertersModule *convertersModule __attribute__((swift_name("convertersModule")));
@property (readonly) Mobile_multiplatformGetIdentitiesFromApi *getIdentitiesFromApi __attribute__((swift_name("getIdentitiesFromApi")));
@property (readonly) Mobile_multiplatformGetScansFromApi *getScansFromApi __attribute__((swift_name("getScansFromApi")));
@property (readonly) Mobile_multiplatformIdentitiesSyncStrategy *identitiesSyncStrategy __attribute__((swift_name("identitiesSyncStrategy")));
@property (readonly) Mobile_multiplatformInitialSyncManager *initialSyncManager __attribute__((swift_name("initialSyncManager")));
@property (readonly) Mobile_multiplatformLocalSyncStatusRepository *localSyncStatusRepository __attribute__((swift_name("localSyncStatusRepository")));
@property (readonly) Mobile_multiplatformPullIdentitiesFromApi *pullIdentitiesFromApi __attribute__((swift_name("pullIdentitiesFromApi")));
@property (readonly) Mobile_multiplatformPullScansFromApi *pullScansFromApi __attribute__((swift_name("pullScansFromApi")));
@property (readonly) Mobile_multiplatformPushIdentitiesToApi *pushIdentitiesToApi __attribute__((swift_name("pushIdentitiesToApi")));
@property (readonly) Mobile_multiplatformScansSyncStrategy *scansSyncStrategy __attribute__((swift_name("scansSyncStrategy")));
@property (readonly) Mobile_multiplatformSync *sync __attribute__((swift_name("sync")));
@property (readonly) Mobile_multiplatformSyncActionsManager *syncActionManager __attribute__((swift_name("syncActionManager")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *syncDispatcher __attribute__((swift_name("syncDispatcher")));
@property (readonly) Mobile_multiplatformSyncExecutorFactory *syncExecutorsFactory __attribute__((swift_name("syncExecutorsFactory")));
@property (readonly) Mobile_multiplatformSyncPeriodsManager *syncPeriodsManager __attribute__((swift_name("syncPeriodsManager")));
@property (readonly) id<Mobile_multiplatformSyncStrategiesProvider> syncStrategiesProvider __attribute__((swift_name("syncStrategiesProvider")));
@end;

__attribute__((swift_name("LocalSyncStatus")))
@protocol Mobile_multiplatformLocalSyncStatus
@required
@property Mobile_multiplatformInitialSyncManagerInitialSyncStatus *initialSyncStatus __attribute__((swift_name("initialSyncStatus")));
@property NSString *name __attribute__((swift_name("name")));
@property int64_t syncedUntilTimestampS __attribute__((swift_name("syncedUntilTimestampS")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiGetRequest")))
@interface Mobile_multiplatformApiGetRequest : Mobile_multiplatformBase
- (instancetype)initWithStart:(NSString * _Nullable)start end:(NSString * _Nullable)end ids:(NSMutableArray<Mobile_multiplatformInt *> * _Nullable)ids children:(Mobile_multiplatformInt * _Nullable)children orderBy:(NSMutableArray<NSString *> * _Nullable)orderBy limit:(Mobile_multiplatformInt * _Nullable)limit offset:(Mobile_multiplatformInt * _Nullable)offset perPage:(Mobile_multiplatformInt * _Nullable)perPage page:(Mobile_multiplatformInt * _Nullable)page __attribute__((swift_name("init(start:end:ids:children:orderBy:limit:offset:perPage:page:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiGetRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSMutableArray<Mobile_multiplatformInt *> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSMutableArray<NSString *> * _Nullable)component5 __attribute__((swift_name("component5()")));
- (Mobile_multiplatformInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (Mobile_multiplatformInt * _Nullable)component7 __attribute__((swift_name("component7()")));
- (Mobile_multiplatformInt * _Nullable)component8 __attribute__((swift_name("component8()")));
- (Mobile_multiplatformInt * _Nullable)component9 __attribute__((swift_name("component9()")));
- (Mobile_multiplatformApiGetRequest *)doCopyStart:(NSString * _Nullable)start end:(NSString * _Nullable)end ids:(NSMutableArray<Mobile_multiplatformInt *> * _Nullable)ids children:(Mobile_multiplatformInt * _Nullable)children orderBy:(NSMutableArray<NSString *> * _Nullable)orderBy limit:(Mobile_multiplatformInt * _Nullable)limit offset:(Mobile_multiplatformInt * _Nullable)offset perPage:(Mobile_multiplatformInt * _Nullable)perPage page:(Mobile_multiplatformInt * _Nullable)page __attribute__((swift_name("doCopy(start:end:ids:children:orderBy:limit:offset:perPage:page:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformInt * _Nullable children __attribute__((swift_name("children")));
@property (readonly) NSString * _Nullable end __attribute__((swift_name("end")));
@property (readonly) NSMutableArray<Mobile_multiplatformInt *> * _Nullable ids __attribute__((swift_name("ids")));
@property (readonly) Mobile_multiplatformInt * _Nullable limit __attribute__((swift_name("limit")));
@property (readonly) Mobile_multiplatformInt * _Nullable offset __attribute__((swift_name("offset")));
@property (readonly) NSMutableArray<NSString *> * _Nullable orderBy __attribute__((swift_name("orderBy")));
@property (readonly) Mobile_multiplatformInt * _Nullable page __attribute__((swift_name("page")));
@property (readonly) Mobile_multiplatformInt * _Nullable perPage __attribute__((swift_name("perPage")));
@property (readonly) NSString * _Nullable start __attribute__((swift_name("start")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiGetRequest.Companion")))
@interface Mobile_multiplatformApiGetRequestCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiGetRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiGetResponse")))
@interface Mobile_multiplatformApiGetResponse<T> : Mobile_multiplatformBase
- (instancetype)initWithData:(NSArray<id> *)data errors:(NSArray<Mobile_multiplatformApiError *> * _Nullable)errors pagination:(Mobile_multiplatformApiPagination * _Nullable)pagination __attribute__((swift_name("init(data:errors:pagination:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiGetResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<id> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<Mobile_multiplatformApiError *> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformApiPagination * _Nullable)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformApiGetResponse<T> *)doCopyData:(NSArray<id> *)data errors:(NSArray<Mobile_multiplatformApiError *> * _Nullable)errors pagination:(Mobile_multiplatformApiPagination * _Nullable)pagination __attribute__((swift_name("doCopy(data:errors:pagination:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<id> *data __attribute__((swift_name("data")));
@property (readonly) NSArray<Mobile_multiplatformApiError *> * _Nullable errors __attribute__((swift_name("errors")));
@property (readonly) Mobile_multiplatformApiPagination * _Nullable pagination __attribute__((swift_name("pagination")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiGetResponseCompanion")))
@interface Mobile_multiplatformApiGetResponseCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiGetResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(Mobile_multiplatformKotlinArray<id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializerTypeSerial0:(id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)typeSerial0 __attribute__((swift_name("serializer(typeSerial0:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPagination")))
@interface Mobile_multiplatformApiPagination : Mobile_multiplatformBase
- (instancetype)initWithCurrentPage:(Mobile_multiplatformInt * _Nullable)currentPage perPage:(Mobile_multiplatformInt * _Nullable)perPage count:(Mobile_multiplatformInt * _Nullable)count pages:(Mobile_multiplatformInt * _Nullable)pages __attribute__((swift_name("init(currentPage:perPage:count:pages:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiPaginationCompanion *companion __attribute__((swift_name("companion")));
- (Mobile_multiplatformInt * _Nullable)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformInt * _Nullable)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformInt * _Nullable)component4 __attribute__((swift_name("component4()")));
- (Mobile_multiplatformApiPagination *)doCopyCurrentPage:(Mobile_multiplatformInt * _Nullable)currentPage perPage:(Mobile_multiplatformInt * _Nullable)perPage count:(Mobile_multiplatformInt * _Nullable)count pages:(Mobile_multiplatformInt * _Nullable)pages __attribute__((swift_name("doCopy(currentPage:perPage:count:pages:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformInt * _Nullable count __attribute__((swift_name("count")));
@property (readonly) Mobile_multiplatformInt * _Nullable currentPage __attribute__((swift_name("currentPage")));
@property (readonly) Mobile_multiplatformInt * _Nullable pages __attribute__((swift_name("pages")));
@property (readonly) Mobile_multiplatformInt * _Nullable perPage __attribute__((swift_name("perPage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPagination.Companion")))
@interface Mobile_multiplatformApiPaginationCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiPaginationCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPullRequest")))
@interface Mobile_multiplatformApiPullRequest : Mobile_multiplatformBase
- (instancetype)initWithPeriod:(NSString *)period __attribute__((swift_name("init(period:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiPullRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformApiPullRequest *)doCopyPeriod:(NSString *)period __attribute__((swift_name("doCopy(period:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *period __attribute__((swift_name("period")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPullRequest.Companion")))
@interface Mobile_multiplatformApiPullRequestCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiPullRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPullResponse")))
@interface Mobile_multiplatformApiPullResponse<T> : Mobile_multiplatformBase
- (instancetype)initWithData:(NSArray<T> *)data errors:(NSArray<Mobile_multiplatformApiError *> * _Nullable)errors __attribute__((swift_name("init(data:errors:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiPullResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<T> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<Mobile_multiplatformApiError *> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformApiPullResponse<T> *)doCopyData:(NSArray<T> *)data errors:(NSArray<Mobile_multiplatformApiError *> * _Nullable)errors __attribute__((swift_name("doCopy(data:errors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<T> *data __attribute__((swift_name("data")));
@property (readonly) NSArray<Mobile_multiplatformApiError *> * _Nullable errors __attribute__((swift_name("errors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPullResponseCompanion")))
@interface Mobile_multiplatformApiPullResponseCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiPullResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(Mobile_multiplatformKotlinArray<id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializerTypeSerial0:(id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)typeSerial0 __attribute__((swift_name("serializer(typeSerial0:)")));
@end;

__attribute__((swift_name("ApiPushRequest")))
@protocol Mobile_multiplatformApiPushRequest
@required
@property (readonly) NSArray<id> *data __attribute__((swift_name("data")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPushResponse")))
@interface Mobile_multiplatformApiPushResponse<T> : Mobile_multiplatformBase
- (instancetype)initWithData:(NSArray<T> *)data errors:(NSArray<Mobile_multiplatformApiError *> * _Nullable)errors __attribute__((swift_name("init(data:errors:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiPushResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<T> *)component1 __attribute__((swift_name("component1()")));
- (NSArray<Mobile_multiplatformApiError *> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformApiPushResponse<T> *)doCopyData:(NSArray<T> *)data errors:(NSArray<Mobile_multiplatformApiError *> * _Nullable)errors __attribute__((swift_name("doCopy(data:errors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<T> *data __attribute__((swift_name("data")));
@property (readonly) NSArray<Mobile_multiplatformApiError *> * _Nullable errors __attribute__((swift_name("errors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPushResponseCompanion")))
@interface Mobile_multiplatformApiPushResponseCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiPushResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializerTypeParamsSerializers:(Mobile_multiplatformKotlinArray<id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>> *)typeParamsSerializers __attribute__((swift_name("serializer(typeParamsSerializers:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializerTypeSerial0:(id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)typeSerial0 __attribute__((swift_name("serializer(typeSerial0:)")));
@end;

__attribute__((swift_name("ApiIdentityBase")))
@protocol Mobile_multiplatformApiIdentityBase
@required
@property (readonly) NSString * _Nullable address __attribute__((swift_name("address")));
@property (readonly) Mobile_multiplatformInt * _Nullable ban __attribute__((swift_name("ban")));
@property (readonly) NSString * _Nullable banEndAt __attribute__((swift_name("banEndAt")));
@property (readonly) NSString * _Nullable banStartAt __attribute__((swift_name("banStartAt")));
@property (readonly) NSString * _Nullable bannedBy __attribute__((swift_name("bannedBy")));
@property (readonly) NSString * _Nullable birthday __attribute__((swift_name("birthday")));
@property (readonly) NSString * _Nullable city __attribute__((swift_name("city")));
@property (readonly) NSString * _Nullable createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString * _Nullable expiresAt __attribute__((swift_name("expiresAt")));
@property (readonly) NSString * _Nullable eyeColor __attribute__((swift_name("eyeColor")));
@property (readonly) NSString * _Nullable firstName __attribute__((swift_name("firstName")));
@property (readonly) NSString * _Nullable fullName __attribute__((swift_name("fullName")));
@property (readonly) NSString * _Nullable gender __attribute__((swift_name("gender")));
@property (readonly) NSString * _Nullable hairColor __attribute__((swift_name("hairColor")));
@property (readonly) NSString * _Nullable height __attribute__((swift_name("height")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable issuedAt __attribute__((swift_name("issuedAt")));
@property (readonly) NSString * _Nullable lastName __attribute__((swift_name("lastName")));
@property (readonly) NSString * _Nullable lastScannedAt __attribute__((swift_name("lastScannedAt")));
@property (readonly) NSString * _Nullable licenseNumber __attribute__((swift_name("licenseNumber")));
@property (readonly) NSString * _Nullable middleName __attribute__((swift_name("middleName")));
@property (readonly) NSString * _Nullable orgID __attribute__((swift_name("orgID")));
@property (readonly) Mobile_multiplatformInt * _Nullable orientation __attribute__((swift_name("orientation")));
@property (readonly) NSString * _Nullable postalCode __attribute__((swift_name("postalCode")));
@property (readonly) Mobile_multiplatformInt * _Nullable scansInPeriod __attribute__((swift_name("scansInPeriod")));
@property (readonly) NSString * _Nullable state __attribute__((swift_name("state")));
@property (readonly) NSString * _Nullable street __attribute__((swift_name("street")));
@property (readonly) NSString * _Nullable uid __attribute__((swift_name("uid")));
@property (readonly) NSString *userID __attribute__((swift_name("userID")));
@property (readonly) Mobile_multiplatformInt * _Nullable vip __attribute__((swift_name("vip")));
@property (readonly) NSString * _Nullable vipBy __attribute__((swift_name("vipBy")));
@property (readonly) NSString * _Nullable vipEndAt __attribute__((swift_name("vipEndAt")));
@property (readonly) NSString * _Nullable vipStartAt __attribute__((swift_name("vipStartAt")));
@property (readonly) Mobile_multiplatformInt * _Nullable visits __attribute__((swift_name("visits")));
@property (readonly) NSString * _Nullable weight __attribute__((swift_name("weight")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiIdentity")))
@interface Mobile_multiplatformApiIdentity : Mobile_multiplatformBase <Mobile_multiplatformApiIdentityBase>
- (instancetype)initWithApiIdentityBase:(id<Mobile_multiplatformApiIdentityBase>)apiIdentityBase __attribute__((swift_name("init(apiIdentityBase:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithId:(int32_t)id uid:(NSString * _Nullable)uid userID:(NSString *)userID orgID:(NSString * _Nullable)orgID orientation:(Mobile_multiplatformInt * _Nullable)orientation licenseNumber:(NSString * _Nullable)licenseNumber birthday:(NSString * _Nullable)birthday issuedAt:(NSString * _Nullable)issuedAt expiresAt:(NSString * _Nullable)expiresAt height:(NSString * _Nullable)height weight:(NSString * _Nullable)weight eyeColor:(NSString * _Nullable)eyeColor hairColor:(NSString * _Nullable)hairColor address:(NSString * _Nullable)address street:(NSString * _Nullable)street city:(NSString * _Nullable)city postalCode:(NSString * _Nullable)postalCode fullName:(NSString * _Nullable)fullName firstName:(NSString * _Nullable)firstName middleName:(NSString * _Nullable)middleName lastName:(NSString * _Nullable)lastName gender:(NSString * _Nullable)gender ban:(Mobile_multiplatformInt * _Nullable)ban bannedBy:(NSString * _Nullable)bannedBy banStartAt:(NSString * _Nullable)banStartAt banEndAt:(NSString * _Nullable)banEndAt vip:(Mobile_multiplatformInt * _Nullable)vip vipBy:(NSString * _Nullable)vipBy vipStartAt:(NSString * _Nullable)vipStartAt vipEndAt:(NSString * _Nullable)vipEndAt visits:(Mobile_multiplatformInt * _Nullable)visits state:(NSString * _Nullable)state createdAt:(NSString * _Nullable)createdAt lastScannedAt:(NSString * _Nullable)lastScannedAt scansInPeriod:(Mobile_multiplatformInt * _Nullable)scansInPeriod __attribute__((swift_name("init(id:uid:userID:orgID:orientation:licenseNumber:birthday:issuedAt:expiresAt:height:weight:eyeColor:hairColor:address:street:city:postalCode:fullName:firstName:middleName:lastName:gender:ban:bannedBy:banStartAt:banEndAt:vip:vipBy:vipStartAt:vipEndAt:visits:state:createdAt:lastScannedAt:scansInPeriod:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiIdentityCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString * _Nullable)component12 __attribute__((swift_name("component12()")));
- (NSString * _Nullable)component13 __attribute__((swift_name("component13()")));
- (NSString * _Nullable)component14 __attribute__((swift_name("component14()")));
- (NSString * _Nullable)component15 __attribute__((swift_name("component15()")));
- (NSString * _Nullable)component16 __attribute__((swift_name("component16()")));
- (NSString * _Nullable)component17 __attribute__((swift_name("component17()")));
- (NSString * _Nullable)component18 __attribute__((swift_name("component18()")));
- (NSString * _Nullable)component19 __attribute__((swift_name("component19()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component20 __attribute__((swift_name("component20()")));
- (NSString * _Nullable)component21 __attribute__((swift_name("component21()")));
- (NSString * _Nullable)component22 __attribute__((swift_name("component22()")));
- (Mobile_multiplatformInt * _Nullable)component23 __attribute__((swift_name("component23()")));
- (NSString * _Nullable)component24 __attribute__((swift_name("component24()")));
- (NSString * _Nullable)component25 __attribute__((swift_name("component25()")));
- (NSString * _Nullable)component26 __attribute__((swift_name("component26()")));
- (Mobile_multiplatformInt * _Nullable)component27 __attribute__((swift_name("component27()")));
- (NSString * _Nullable)component28 __attribute__((swift_name("component28()")));
- (NSString * _Nullable)component29 __attribute__((swift_name("component29()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component30 __attribute__((swift_name("component30()")));
- (Mobile_multiplatformInt * _Nullable)component31 __attribute__((swift_name("component31()")));
- (NSString * _Nullable)component32 __attribute__((swift_name("component32()")));
- (NSString * _Nullable)component33 __attribute__((swift_name("component33()")));
- (NSString * _Nullable)component34 __attribute__((swift_name("component34()")));
- (Mobile_multiplatformInt * _Nullable)component35 __attribute__((swift_name("component35()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (Mobile_multiplatformInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (Mobile_multiplatformApiIdentity *)doCopyId:(int32_t)id uid:(NSString * _Nullable)uid userID:(NSString *)userID orgID:(NSString * _Nullable)orgID orientation:(Mobile_multiplatformInt * _Nullable)orientation licenseNumber:(NSString * _Nullable)licenseNumber birthday:(NSString * _Nullable)birthday issuedAt:(NSString * _Nullable)issuedAt expiresAt:(NSString * _Nullable)expiresAt height:(NSString * _Nullable)height weight:(NSString * _Nullable)weight eyeColor:(NSString * _Nullable)eyeColor hairColor:(NSString * _Nullable)hairColor address:(NSString * _Nullable)address street:(NSString * _Nullable)street city:(NSString * _Nullable)city postalCode:(NSString * _Nullable)postalCode fullName:(NSString * _Nullable)fullName firstName:(NSString * _Nullable)firstName middleName:(NSString * _Nullable)middleName lastName:(NSString * _Nullable)lastName gender:(NSString * _Nullable)gender ban:(Mobile_multiplatformInt * _Nullable)ban bannedBy:(NSString * _Nullable)bannedBy banStartAt:(NSString * _Nullable)banStartAt banEndAt:(NSString * _Nullable)banEndAt vip:(Mobile_multiplatformInt * _Nullable)vip vipBy:(NSString * _Nullable)vipBy vipStartAt:(NSString * _Nullable)vipStartAt vipEndAt:(NSString * _Nullable)vipEndAt visits:(Mobile_multiplatformInt * _Nullable)visits state:(NSString * _Nullable)state createdAt:(NSString * _Nullable)createdAt lastScannedAt:(NSString * _Nullable)lastScannedAt scansInPeriod:(Mobile_multiplatformInt * _Nullable)scansInPeriod __attribute__((swift_name("doCopy(id:uid:userID:orgID:orientation:licenseNumber:birthday:issuedAt:expiresAt:height:weight:eyeColor:hairColor:address:street:city:postalCode:fullName:firstName:middleName:lastName:gender:ban:bannedBy:banStartAt:banEndAt:vip:vipBy:vipStartAt:vipEndAt:visits:state:createdAt:lastScannedAt:scansInPeriod:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable address __attribute__((swift_name("address")));
@property (readonly) Mobile_multiplatformInt * _Nullable ban __attribute__((swift_name("ban")));
@property (readonly) NSString * _Nullable banEndAt __attribute__((swift_name("banEndAt")));
@property (readonly) NSString * _Nullable banStartAt __attribute__((swift_name("banStartAt")));
@property (readonly) NSString * _Nullable bannedBy __attribute__((swift_name("bannedBy")));
@property (readonly) NSString * _Nullable birthday __attribute__((swift_name("birthday")));
@property (readonly) NSString * _Nullable city __attribute__((swift_name("city")));
@property (readonly) NSString * _Nullable createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString * _Nullable expiresAt __attribute__((swift_name("expiresAt")));
@property (readonly) NSString * _Nullable eyeColor __attribute__((swift_name("eyeColor")));
@property (readonly) NSString * _Nullable firstName __attribute__((swift_name("firstName")));
@property (readonly) NSString * _Nullable fullName __attribute__((swift_name("fullName")));
@property (readonly) NSString * _Nullable gender __attribute__((swift_name("gender")));
@property (readonly) NSString * _Nullable hairColor __attribute__((swift_name("hairColor")));
@property (readonly) NSString * _Nullable height __attribute__((swift_name("height")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable issuedAt __attribute__((swift_name("issuedAt")));
@property (readonly) NSString * _Nullable lastName __attribute__((swift_name("lastName")));
@property (readonly) NSString * _Nullable lastScannedAt __attribute__((swift_name("lastScannedAt")));
@property (readonly) NSString * _Nullable licenseNumber __attribute__((swift_name("licenseNumber")));
@property (readonly) NSString * _Nullable middleName __attribute__((swift_name("middleName")));
@property (readonly) NSString * _Nullable orgID __attribute__((swift_name("orgID")));
@property (readonly) Mobile_multiplatformInt * _Nullable orientation __attribute__((swift_name("orientation")));
@property (readonly) NSString * _Nullable postalCode __attribute__((swift_name("postalCode")));
@property (readonly) Mobile_multiplatformInt * _Nullable scansInPeriod __attribute__((swift_name("scansInPeriod")));
@property (readonly) NSString * _Nullable state __attribute__((swift_name("state")));
@property (readonly) NSString * _Nullable street __attribute__((swift_name("street")));
@property (readonly) NSString * _Nullable uid __attribute__((swift_name("uid")));
@property (readonly) NSString *userID __attribute__((swift_name("userID")));
@property (readonly) Mobile_multiplatformInt * _Nullable vip __attribute__((swift_name("vip")));
@property (readonly) NSString * _Nullable vipBy __attribute__((swift_name("vipBy")));
@property (readonly) NSString * _Nullable vipEndAt __attribute__((swift_name("vipEndAt")));
@property (readonly) NSString * _Nullable vipStartAt __attribute__((swift_name("vipStartAt")));
@property (readonly) Mobile_multiplatformInt * _Nullable visits __attribute__((swift_name("visits")));
@property (readonly) NSString * _Nullable weight __attribute__((swift_name("weight")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiIdentity.Companion")))
@interface Mobile_multiplatformApiIdentityCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiIdentityCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiIdentitySyncEntry")))
@interface Mobile_multiplatformApiIdentitySyncEntry : Mobile_multiplatformBase <Mobile_multiplatformApiIdentityBase, Mobile_multiplatformApiSyncEntry>
- (instancetype)initWithId:(int32_t)id uid:(NSString * _Nullable)uid userID:(NSString *)userID orgID:(NSString * _Nullable)orgID orientation:(Mobile_multiplatformInt * _Nullable)orientation licenseNumber:(NSString * _Nullable)licenseNumber birthday:(NSString * _Nullable)birthday issuedAt:(NSString * _Nullable)issuedAt expiresAt:(NSString * _Nullable)expiresAt height:(NSString * _Nullable)height weight:(NSString * _Nullable)weight eyeColor:(NSString * _Nullable)eyeColor hairColor:(NSString * _Nullable)hairColor address:(NSString * _Nullable)address street:(NSString * _Nullable)street city:(NSString * _Nullable)city postalCode:(NSString * _Nullable)postalCode fullName:(NSString * _Nullable)fullName firstName:(NSString * _Nullable)firstName middleName:(NSString * _Nullable)middleName lastName:(NSString * _Nullable)lastName gender:(NSString * _Nullable)gender ban:(Mobile_multiplatformInt * _Nullable)ban bannedBy:(NSString * _Nullable)bannedBy banStartAt:(NSString * _Nullable)banStartAt banEndAt:(NSString * _Nullable)banEndAt vip:(Mobile_multiplatformInt * _Nullable)vip vipBy:(NSString * _Nullable)vipBy vipStartAt:(NSString * _Nullable)vipStartAt vipEndAt:(NSString * _Nullable)vipEndAt visits:(Mobile_multiplatformInt * _Nullable)visits state:(NSString * _Nullable)state createdAt:(NSString * _Nullable)createdAt lastScannedAt:(NSString * _Nullable)lastScannedAt scansInPeriod:(Mobile_multiplatformInt * _Nullable)scansInPeriod remoteID:(Mobile_multiplatformInt * _Nullable)remoteID status:(Mobile_multiplatformInt * _Nullable)status date:(NSString * _Nullable)date __attribute__((swift_name("init(id:uid:userID:orgID:orientation:licenseNumber:birthday:issuedAt:expiresAt:height:weight:eyeColor:hairColor:address:street:city:postalCode:fullName:firstName:middleName:lastName:gender:ban:bannedBy:banStartAt:banEndAt:vip:vipBy:vipStartAt:vipEndAt:visits:state:createdAt:lastScannedAt:scansInPeriod:remoteID:status:date:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiIdentitySyncEntryCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) NSString * _Nullable address __attribute__((swift_name("address")));
@property (readonly) Mobile_multiplatformInt * _Nullable ban __attribute__((swift_name("ban")));
@property (readonly) NSString * _Nullable banEndAt __attribute__((swift_name("banEndAt")));
@property (readonly) NSString * _Nullable banStartAt __attribute__((swift_name("banStartAt")));
@property (readonly) NSString * _Nullable bannedBy __attribute__((swift_name("bannedBy")));
@property (readonly) NSString * _Nullable birthday __attribute__((swift_name("birthday")));
@property (readonly) NSString * _Nullable city __attribute__((swift_name("city")));
@property (readonly) NSString * _Nullable createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString * _Nullable date __attribute__((swift_name("date")));
@property (readonly) NSString * _Nullable expiresAt __attribute__((swift_name("expiresAt")));
@property (readonly) NSString * _Nullable eyeColor __attribute__((swift_name("eyeColor")));
@property (readonly) NSString * _Nullable firstName __attribute__((swift_name("firstName")));
@property (readonly) NSString * _Nullable fullName __attribute__((swift_name("fullName")));
@property (readonly) NSString * _Nullable gender __attribute__((swift_name("gender")));
@property (readonly) NSString * _Nullable hairColor __attribute__((swift_name("hairColor")));
@property (readonly) NSString * _Nullable height __attribute__((swift_name("height")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable issuedAt __attribute__((swift_name("issuedAt")));
@property (readonly) NSString * _Nullable lastName __attribute__((swift_name("lastName")));
@property (readonly) NSString * _Nullable lastScannedAt __attribute__((swift_name("lastScannedAt")));
@property (readonly) NSString * _Nullable licenseNumber __attribute__((swift_name("licenseNumber")));
@property (readonly) NSString * _Nullable middleName __attribute__((swift_name("middleName")));
@property (readonly) NSString * _Nullable orgID __attribute__((swift_name("orgID")));
@property (readonly) Mobile_multiplatformInt * _Nullable orientation __attribute__((swift_name("orientation")));
@property (readonly) NSString * _Nullable postalCode __attribute__((swift_name("postalCode")));
@property (readonly) Mobile_multiplatformInt * _Nullable remoteID __attribute__((swift_name("remoteID")));
@property (readonly) Mobile_multiplatformInt * _Nullable scansInPeriod __attribute__((swift_name("scansInPeriod")));
@property (readonly) NSString * _Nullable state __attribute__((swift_name("state")));
@property Mobile_multiplatformInt * _Nullable status __attribute__((swift_name("status")));
@property (readonly) NSString * _Nullable street __attribute__((swift_name("street")));
@property (readonly) NSString * _Nullable uid __attribute__((swift_name("uid")));
@property (readonly) NSString *userID __attribute__((swift_name("userID")));
@property (readonly) Mobile_multiplatformInt * _Nullable vip __attribute__((swift_name("vip")));
@property (readonly) NSString * _Nullable vipBy __attribute__((swift_name("vipBy")));
@property (readonly) NSString * _Nullable vipEndAt __attribute__((swift_name("vipEndAt")));
@property (readonly) NSString * _Nullable vipStartAt __attribute__((swift_name("vipStartAt")));
@property (readonly) Mobile_multiplatformInt * _Nullable visits __attribute__((swift_name("visits")));
@property (readonly) NSString * _Nullable weight __attribute__((swift_name("weight")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiIdentitySyncEntry.Companion")))
@interface Mobile_multiplatformApiIdentitySyncEntryCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiIdentitySyncEntryCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPushIdentityRequest")))
@interface Mobile_multiplatformApiPushIdentityRequest : Mobile_multiplatformBase <Mobile_multiplatformApiPushRequest>
- (instancetype)initWithData:(NSArray<Mobile_multiplatformApiIdentitySyncEntry *> *)data __attribute__((swift_name("init(data:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiPushIdentityRequestCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) NSArray<Mobile_multiplatformApiIdentitySyncEntry *> *data __attribute__((swift_name("data")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiPushIdentityRequest.Companion")))
@interface Mobile_multiplatformApiPushIdentityRequestCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiPushIdentityRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("IdOwner")))
@protocol Mobile_multiplatformIdOwner
@required
@property (setter=setId:) int64_t id_ __attribute__((swift_name("id_")));
@end;

__attribute__((swift_name("RidOwner")))
@protocol Mobile_multiplatformRidOwner
@required
@property Mobile_multiplatformLong * _Nullable rid __attribute__((swift_name("rid")));
@end;

__attribute__((swift_name("CreateUpdateTimeOwner")))
@protocol Mobile_multiplatformCreateUpdateTimeOwner
@required
@property int64_t createdAtS __attribute__((swift_name("createdAtS")));
@property int64_t updatedAtS __attribute__((swift_name("updatedAtS")));
@end;

__attribute__((swift_name("BaseEntity")))
@protocol Mobile_multiplatformBaseEntity <Mobile_multiplatformIdOwner, Mobile_multiplatformRidOwner, Mobile_multiplatformCreateUpdateTimeOwner>
@required
@end;

__attribute__((swift_name("WithDeletionMarkMutable")))
@protocol Mobile_multiplatformWithDeletionMarkMutable
@required
@property BOOL deleted __attribute__((swift_name("deleted")));
@end;

__attribute__((swift_name("Changable")))
@protocol Mobile_multiplatformChangable
@required
@property BOOL hasChanges __attribute__((swift_name("hasChanges")));
@end;

__attribute__((swift_name("LocalIdentity")))
@protocol Mobile_multiplatformLocalIdentity <Mobile_multiplatformBaseEntity, Mobile_multiplatformWithDeletionMarkMutable, Mobile_multiplatformChangable>
@required
@property NSString * _Nullable address __attribute__((swift_name("address")));
@property (setter=setBan:) BOOL ban_ __attribute__((swift_name("ban_")));
@property Mobile_multiplatformLong * _Nullable banAt __attribute__((swift_name("banAt")));
@property NSString * _Nullable banBy __attribute__((swift_name("banBy")));
@property Mobile_multiplatformLong * _Nullable banEndAt __attribute__((swift_name("banEndAt")));
@property Mobile_multiplatformLong * _Nullable birthday __attribute__((swift_name("birthday")));
@property NSString * _Nullable city __attribute__((swift_name("city")));
@property Mobile_multiplatformLong * _Nullable expiresAt __attribute__((swift_name("expiresAt")));
@property NSString * _Nullable eyeColor __attribute__((swift_name("eyeColor")));
@property NSString * _Nullable firstName __attribute__((swift_name("firstName")));
@property NSString * _Nullable fullName __attribute__((swift_name("fullName")));
@property NSString * _Nullable gender __attribute__((swift_name("gender")));
@property NSString * _Nullable hairColor __attribute__((swift_name("hairColor")));
@property NSString * _Nullable height __attribute__((swift_name("height")));
@property Mobile_multiplatformLong * _Nullable issuedAt __attribute__((swift_name("issuedAt")));
@property NSString * _Nullable lastName __attribute__((swift_name("lastName")));
@property NSString * _Nullable licenseNumber __attribute__((swift_name("licenseNumber")));
@property NSString * _Nullable middleName __attribute__((swift_name("middleName")));
@property NSString * _Nullable state __attribute__((swift_name("state")));
@property Mobile_multiplatformLong * _Nullable updatedFaceAt __attribute__((swift_name("updatedFaceAt")));
@property NSString *userId __attribute__((swift_name("userId")));
@property (setter=setVip:) BOOL vip_ __attribute__((swift_name("vip_")));
@property Mobile_multiplatformLong * _Nullable vipAt __attribute__((swift_name("vipAt")));
@property NSString * _Nullable vipBy __attribute__((swift_name("vipBy")));
@property Mobile_multiplatformLong * _Nullable vipEndAt __attribute__((swift_name("vipEndAt")));
@property Mobile_multiplatformLong * _Nullable visits __attribute__((swift_name("visits")));
@property NSString * _Nullable weight __attribute__((swift_name("weight")));
@end;

__attribute__((swift_name("ApiScanBase")))
@protocol Mobile_multiplatformApiScanBase
@required
@property (readonly) Mobile_multiplatformInt * _Nullable ban __attribute__((swift_name("ban")));
@property (readonly) NSString *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) Mobile_multiplatformInt * _Nullable flags __attribute__((swift_name("flags")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) Mobile_multiplatformInt * _Nullable identityID __attribute__((swift_name("identityID")));
@property (readonly) NSString *userID __attribute__((swift_name("userID")));
@property (readonly) NSString * _Nullable verdictName __attribute__((swift_name("verdictName")));
@property (readonly) Mobile_multiplatformInt * _Nullable verdictResult __attribute__((swift_name("verdictResult")));
@property (readonly) Mobile_multiplatformInt * _Nullable verdictType __attribute__((swift_name("verdictType")));
@property (readonly) NSString * _Nullable verdictValue __attribute__((swift_name("verdictValue")));
@property (readonly) Mobile_multiplatformInt * _Nullable vip __attribute__((swift_name("vip")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiScan")))
@interface Mobile_multiplatformApiScan : Mobile_multiplatformBase <Mobile_multiplatformApiScanBase>
- (instancetype)initWithId:(int32_t)id userID:(NSString *)userID identityID:(Mobile_multiplatformInt * _Nullable)identityID createdAt:(NSString *)createdAt flags:(Mobile_multiplatformInt * _Nullable)flags verdictType:(Mobile_multiplatformInt * _Nullable)verdictType verdictResult:(Mobile_multiplatformInt * _Nullable)verdictResult verdictName:(NSString * _Nullable)verdictName verdictValue:(NSString * _Nullable)verdictValue vip:(Mobile_multiplatformInt * _Nullable)vip ban:(Mobile_multiplatformInt * _Nullable)ban __attribute__((swift_name("init(id:userID:identityID:createdAt:flags:verdictType:verdictResult:verdictName:verdictValue:vip:ban:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiScanCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformInt * _Nullable)component10 __attribute__((swift_name("component10()")));
- (Mobile_multiplatformInt * _Nullable)component11 __attribute__((swift_name("component11()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformInt * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (Mobile_multiplatformInt * _Nullable)component5 __attribute__((swift_name("component5()")));
- (Mobile_multiplatformInt * _Nullable)component6 __attribute__((swift_name("component6()")));
- (Mobile_multiplatformInt * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (Mobile_multiplatformApiScan *)doCopyId:(int32_t)id userID:(NSString *)userID identityID:(Mobile_multiplatformInt * _Nullable)identityID createdAt:(NSString *)createdAt flags:(Mobile_multiplatformInt * _Nullable)flags verdictType:(Mobile_multiplatformInt * _Nullable)verdictType verdictResult:(Mobile_multiplatformInt * _Nullable)verdictResult verdictName:(NSString * _Nullable)verdictName verdictValue:(NSString * _Nullable)verdictValue vip:(Mobile_multiplatformInt * _Nullable)vip ban:(Mobile_multiplatformInt * _Nullable)ban __attribute__((swift_name("doCopy(id:userID:identityID:createdAt:flags:verdictType:verdictResult:verdictName:verdictValue:vip:ban:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformInt * _Nullable ban __attribute__((swift_name("ban")));
@property (readonly) NSString *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) Mobile_multiplatformInt * _Nullable flags __attribute__((swift_name("flags")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) Mobile_multiplatformInt * _Nullable identityID __attribute__((swift_name("identityID")));
@property (readonly) NSString *userID __attribute__((swift_name("userID")));
@property (readonly) NSString * _Nullable verdictName __attribute__((swift_name("verdictName")));
@property (readonly) Mobile_multiplatformInt * _Nullable verdictResult __attribute__((swift_name("verdictResult")));
@property (readonly) Mobile_multiplatformInt * _Nullable verdictType __attribute__((swift_name("verdictType")));
@property (readonly) NSString * _Nullable verdictValue __attribute__((swift_name("verdictValue")));
@property (readonly) Mobile_multiplatformInt * _Nullable vip __attribute__((swift_name("vip")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiScan.Companion")))
@interface Mobile_multiplatformApiScanCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiScanCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiScanSyncEntry")))
@interface Mobile_multiplatformApiScanSyncEntry : Mobile_multiplatformBase <Mobile_multiplatformApiScanBase, Mobile_multiplatformApiSyncEntry>
- (instancetype)initWithId:(int32_t)id userID:(NSString *)userID identityID:(Mobile_multiplatformInt * _Nullable)identityID createdAt:(NSString *)createdAt flags:(Mobile_multiplatformInt * _Nullable)flags verdictType:(Mobile_multiplatformInt * _Nullable)verdictType verdictResult:(Mobile_multiplatformInt * _Nullable)verdictResult verdictName:(NSString * _Nullable)verdictName verdictValue:(NSString * _Nullable)verdictValue vip:(Mobile_multiplatformInt * _Nullable)vip ban:(Mobile_multiplatformInt * _Nullable)ban remoteID:(Mobile_multiplatformInt * _Nullable)remoteID status:(Mobile_multiplatformInt * _Nullable)status date:(NSString * _Nullable)date __attribute__((swift_name("init(id:userID:identityID:createdAt:flags:verdictType:verdictResult:verdictName:verdictValue:vip:ban:remoteID:status:date:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiScanSyncEntryCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) Mobile_multiplatformInt * _Nullable ban __attribute__((swift_name("ban")));
@property (readonly) NSString *createdAt __attribute__((swift_name("createdAt")));
@property (readonly) NSString * _Nullable date __attribute__((swift_name("date")));
@property (readonly) Mobile_multiplatformInt * _Nullable flags __attribute__((swift_name("flags")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@property (readonly) Mobile_multiplatformInt * _Nullable identityID __attribute__((swift_name("identityID")));
@property (readonly) Mobile_multiplatformInt * _Nullable remoteID __attribute__((swift_name("remoteID")));
@property Mobile_multiplatformInt * _Nullable status __attribute__((swift_name("status")));
@property (readonly) NSString *userID __attribute__((swift_name("userID")));
@property (readonly) NSString * _Nullable verdictName __attribute__((swift_name("verdictName")));
@property (readonly) Mobile_multiplatformInt * _Nullable verdictResult __attribute__((swift_name("verdictResult")));
@property (readonly) Mobile_multiplatformInt * _Nullable verdictType __attribute__((swift_name("verdictType")));
@property (readonly) NSString * _Nullable verdictValue __attribute__((swift_name("verdictValue")));
@property (readonly) Mobile_multiplatformInt * _Nullable vip __attribute__((swift_name("vip")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiScanSyncEntry.Companion")))
@interface Mobile_multiplatformApiScanSyncEntryCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiScanSyncEntryCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("LocalScan")))
@protocol Mobile_multiplatformLocalScan <Mobile_multiplatformBaseEntity, Mobile_multiplatformWithDeletionMarkMutable>
@required
@property (setter=setBan:) BOOL ban_ __attribute__((swift_name("ban_")));
@property int64_t createdAtHourS __attribute__((swift_name("createdAtHourS")));
@property Mobile_multiplatformLong * _Nullable identityId __attribute__((swift_name("identityId")));
@property NSString * _Nullable userId __attribute__((swift_name("userId")));
@property Mobile_multiplatformInt * _Nullable verdictResult __attribute__((swift_name("verdictResult")));
@property NSString * _Nullable verdictValue __attribute__((swift_name("verdictValue")));
@property (setter=setVip:) BOOL vip_ __attribute__((swift_name("vip_")));
@end;

__attribute__((swift_name("SyncStrategy")))
@protocol Mobile_multiplatformSyncStrategy
@required
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow> _Nullable)changesMonitor __attribute__((swift_name("changesMonitor()")));
@property (readonly) NSString *entitiesName __attribute__((swift_name("entitiesName")));
@end;

__attribute__((swift_name("UpdatingSyncStrategy")))
@interface Mobile_multiplatformUpdatingSyncStrategy : Mobile_multiplatformBase <Mobile_multiplatformSyncStrategy>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Mobile_multiplatformUpdatingSyncStrategyCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getPagePageSize:(int32_t)pageSize pageNumber:(int32_t)pageNumber completionHandler:(void (^)(Mobile_multiplatformGetPageResults * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getPage(pageSize:pageNumber:completionHandler:)")));
- (Mobile_multiplatformGetPageResults * _Nullable)processGetPageErrorsCodeAndMessagePairs:(NSArray<Mobile_multiplatformKotlinPair<Mobile_multiplatformInt *, NSString *> *> * _Nullable)codeAndMessagePairs __attribute__((swift_name("processGetPageErrors(codeAndMessagePairs:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)pullPeriodString:(NSString *)periodString completionHandler:(void (^)(Mobile_multiplatformPullResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("pull(periodString:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)pushWithCompletionHandler:(void (^)(Mobile_multiplatformPushResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("push(completionHandler:)")));
@end;

__attribute__((swift_name("DownloadSyncStrategy")))
@interface Mobile_multiplatformDownloadSyncStrategy<ApiEntity, SyncEntity, LocalEntity, Converter> : Mobile_multiplatformUpdatingSyncStrategy
- (instancetype)initWithRepository:(id<Mobile_multiplatformLocalRepository>)repository converter:(Converter)converter getFromApi:(Mobile_multiplatformGetFromApi<ApiEntity> *)getFromApi pullFromApi:(Mobile_multiplatformPullFromApi<SyncEntity> *)pullFromApi syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher actionOnSyncDelete:(Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *)actionOnSyncDelete __attribute__((swift_name("init(repository:converter:getFromApi:pullFromApi:syncDispatcher:actionOnSyncDelete:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getExistingEntriesForUpdateRids:(NSArray<Mobile_multiplatformLong *> *)rids completionHandler:(void (^)(NSArray<LocalEntity> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getExistingEntriesForUpdate(rids:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getPagePageSize:(int32_t)pageSize pageNumber:(int32_t)pageNumber completionHandler:(void (^)(Mobile_multiplatformGetPageResults * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getPage(pageSize:pageNumber:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)pullPeriodString:(NSString *)periodString completionHandler:(void (^)(Mobile_multiplatformPullResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("pull(periodString:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)pushWithCompletionHandler:(void (^)(Mobile_multiplatformPushResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("push(completionHandler:)")));
- (void)supplyApiEntitiesApiEntities:(NSArray<id> *)apiEntities markAsChanged:(BOOL)markAsChanged __attribute__((swift_name("supplyApiEntities(apiEntities:markAsChanged:)")));
@property (readonly) Converter converter __attribute__((swift_name("converter")));
@end;

__attribute__((swift_name("TwoWaySyncStrategy")))
@interface Mobile_multiplatformTwoWaySyncStrategy<ApiEntity, SyncEntity, LocalEntity, Converter, R> : Mobile_multiplatformDownloadSyncStrategy<ApiEntity, SyncEntity, LocalEntity, Converter>
- (instancetype)initWithRepository:(R)repository converter:(Converter)converter getFromApi:(Mobile_multiplatformGetFromApi<ApiEntity> *)getFromApi pullFromApi:(Mobile_multiplatformPullFromApi<SyncEntity> *)pullFromApi pushToApi:(Mobile_multiplatformPushToApi<SyncEntity> *)pushToApi syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher actionOnSyncDelete:(Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *)actionOnSyncDelete __attribute__((swift_name("init(repository:converter:getFromApi:pullFromApi:pushToApi:syncDispatcher:actionOnSyncDelete:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithRepository:(id<Mobile_multiplatformLocalRepository>)repository converter:(id<Mobile_multiplatformApiToLocalConverter>)converter getFromApi:(Mobile_multiplatformGetFromApi<id> *)getFromApi pullFromApi:(Mobile_multiplatformPullFromApi<id<Mobile_multiplatformApiSyncEntry>> *)pullFromApi syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher actionOnSyncDelete:(Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *)actionOnSyncDelete __attribute__((swift_name("init(repository:converter:getFromApi:pullFromApi:syncDispatcher:actionOnSyncDelete:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformTwoWaySyncStrategyCompanion *companion __attribute__((swift_name("companion")));
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow>)changesMonitor __attribute__((swift_name("changesMonitor()")));
- (id<Mobile_multiplatformApiPushRequest>)createPushRequestData:(NSArray<SyncEntity> *)data __attribute__((swift_name("createPushRequest(data:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)pushWithCompletionHandler:(void (^)(Mobile_multiplatformPushResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("push(completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IdentitiesSyncStrategy")))
@interface Mobile_multiplatformIdentitiesSyncStrategy : Mobile_multiplatformTwoWaySyncStrategy<Mobile_multiplatformApiIdentity *, Mobile_multiplatformApiIdentitySyncEntry *, id<Mobile_multiplatformLocalIdentity>, Mobile_multiplatformIdentityConverter *, id<Mobile_multiplatformLocalRepositoryWithChanges>>
- (instancetype)initWithRepository:(id<Mobile_multiplatformLocalRepositoryWithChanges>)repository getFromApi:(Mobile_multiplatformGetIdentitiesFromApi *)getFromApi pullFromApi:(Mobile_multiplatformPullIdentitiesFromApi *)pullFromApi pushToApi:(Mobile_multiplatformPushIdentitiesToApi *)pushToApi converter:(Mobile_multiplatformIdentityConverter *)converter syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher __attribute__((swift_name("init(repository:getFromApi:pullFromApi:pushToApi:converter:syncDispatcher:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithRepository:(id<Mobile_multiplatformLocalRepository>)repository converter:(id<Mobile_multiplatformApiAndLocalConverter>)converter getFromApi:(Mobile_multiplatformGetFromApi<id> *)getFromApi pullFromApi:(Mobile_multiplatformPullFromApi<id<Mobile_multiplatformApiSyncEntry>> *)pullFromApi pushToApi:(Mobile_multiplatformPushToApi<id<Mobile_multiplatformApiSyncEntry>> *)pushToApi syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher actionOnSyncDelete:(Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *)actionOnSyncDelete __attribute__((swift_name("init(repository:converter:getFromApi:pullFromApi:pushToApi:syncDispatcher:actionOnSyncDelete:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (Mobile_multiplatformApiPushIdentityRequest *)createPushRequestData:(NSArray<Mobile_multiplatformApiIdentitySyncEntry *> *)data __attribute__((swift_name("createPushRequest(data:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getExistingEntriesForUpdateRids:(NSArray<Mobile_multiplatformLong *> *)rids completionHandler:(void (^)(NSArray<id<Mobile_multiplatformLocalIdentity>> * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getExistingEntriesForUpdate(rids:completionHandler:)")));
@property (readonly) NSString *entitiesName __attribute__((swift_name("entitiesName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ScansSyncStrategy")))
@interface Mobile_multiplatformScansSyncStrategy : Mobile_multiplatformDownloadSyncStrategy<Mobile_multiplatformApiScan *, Mobile_multiplatformApiScanSyncEntry *, id<Mobile_multiplatformLocalScan>, Mobile_multiplatformScanConverter *>
- (instancetype)initWithRepository:(id<Mobile_multiplatformLocalRepository>)repository getFromApi:(Mobile_multiplatformGetScansFromApi *)getFromApi pullFromApi:(Mobile_multiplatformPullScansFromApi *)pullFromApi converter:(Mobile_multiplatformScanConverter *)converter syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher __attribute__((swift_name("init(repository:getFromApi:pullFromApi:converter:syncDispatcher:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithRepository:(id<Mobile_multiplatformLocalRepository>)repository converter:(id<Mobile_multiplatformApiToLocalConverter>)converter getFromApi:(Mobile_multiplatformGetFromApi<id> *)getFromApi pullFromApi:(Mobile_multiplatformPullFromApi<id<Mobile_multiplatformApiSyncEntry>> *)pullFromApi syncDispatcher:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)syncDispatcher actionOnSyncDelete:(Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *)actionOnSyncDelete __attribute__((swift_name("init(repository:converter:getFromApi:pullFromApi:syncDispatcher:actionOnSyncDelete:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<Mobile_multiplatformKotlinx_coroutines_coreFlow>)changesMonitor __attribute__((swift_name("changesMonitor()")));
@property (readonly) NSString *entitiesName __attribute__((swift_name("entitiesName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Constants")))
@interface Mobile_multiplatformConstants : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)constants __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformConstants *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKotlinArray<Mobile_multiplatformInt *> *CREATED_OR_UPDATED __attribute__((swift_name("CREATED_OR_UPDATED")));
@property (readonly) int32_t SYNC_STATUS_CREATED __attribute__((swift_name("SYNC_STATUS_CREATED")));
@property (readonly) int32_t SYNC_STATUS_DELETED __attribute__((swift_name("SYNC_STATUS_DELETED")));
@property (readonly) int32_t SYNC_STATUS_UPDATED __attribute__((swift_name("SYNC_STATUS_UPDATED")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DownloadSyncStrategyActionOnSyncDelete")))
@interface Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete : Mobile_multiplatformKotlinEnum<Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *markAsDeleted __attribute__((swift_name("markAsDeleted")));
@property (class, readonly) Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *actuallyDelete __attribute__((swift_name("actuallyDelete")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformDownloadSyncStrategyActionOnSyncDelete *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DownloadSyncStrategyDataAndErrors")))
@interface Mobile_multiplatformDownloadSyncStrategyDataAndErrors<T> : Mobile_multiplatformBase
- (instancetype)initWithData:(T _Nullable)data errors:(NSArray<Mobile_multiplatformKotlinPair<Mobile_multiplatformInt *, NSString *> *> * _Nullable)errors __attribute__((swift_name("init(data:errors:)"))) __attribute__((objc_designated_initializer));
@property (readonly) T _Nullable data __attribute__((swift_name("data")));
@property (readonly) NSArray<Mobile_multiplatformKotlinPair<Mobile_multiplatformInt *, NSString *> *> * _Nullable errors __attribute__((swift_name("errors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncStrategyCompanion")))
@interface Mobile_multiplatformSyncStrategyCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformSyncStrategyCompanion *shared __attribute__((swift_name("shared")));
- (int32_t)getStatusRid:(Mobile_multiplatformLong * _Nullable)rid deleted:(BOOL)deleted __attribute__((swift_name("getStatus(rid:deleted:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TwoWaySyncStrategyCompanion")))
@interface Mobile_multiplatformTwoWaySyncStrategyCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformTwoWaySyncStrategyCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UpdatingSyncStrategy.Companion")))
@interface Mobile_multiplatformUpdatingSyncStrategyCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformUpdatingSyncStrategyCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) int32_t PULL_CHUNKS_SIZE __attribute__((swift_name("PULL_CHUNKS_SIZE")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DeobfuscateIdentityForHistory")))
@interface Mobile_multiplatformDeobfuscateIdentityForHistory : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (Mobile_multiplatformApiIdentity *)invokeSourceIdentity:(id<Mobile_multiplatformApiIdentityBase>)sourceIdentity extractedInfo:(id<Mobile_multiplatformCommonUsdlExtractedInfo>)extractedInfo __attribute__((swift_name("invoke(sourceIdentity:extractedInfo:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncDispatcherProvider")))
@interface Mobile_multiplatformSyncDispatcherProvider : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)syncDispatcherProvider __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformSyncDispatcherProvider *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)provide __attribute__((swift_name("provide()")));
@end;

__attribute__((swift_name("DateTimeConverter")))
@interface Mobile_multiplatformDateTimeConverter : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)formatTimestampS:(int64_t)timestampS __attribute__((swift_name("format(timestampS:)")));
- (int64_t)fromCheckedLengthStringToSecondsString:(NSString *)string __attribute__((swift_name("fromCheckedLengthStringToSeconds(string:)")));
- (int64_t)fromStringToSecondsString:(NSString *)string __attribute__((swift_name("fromStringToSeconds(string:)")));
- (Mobile_multiplatformLong * _Nullable)fromStringToSecondsNullableString:(NSString * _Nullable)string __attribute__((swift_name("fromStringToSecondsNullable(string:)")));
@property (readonly) int32_t patternLength __attribute__((swift_name("patternLength")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiDateOnlyConverter")))
@interface Mobile_multiplatformApiDateOnlyConverter : Mobile_multiplatformDateTimeConverter
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)formatTimestampS:(int64_t)timestampS __attribute__((swift_name("format(timestampS:)")));
- (int64_t)fromCheckedLengthStringToSecondsString:(NSString *)string __attribute__((swift_name("fromCheckedLengthStringToSeconds(string:)")));
@property (readonly) int32_t patternLength __attribute__((swift_name("patternLength")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiDateTimeConverter")))
@interface Mobile_multiplatformApiDateTimeConverter : Mobile_multiplatformDateTimeConverter
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)formatTimestampS:(int64_t)timestampS __attribute__((swift_name("format(timestampS:)")));
- (int64_t)fromCheckedLengthStringToSecondsString:(NSString *)string __attribute__((swift_name("fromCheckedLengthStringToSeconds(string:)")));
@property (readonly) int32_t patternLength __attribute__((swift_name("patternLength")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiTimeOnlyConverter")))
@interface Mobile_multiplatformApiTimeOnlyConverter : Mobile_multiplatformDateTimeConverter
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)formatTimestampS:(int64_t)timestampS __attribute__((swift_name("format(timestampS:)")));
- (int64_t)fromCheckedLengthStringToSecondsString:(NSString *)string __attribute__((swift_name("fromCheckedLengthStringToSeconds(string:)")));
@property (readonly) int32_t patternLength __attribute__((swift_name("patternLength")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SimpleTimeWithUnit")))
@interface Mobile_multiplatformSimpleTimeWithUnit : Mobile_multiplatformBase
- (instancetype)initWithMillis:(int64_t)millis __attribute__((swift_name("init(millis:)"))) __attribute__((objc_designated_initializer));
- (int64_t)days __attribute__((swift_name("days()")));
- (int64_t)hours __attribute__((swift_name("hours()")));
- (int64_t)millis __attribute__((swift_name("millis()")));
- (Mobile_multiplatformSimpleTimeWithUnit *)minusAnotherTime:(Mobile_multiplatformSimpleTimeWithUnit *)anotherTime __attribute__((swift_name("minus(anotherTime:)")));
- (int64_t)minutes __attribute__((swift_name("minutes()")));
- (Mobile_multiplatformSimpleTimeWithUnit *)plusAnotherTime:(Mobile_multiplatformSimpleTimeWithUnit *)anotherTime __attribute__((swift_name("plus(anotherTime:)")));
- (int64_t)seconds __attribute__((swift_name("seconds()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Auth0Tools")))
@interface Mobile_multiplatformAuth0Tools : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)auth0Tools __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformAuth0Tools *shared __attribute__((swift_name("shared")));
- (NSString *)getFullUrlAuth0Url:(NSString *)auth0Url __attribute__((swift_name("getFullUrl(auth0Url:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Auth0LoginData")))
@interface Mobile_multiplatformAuth0LoginData : Mobile_multiplatformBase
- (instancetype)initWithUsername:(NSString *)username password:(NSString *)password clientId:(NSString *)clientId audience:(NSString * _Nullable)audience grantType:(NSString *)grantType scope:(NSString *)scope __attribute__((swift_name("init(username:password:clientId:audience:grantType:scope:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformAuth0LoginDataCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (Mobile_multiplatformAuth0LoginData *)doCopyUsername:(NSString *)username password:(NSString *)password clientId:(NSString *)clientId audience:(NSString * _Nullable)audience grantType:(NSString *)grantType scope:(NSString *)scope __attribute__((swift_name("doCopy(username:password:clientId:audience:grantType:scope:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable audience __attribute__((swift_name("audience")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *grantType __attribute__((swift_name("grantType")));
@property (readonly) NSString *password __attribute__((swift_name("password")));
@property (readonly) NSString *scope __attribute__((swift_name("scope")));
@property (readonly) NSString *username __attribute__((swift_name("username")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Auth0LoginData.Companion")))
@interface Mobile_multiplatformAuth0LoginDataCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformAuth0LoginDataCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Auth0TokenRefreshRequest")))
@interface Mobile_multiplatformAuth0TokenRefreshRequest : Mobile_multiplatformBase
- (instancetype)initWithClientId:(NSString *)clientId audience:(NSString * _Nullable)audience grantType:(NSString *)grantType refreshToken:(NSString *)refreshToken __attribute__((swift_name("init(clientId:audience:grantType:refreshToken:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformAuth0TokenRefreshRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (Mobile_multiplatformAuth0TokenRefreshRequest *)doCopyClientId:(NSString *)clientId audience:(NSString * _Nullable)audience grantType:(NSString *)grantType refreshToken:(NSString *)refreshToken __attribute__((swift_name("doCopy(clientId:audience:grantType:refreshToken:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable audience __attribute__((swift_name("audience")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *grantType __attribute__((swift_name("grantType")));
@property (readonly) NSString *refreshToken __attribute__((swift_name("refreshToken")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Auth0TokenRefreshRequest.Companion")))
@interface Mobile_multiplatformAuth0TokenRefreshRequestCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformAuth0TokenRefreshRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Auth0TokenResponse")))
@interface Mobile_multiplatformAuth0TokenResponse : Mobile_multiplatformBase
- (instancetype)initWithAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken idToken:(NSString * _Nullable)idToken __attribute__((swift_name("init(accessToken:refreshToken:idToken:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformAuth0TokenResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformAuth0TokenResponse *)doCopyAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken idToken:(NSString * _Nullable)idToken __attribute__((swift_name("doCopy(accessToken:refreshToken:idToken:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *accessToken __attribute__((swift_name("accessToken")));
@property (readonly) NSString * _Nullable idToken __attribute__((swift_name("idToken")));
@property (readonly) NSString *refreshToken __attribute__((swift_name("refreshToken")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Auth0TokenResponse.Companion")))
@interface Mobile_multiplatformAuth0TokenResponseCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformAuth0TokenResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoginData")))
@interface Mobile_multiplatformLoginData : Mobile_multiplatformBase
- (instancetype)initWithUsername:(NSString *)username password:(NSString *)password clientId:(NSString *)clientId scope:(NSString *)scope __attribute__((swift_name("init(username:password:clientId:scope:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (Mobile_multiplatformLoginData *)doCopyUsername:(NSString *)username password:(NSString *)password clientId:(NSString *)clientId scope:(NSString *)scope __attribute__((swift_name("doCopy(username:password:clientId:scope:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *password __attribute__((swift_name("password")));
@property (readonly) NSString *scope __attribute__((swift_name("scope")));
@property (readonly) NSString *username __attribute__((swift_name("username")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LoginResult")))
@interface Mobile_multiplatformLoginResult : Mobile_multiplatformBase
- (instancetype)initWithAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken audience:(NSString *)audience userId:(NSString *)userId orgId:(NSString *)orgId __attribute__((swift_name("init(accessToken:refreshToken:audience:userId:orgId:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSString *)component5 __attribute__((swift_name("component5()")));
- (Mobile_multiplatformLoginResult *)doCopyAccessToken:(NSString *)accessToken refreshToken:(NSString *)refreshToken audience:(NSString *)audience userId:(NSString *)userId orgId:(NSString *)orgId __attribute__((swift_name("doCopy(accessToken:refreshToken:audience:userId:orgId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *accessToken __attribute__((swift_name("accessToken")));
@property (readonly) NSString *audience __attribute__((swift_name("audience")));
@property (readonly) NSString *orgId __attribute__((swift_name("orgId")));
@property (readonly) NSString *refreshToken __attribute__((swift_name("refreshToken")));
@property (readonly) NSString *userId __attribute__((swift_name("userId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TokenRefreshData")))
@interface Mobile_multiplatformTokenRefreshData : Mobile_multiplatformBase
- (instancetype)initWithClientId:(NSString *)clientId audience:(NSString *)audience refreshToken:(NSString *)refreshToken __attribute__((swift_name("init(clientId:audience:refreshToken:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformTokenRefreshData *)doCopyClientId:(NSString *)clientId audience:(NSString *)audience refreshToken:(NSString *)refreshToken __attribute__((swift_name("doCopy(clientId:audience:refreshToken:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *audience __attribute__((swift_name("audience")));
@property (readonly) NSString *clientId __attribute__((swift_name("clientId")));
@property (readonly) NSString *refreshToken __attribute__((swift_name("refreshToken")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TokenRefreshResult")))
@interface Mobile_multiplatformTokenRefreshResult : Mobile_multiplatformBase
- (instancetype)initWithAccessToken:(NSString *)accessToken __attribute__((swift_name("init(accessToken:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformTokenRefreshResult *)doCopyAccessToken:(NSString *)accessToken __attribute__((swift_name("doCopy(accessToken:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *accessToken __attribute__((swift_name("accessToken")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Login")))
@interface Mobile_multiplatformLogin : Mobile_multiplatformBase
- (instancetype)initWithAuth0Url:(NSString *)auth0Url __attribute__((swift_name("init(auth0Url:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeLoginData:(Mobile_multiplatformLoginData *)loginData completionHandler:(void (^)(Mobile_multiplatformLoginResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(loginData:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Login.FirstAuthStageData")))
@interface Mobile_multiplatformLoginFirstAuthStageData : Mobile_multiplatformBase
- (instancetype)initWithAudience:(NSString *)audience userId:(NSString *)userId orgId:(NSString *)orgId __attribute__((swift_name("init(audience:userId:orgId:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString *)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformLoginFirstAuthStageData *)doCopyAudience:(NSString *)audience userId:(NSString *)userId orgId:(NSString *)orgId __attribute__((swift_name("doCopy(audience:userId:orgId:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *audience __attribute__((swift_name("audience")));
@property (readonly) NSString *orgId __attribute__((swift_name("orgId")));
@property (readonly) NSString *userId __attribute__((swift_name("userId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TokenRefresh")))
@interface Mobile_multiplatformTokenRefresh : Mobile_multiplatformBase
- (instancetype)initWithAuth0Url:(NSString *)auth0Url __attribute__((swift_name("init(auth0Url:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeRefreshData:(Mobile_multiplatformTokenRefreshData *)refreshData completionHandler:(void (^)(Mobile_multiplatformTokenRefreshResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(refreshData:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiGetUsersResponse")))
@interface Mobile_multiplatformApiGetUsersResponse : Mobile_multiplatformBase
- (instancetype)initWithTopOrgId:(NSString *)topOrgId data:(NSArray<Mobile_multiplatformApiOrganizationUsersData *> *)data errors:(NSArray<Mobile_multiplatformApiError *> *)errors __attribute__((swift_name("init(topOrgId:data:errors:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiGetUsersResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSArray<Mobile_multiplatformApiOrganizationUsersData *> *)component2 __attribute__((swift_name("component2()")));
- (NSArray<Mobile_multiplatformApiError *> *)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformApiGetUsersResponse *)doCopyTopOrgId:(NSString *)topOrgId data:(NSArray<Mobile_multiplatformApiOrganizationUsersData *> *)data errors:(NSArray<Mobile_multiplatformApiError *> *)errors __attribute__((swift_name("doCopy(topOrgId:data:errors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<Mobile_multiplatformApiOrganizationUsersData *> *data __attribute__((swift_name("data")));
@property (readonly) NSArray<Mobile_multiplatformApiError *> *errors __attribute__((swift_name("errors")));
@property (readonly) NSString *topOrgId __attribute__((swift_name("topOrgId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiGetUsersResponse.Companion")))
@interface Mobile_multiplatformApiGetUsersResponseCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiGetUsersResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiOrganizationUsersData")))
@interface Mobile_multiplatformApiOrganizationUsersData : Mobile_multiplatformBase
- (instancetype)initWithOrganizationId:(NSString *)organizationId organizationName:(NSString *)organizationName usersCount:(int32_t)usersCount __attribute__((swift_name("init(organizationId:organizationName:usersCount:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiOrganizationUsersDataCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformApiOrganizationUsersData *)doCopyOrganizationId:(NSString *)organizationId organizationName:(NSString *)organizationName usersCount:(int32_t)usersCount __attribute__((swift_name("doCopy(organizationId:organizationName:usersCount:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *organizationId __attribute__((swift_name("organizationId")));
@property (readonly) NSString *organizationName __attribute__((swift_name("organizationName")));
@property (readonly) int32_t usersCount __attribute__((swift_name("usersCount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiOrganizationUsersData.Companion")))
@interface Mobile_multiplatformApiOrganizationUsersDataCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiOrganizationUsersDataCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiReassignmentApiRequest")))
@interface Mobile_multiplatformApiReassignmentApiRequest : Mobile_multiplatformBase
- (instancetype)initWithFromOrgId:(NSString *)fromOrgId toOrgId:(NSString *)toOrgId count:(int32_t)count __attribute__((swift_name("init(fromOrgId:toOrgId:count:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiReassignmentApiRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformApiReassignmentApiRequest *)doCopyFromOrgId:(NSString *)fromOrgId toOrgId:(NSString *)toOrgId count:(int32_t)count __attribute__((swift_name("doCopy(fromOrgId:toOrgId:count:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t count __attribute__((swift_name("count")));
@property (readonly) NSString *fromOrgId __attribute__((swift_name("fromOrgId")));
@property (readonly) NSString *toOrgId __attribute__((swift_name("toOrgId")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiReassignmentApiRequest.Companion")))
@interface Mobile_multiplatformApiReassignmentApiRequestCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiReassignmentApiRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiReassignmentResponse")))
@interface Mobile_multiplatformApiReassignmentResponse : Mobile_multiplatformBase
- (instancetype)initWithErrors:(NSArray<Mobile_multiplatformApiError *> *)errors __attribute__((swift_name("init(errors:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformApiReassignmentResponseCompanion *companion __attribute__((swift_name("companion")));
- (NSArray<Mobile_multiplatformApiError *> *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformApiReassignmentResponse *)doCopyErrors:(NSArray<Mobile_multiplatformApiError *> *)errors __attribute__((swift_name("doCopy(errors:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<Mobile_multiplatformApiError *> *errors __attribute__((swift_name("errors")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiReassignmentResponse.Companion")))
@interface Mobile_multiplatformApiReassignmentResponseCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformApiReassignmentResponseCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("OrgData")))
@interface Mobile_multiplatformOrgData : Mobile_multiplatformBase
- (instancetype)initWithOrgId:(NSString *)orgId name:(NSString *)name users:(int32_t)users __attribute__((swift_name("init(orgId:name:users:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformOrgData *)doCopyOrgId:(NSString *)orgId name:(NSString *)name users:(int32_t)users __attribute__((swift_name("doCopy(orgId:name:users:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *orgId __attribute__((swift_name("orgId")));
@property (readonly) int32_t users __attribute__((swift_name("users")));
@end;

__attribute__((swift_name("ReassignmentResult")))
@interface Mobile_multiplatformReassignmentResult : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("ReassignmentResult.Error")))
@interface Mobile_multiplatformReassignmentResultError : Mobile_multiplatformReassignmentResult
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReassignmentResult.ErrorNetworkError")))
@interface Mobile_multiplatformReassignmentResultErrorNetworkError : Mobile_multiplatformReassignmentResultError
- (instancetype)initWithError:(Mobile_multiplatformNetworkProblem *)error __attribute__((swift_name("init(error:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (Mobile_multiplatformNetworkProblem *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformReassignmentResultErrorNetworkError *)doCopyError:(Mobile_multiplatformNetworkProblem *)error __attribute__((swift_name("doCopy(error:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformNetworkProblem *error __attribute__((swift_name("error")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReassignmentResult.Success")))
@interface Mobile_multiplatformReassignmentResultSuccess : Mobile_multiplatformReassignmentResult
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)success __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformReassignmentResultSuccess *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("UserManagementData")))
@interface Mobile_multiplatformUserManagementData : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((swift_name("UserManagementData.Error")))
@interface Mobile_multiplatformUserManagementDataError : Mobile_multiplatformUserManagementData
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserManagementData.ErrorCurrentOrganizationDataNotFound")))
@interface Mobile_multiplatformUserManagementDataErrorCurrentOrganizationDataNotFound : Mobile_multiplatformUserManagementDataError
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)currentOrganizationDataNotFound __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformUserManagementDataErrorCurrentOrganizationDataNotFound *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserManagementData.ErrorNetworkError")))
@interface Mobile_multiplatformUserManagementDataErrorNetworkError : Mobile_multiplatformUserManagementDataError
- (instancetype)initWithError:(Mobile_multiplatformNetworkProblem *)error __attribute__((swift_name("init(error:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (Mobile_multiplatformNetworkProblem *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformUserManagementDataErrorNetworkError *)doCopyError:(Mobile_multiplatformNetworkProblem *)error __attribute__((swift_name("doCopy(error:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformNetworkProblem *error __attribute__((swift_name("error")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserManagementData.ErrorTopOrganizationDataNotFound")))
@interface Mobile_multiplatformUserManagementDataErrorTopOrganizationDataNotFound : Mobile_multiplatformUserManagementDataError
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (instancetype)topOrganizationDataNotFound __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformUserManagementDataErrorTopOrganizationDataNotFound *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UserManagementData.Success")))
@interface Mobile_multiplatformUserManagementDataSuccess : Mobile_multiplatformUserManagementData
- (instancetype)initWithTopOrg:(Mobile_multiplatformOrgData *)topOrg currentOrg:(Mobile_multiplatformOrgData *)currentOrg allOrgs:(NSArray<Mobile_multiplatformOrgData *> *)allOrgs __attribute__((swift_name("init(topOrg:currentOrg:allOrgs:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (Mobile_multiplatformOrgData *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformOrgData *)component2 __attribute__((swift_name("component2()")));
- (NSArray<Mobile_multiplatformOrgData *> *)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformUserManagementDataSuccess *)doCopyTopOrg:(Mobile_multiplatformOrgData *)topOrg currentOrg:(Mobile_multiplatformOrgData *)currentOrg allOrgs:(NSArray<Mobile_multiplatformOrgData *> *)allOrgs __attribute__((swift_name("doCopy(topOrg:currentOrg:allOrgs:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<Mobile_multiplatformOrgData *> *allOrgs __attribute__((swift_name("allOrgs")));
@property (readonly) Mobile_multiplatformOrgData *currentOrg __attribute__((swift_name("currentOrg")));
@property (readonly) Mobile_multiplatformOrgData *topOrg __attribute__((swift_name("topOrg")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetSubscriptionPrice")))
@interface Mobile_multiplatformGetSubscriptionPrice : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeUsersQuantity:(int32_t)usersQuantity completionHandler:(void (^)(Mobile_multiplatformFloat * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(usersQuantity:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GetUserManagementData")))
@interface Mobile_multiplatformGetUserManagementData : Mobile_multiplatformBase
- (instancetype)initWithAuthDataRepository:(id<Mobile_multiplatformAuthDataRepository>)authDataRepository ktorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider __attribute__((swift_name("init(authDataRepository:ktorProvider:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeWithCompletionHandler:(void (^)(Mobile_multiplatformUserManagementData * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ReassignUsers")))
@interface Mobile_multiplatformReassignUsers : Mobile_multiplatformBase
- (instancetype)initWithKtorProvider:(Mobile_multiplatformKtorProvider *)ktorProvider __attribute__((swift_name("init(ktorProvider:)"))) __attribute__((objc_designated_initializer));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeSourceOrgId:(NSString *)sourceOrgId targetOrgId:(NSString *)targetOrgId usersToReassign:(int32_t)usersToReassign completionHandler:(void (^)(Mobile_multiplatformReassignmentResult * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(sourceOrgId:targetOrgId:usersToReassign:completionHandler:)")));
@end;

__attribute__((swift_name("KotlinNumber")))
@interface Mobile_multiplatformKotlinNumber : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (int8_t)toByte __attribute__((swift_name("toByte()")));
- (unichar)toChar __attribute__((swift_name("toChar()")));
- (double)toDouble __attribute__((swift_name("toDouble()")));
- (float)toFloat __attribute__((swift_name("toFloat()")));
- (int32_t)toInt __attribute__((swift_name("toInt()")));
- (int64_t)toLong __attribute__((swift_name("toLong()")));
- (int16_t)toShort __attribute__((swift_name("toShort()")));
@end;

@interface Mobile_multiplatformKotlinNumber (Extensions)
- (Mobile_multiplatformSimpleTimeWithUnit *)days __attribute__((swift_name("days()")));
- (Mobile_multiplatformSimpleTimeWithUnit *)hours __attribute__((swift_name("hours()")));
- (Mobile_multiplatformSimpleTimeWithUnit *)millis __attribute__((swift_name("millis()")));
- (Mobile_multiplatformSimpleTimeWithUnit *)minutes __attribute__((swift_name("minutes()")));
- (Mobile_multiplatformSimpleTimeWithUnit *)seconds __attribute__((swift_name("seconds()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NetworkUtilsKt")))
@interface Mobile_multiplatformNetworkUtilsKt : Mobile_multiplatformBase
+ (NSString *)apiErrorsToMessageErrors:(NSArray<Mobile_multiplatformApiError *> *)errors __attribute__((swift_name("apiErrorsToMessage(errors:)")));
+ (void)logHttpOkErrorApiErrors:(NSArray<Mobile_multiplatformApiError *> *)apiErrors __attribute__((swift_name("logHttpOkError(apiErrors:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
+ (void)processNetworkErrorOrThrowThrowable:(Mobile_multiplatformKotlinThrowable *)throwable completionHandler:(void (^)(Mobile_multiplatformNetworkProblem * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("processNetworkErrorOrThrow(throwable:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
+ (void)tryToParseErrorBodyContent:(id<Mobile_multiplatformKtor_ioByteReadChannel>)content completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("tryToParseErrorBody(content:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiIdentityEntityKt")))
@interface Mobile_multiplatformApiIdentityEntityKt : Mobile_multiplatformBase
@property (class, readonly) Mobile_multiplatformApiEntity *ApiIdentityEntity __attribute__((swift_name("ApiIdentityEntity")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ApiScanEntityKt")))
@interface Mobile_multiplatformApiScanEntityKt : Mobile_multiplatformBase
@property (class, readonly) Mobile_multiplatformApiEntity *ApiScanEntity __attribute__((swift_name("ApiScanEntity")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TimeUtilsKt")))
@interface Mobile_multiplatformTimeUtilsKt : Mobile_multiplatformBase
+ (int64_t)toHourStartSTimestampS:(int64_t)timestampS __attribute__((swift_name("toHourStartS(timestampS:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreFlow")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreFlow
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)collectCollector:(id<Mobile_multiplatformKotlinx_coroutines_coreFlowCollector>)collector completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("collect(collector:completionHandler:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSharedFlow")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreSharedFlow <Mobile_multiplatformKotlinx_coroutines_coreFlow>
@required
@property (readonly) NSArray<id> *replayCache __attribute__((swift_name("replayCache")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreFlowCollector")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreFlowCollector
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)emitValue:(id _Nullable)value completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("emit(value:completionHandler:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreMutableSharedFlow")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreMutableSharedFlow <Mobile_multiplatformKotlinx_coroutines_coreSharedFlow, Mobile_multiplatformKotlinx_coroutines_coreFlowCollector>
@required
- (void)resetReplayCache __attribute__((swift_name("resetReplayCache()")));
- (BOOL)tryEmitValue:(id _Nullable)value __attribute__((swift_name("tryEmit(value:)")));
@property (readonly) id<Mobile_multiplatformKotlinx_coroutines_coreStateFlow> subscriptionCount __attribute__((swift_name("subscriptionCount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NapierLogLevel")))
@interface Mobile_multiplatformNapierLogLevel : Mobile_multiplatformKotlinEnum<Mobile_multiplatformNapierLogLevel *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Mobile_multiplatformNapierLogLevel *verbose __attribute__((swift_name("verbose")));
@property (class, readonly) Mobile_multiplatformNapierLogLevel *debug __attribute__((swift_name("debug")));
@property (class, readonly) Mobile_multiplatformNapierLogLevel *info __attribute__((swift_name("info")));
@property (class, readonly) Mobile_multiplatformNapierLogLevel *warning __attribute__((swift_name("warning")));
@property (class, readonly) Mobile_multiplatformNapierLogLevel *error __attribute__((swift_name("error")));
@property (class, readonly) Mobile_multiplatformNapierLogLevel *assert __attribute__((swift_name("assert")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformNapierLogLevel *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface Mobile_multiplatformKotlinThrowable : Mobile_multiplatformBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (Mobile_multiplatformKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialFormat")))
@protocol Mobile_multiplatformKotlinx_serialization_coreSerialFormat
@required
@property (readonly) Mobile_multiplatformKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreStringFormat")))
@protocol Mobile_multiplatformKotlinx_serialization_coreStringFormat <Mobile_multiplatformKotlinx_serialization_coreSerialFormat>
@required
- (id _Nullable)decodeFromStringDeserializer:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (NSString *)encodeToStringSerializer:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
@end;

__attribute__((swift_name("Kotlinx_serialization_jsonJson")))
@interface Mobile_multiplatformKotlinx_serialization_jsonJson : Mobile_multiplatformBase <Mobile_multiplatformKotlinx_serialization_coreStringFormat>
- (instancetype)initWithConfiguration:(Mobile_multiplatformKotlinx_serialization_jsonJsonConfiguration *)configuration serializersModule:(Mobile_multiplatformKotlinx_serialization_coreSerializersModule *)serializersModule __attribute__((swift_name("init(configuration:serializersModule:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKotlinx_serialization_jsonJsonDefault *companion __attribute__((swift_name("companion")));
- (id _Nullable)decodeFromJsonElementDeserializer:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>)deserializer element:(Mobile_multiplatformKotlinx_serialization_jsonJsonElement *)element __attribute__((swift_name("decodeFromJsonElement(deserializer:element:)")));
- (id _Nullable)decodeFromStringDeserializer:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>)deserializer string:(NSString *)string __attribute__((swift_name("decodeFromString(deserializer:string:)")));
- (Mobile_multiplatformKotlinx_serialization_jsonJsonElement *)encodeToJsonElementSerializer:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToJsonElement(serializer:value:)")));
- (NSString *)encodeToStringSerializer:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeToString(serializer:value:)")));
- (Mobile_multiplatformKotlinx_serialization_jsonJsonElement *)parseToJsonElementString:(NSString *)string __attribute__((swift_name("parseToJsonElement(string:)")));
@property (readonly) Mobile_multiplatformKotlinx_serialization_jsonJsonConfiguration *configuration __attribute__((swift_name("configuration")));
@property (readonly) Mobile_multiplatformKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<Mobile_multiplatformKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end;

__attribute__((swift_name("Ktor_ioCloseable")))
@protocol Mobile_multiplatformKtor_ioCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClient")))
@interface Mobile_multiplatformKtor_client_coreHttpClient : Mobile_multiplatformBase <Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope, Mobile_multiplatformKtor_ioCloseable>
- (instancetype)initWithEngine:(id<Mobile_multiplatformKtor_client_coreHttpClientEngine>)engine userConfig:(Mobile_multiplatformKtor_client_coreHttpClientConfig<Mobile_multiplatformKtor_client_coreHttpClientEngineConfig *> *)userConfig __attribute__((swift_name("init(engine:userConfig:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (Mobile_multiplatformKtor_client_coreHttpClient *)configBlock:(void (^)(Mobile_multiplatformKtor_client_coreHttpClientConfig<id> *))block __attribute__((swift_name("config(block:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeBuilder:(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *)builder completionHandler:(void (^)(Mobile_multiplatformKtor_client_coreHttpClientCall * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(builder:completionHandler:)"))) __attribute__((unavailable("Unbound [HttpClientCall] is deprecated. Consider using [request<HttpResponse>(builder)] instead.")));
- (BOOL)isSupportedCapability:(id<Mobile_multiplatformKtor_client_coreHttpClientEngineCapability>)capability __attribute__((swift_name("isSupported(capability:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<Mobile_multiplatformKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) id<Mobile_multiplatformKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher"))) __attribute__((unavailable("[dispatcher] is deprecated. Use coroutineContext instead.")));
@property (readonly) id<Mobile_multiplatformKtor_client_coreHttpClientEngine> engine __attribute__((swift_name("engine")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpClientEngineConfig *engineConfig __attribute__((swift_name("engineConfig")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpReceivePipeline *receivePipeline __attribute__((swift_name("receivePipeline")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpRequestPipeline *requestPipeline __attribute__((swift_name("requestPipeline")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpResponsePipeline *responsePipeline __attribute__((swift_name("responsePipeline")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpSendPipeline *sendPipeline __attribute__((swift_name("sendPipeline")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol Mobile_multiplatformKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol Mobile_multiplatformKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol Mobile_multiplatformKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol Mobile_multiplatformKotlinKClass <Mobile_multiplatformKotlinKDeclarationContainer, Mobile_multiplatformKotlinKAnnotatedElement, Mobile_multiplatformKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((swift_name("KotlinException")))
@interface Mobile_multiplatformKotlinException : Mobile_multiplatformKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinRuntimeException")))
@interface Mobile_multiplatformKotlinRuntimeException : Mobile_multiplatformKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinIllegalStateException")))
@interface Mobile_multiplatformKotlinIllegalStateException : Mobile_multiplatformKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinCancellationException")))
@interface Mobile_multiplatformKotlinCancellationException : Mobile_multiplatformKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Ktor_client_authAuthProvider")))
@protocol Mobile_multiplatformKtor_client_authAuthProvider
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)addRequestHeadersRequest:(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *)request completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("addRequestHeaders(request:completionHandler:)")));
- (BOOL)isApplicableAuth:(Mobile_multiplatformKtor_httpHttpAuthHeader *)auth __attribute__((swift_name("isApplicable(auth:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)refreshTokenResponse:(Mobile_multiplatformKtor_client_coreHttpResponse *)response completionHandler:(void (^)(Mobile_multiplatformBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("refreshToken(response:completionHandler:)")));
- (BOOL)sendWithoutRequestRequest:(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *)request __attribute__((swift_name("sendWithoutRequest(request:)")));
@property (readonly) BOOL sendWithoutRequest __attribute__((swift_name("sendWithoutRequest"))) __attribute__((deprecated("Please use sendWithoutRequest function instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsAttributeKey")))
@interface Mobile_multiplatformKtor_utilsAttributeKey<T> : Mobile_multiplatformBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_authAuth")))
@interface Mobile_multiplatformKtor_client_authAuth : Mobile_multiplatformBase
- (instancetype)initWithProviders:(NSMutableArray<id<Mobile_multiplatformKtor_client_authAuthProvider>> *)providers __attribute__((swift_name("init(providers:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_client_authAuthFeature *companion __attribute__((swift_name("companion")));
@property (readonly) NSMutableArray<id<Mobile_multiplatformKtor_client_authAuthProvider>> *providers __attribute__((swift_name("providers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngine")))
@protocol Mobile_multiplatformKtor_client_coreHttpClientEngine <Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope, Mobile_multiplatformKtor_ioCloseable>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeData:(Mobile_multiplatformKtor_client_coreHttpRequestData *)data completionHandler:(void (^)(Mobile_multiplatformKtor_client_coreHttpResponseData * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(data:completionHandler:)")));
- (void)installClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpClientEngineConfig *config __attribute__((swift_name("config")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@property (readonly) NSSet<id<Mobile_multiplatformKtor_client_coreHttpClientEngineCapability>> *supportedCapabilities __attribute__((swift_name("supportedCapabilities")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<Mobile_multiplatformKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<Mobile_multiplatformKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol Mobile_multiplatformKotlinx_serialization_coreKSerializer <Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy, Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface Mobile_multiplatformKotlinEnumCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface Mobile_multiplatformKotlinArray<T> : Mobile_multiplatformBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(Mobile_multiplatformInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<Mobile_multiplatformKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol Mobile_multiplatformKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<Mobile_multiplatformKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<Mobile_multiplatformKotlinCoroutineContextElement> _Nullable)getKey:(id<Mobile_multiplatformKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<Mobile_multiplatformKotlinCoroutineContext>)minusKeyKey:(id<Mobile_multiplatformKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<Mobile_multiplatformKotlinCoroutineContext>)plusContext:(id<Mobile_multiplatformKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol Mobile_multiplatformKotlinCoroutineContextElement <Mobile_multiplatformKotlinCoroutineContext>
@required
@property (readonly) id<Mobile_multiplatformKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextElement")))
@interface Mobile_multiplatformKotlinAbstractCoroutineContextElement : Mobile_multiplatformBase <Mobile_multiplatformKotlinCoroutineContextElement>
- (instancetype)initWithKey:(id<Mobile_multiplatformKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<Mobile_multiplatformKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinContinuationInterceptor")))
@protocol Mobile_multiplatformKotlinContinuationInterceptor <Mobile_multiplatformKotlinCoroutineContextElement>
@required
- (id<Mobile_multiplatformKotlinContinuation>)interceptContinuationContinuation:(id<Mobile_multiplatformKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (void)releaseInterceptedContinuationContinuation:(id<Mobile_multiplatformKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher")))
@interface Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher : Mobile_multiplatformKotlinAbstractCoroutineContextElement <Mobile_multiplatformKotlinContinuationInterceptor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithKey:(id<Mobile_multiplatformKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcherKey *companion __attribute__((swift_name("companion")));
- (void)dispatchContext:(id<Mobile_multiplatformKotlinCoroutineContext>)context block:(id<Mobile_multiplatformKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatch(context:block:)")));
- (void)dispatchYieldContext:(id<Mobile_multiplatformKotlinCoroutineContext>)context block:(id<Mobile_multiplatformKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatchYield(context:block:)")));
- (id<Mobile_multiplatformKotlinContinuation>)interceptContinuationContinuation:(id<Mobile_multiplatformKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (BOOL)isDispatchNeededContext:(id<Mobile_multiplatformKotlinCoroutineContext>)context __attribute__((swift_name("isDispatchNeeded(context:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)limitedParallelismParallelism:(int32_t)parallelism __attribute__((swift_name("limitedParallelism(parallelism:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)plusOther:(Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *)other __attribute__((swift_name("plus(other:)"))) __attribute__((unavailable("Operator '+' on two CoroutineDispatcher objects is meaningless. CoroutineDispatcher is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The dispatcher to the right of `+` just replaces the dispatcher to the left.")));
- (void)releaseInterceptedContinuationContinuation:(id<Mobile_multiplatformKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinUnit")))
@interface Mobile_multiplatformKotlinUnit : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unit __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKotlinUnit *shared __attribute__((swift_name("shared")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_httpHttpMessageBuilder")))
@protocol Mobile_multiplatformKtor_httpHttpMessageBuilder
@required
@property (readonly) Mobile_multiplatformKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder")))
@interface Mobile_multiplatformKtor_client_coreHttpRequestBuilder : Mobile_multiplatformBase <Mobile_multiplatformKtor_httpHttpMessageBuilder>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_client_coreHttpRequestBuilderCompanion *companion __attribute__((swift_name("companion")));
- (Mobile_multiplatformKtor_client_coreHttpRequestData *)build __attribute__((swift_name("build()")));
- (id _Nullable)getCapabilityOrNullKey:(id<Mobile_multiplatformKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (void)setAttributesBlock:(void (^)(id<Mobile_multiplatformKtor_utilsAttributes>))block __attribute__((swift_name("setAttributes(block:)")));
- (void)setCapabilityKey:(id<Mobile_multiplatformKtor_client_coreHttpClientEngineCapability>)key capability:(id)capability __attribute__((swift_name("setCapability(key:capability:)")));
- (Mobile_multiplatformKtor_client_coreHttpRequestBuilder *)takeFromBuilder:(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFrom(builder:)")));
- (Mobile_multiplatformKtor_client_coreHttpRequestBuilder *)takeFromWithExecutionContextBuilder:(Mobile_multiplatformKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFromWithExecutionContext(builder:)")));
- (void)urlBlock:(void (^)(Mobile_multiplatformKtor_httpURLBuilder *, Mobile_multiplatformKtor_httpURLBuilder *))block __attribute__((swift_name("url(block:)")));
@property (readonly) id<Mobile_multiplatformKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property id body __attribute__((swift_name("body")));
@property id<Mobile_multiplatformKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) Mobile_multiplatformKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@property Mobile_multiplatformKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) Mobile_multiplatformKtor_httpURLBuilder *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface Mobile_multiplatformKotlinPair<__covariant A, __covariant B> : Mobile_multiplatformBase
- (instancetype)initWithFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (A _Nullable)component1 __attribute__((swift_name("component1()")));
- (B _Nullable)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformKotlinPair<A, B> *)doCopyFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) A _Nullable first __attribute__((swift_name("first")));
@property (readonly) B _Nullable second __attribute__((swift_name("second")));
@end;

__attribute__((swift_name("Ktor_ioByteReadChannel")))
@protocol Mobile_multiplatformKtor_ioByteReadChannel
@required
- (BOOL)cancelCause:(Mobile_multiplatformKotlinThrowable * _Nullable)cause __attribute__((swift_name("cancel(cause:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)discardMax:(int64_t)max completionHandler:(void (^)(Mobile_multiplatformLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("discard(max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)peekToDestination:(Mobile_multiplatformKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max completionHandler:(void (^)(Mobile_multiplatformLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(Mobile_multiplatformKtor_ioIoBuffer *)dst completionHandler:(void (^)(Mobile_multiplatformInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(Mobile_multiplatformKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(Mobile_multiplatformInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(Mobile_multiplatformInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(Mobile_multiplatformInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readBooleanWithCompletionHandler:(void (^)(Mobile_multiplatformBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readBoolean(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readByteWithCompletionHandler:(void (^)(Mobile_multiplatformByte * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readByte(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readDoubleWithCompletionHandler:(void (^)(Mobile_multiplatformDouble * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readDouble(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFloatWithCompletionHandler:(void (^)(Mobile_multiplatformFloat * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFloat(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(Mobile_multiplatformKtor_ioIoBuffer *)dst n:(int32_t)n completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:n:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(Mobile_multiplatformKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readIntWithCompletionHandler:(void (^)(Mobile_multiplatformInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readInt(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readLongWithCompletionHandler:(void (^)(Mobile_multiplatformLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readLong(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readPacketSize:(int32_t)size headerSizeHint:(int32_t)headerSizeHint completionHandler:(void (^)(Mobile_multiplatformKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readPacket(size:headerSizeHint:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readRemainingLimit:(int64_t)limit headerSizeHint:(int32_t)headerSizeHint completionHandler:(void (^)(Mobile_multiplatformKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readRemaining(limit:headerSizeHint:completionHandler:)")));
- (void)readSessionConsumer:(void (^)(id<Mobile_multiplatformKtor_ioReadSession>))consumer __attribute__((swift_name("readSession(consumer:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readShortWithCompletionHandler:(void (^)(Mobile_multiplatformShort * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readShort(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readSuspendableSessionConsumer:(id<Mobile_multiplatformKotlinSuspendFunction1>)consumer completionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readSuspendableSession(consumer:completionHandler:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineLimit:(int32_t)limit completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8Line(limit:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineToOut:(id<Mobile_multiplatformKotlinAppendable>)out limit:(int32_t)limit completionHandler:(void (^)(Mobile_multiplatformBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8LineTo(out:limit:completionHandler:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@property (readonly) Mobile_multiplatformKotlinThrowable * _Nullable closedCause __attribute__((swift_name("closedCause")));
@property (readonly) BOOL isClosedForRead __attribute__((swift_name("isClosedForRead")));
@property (readonly) BOOL isClosedForWrite __attribute__((swift_name("isClosedForWrite")));
@property Mobile_multiplatformKtor_ioByteOrder *readByteOrder __attribute__((swift_name("readByteOrder"))) __attribute__((unavailable("Setting byte order is no longer supported. Read/write in big endian and use reverseByteOrder() extensions.")));
@property (readonly) int64_t totalBytesRead __attribute__((swift_name("totalBytesRead")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreStateFlow")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreStateFlow <Mobile_multiplatformKotlinx_coroutines_coreSharedFlow>
@required
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface Mobile_multiplatformKotlinx_serialization_coreSerializersModule : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)dumpToCollector:(id<Mobile_multiplatformKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<Mobile_multiplatformKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<Mobile_multiplatformKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<Mobile_multiplatformKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonConfiguration")))
@interface Mobile_multiplatformKotlinx_serialization_jsonJsonConfiguration : Mobile_multiplatformBase
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowSpecialFloatingPointValues __attribute__((swift_name("allowSpecialFloatingPointValues")));
@property (readonly) BOOL allowStructuredMapKeys __attribute__((swift_name("allowStructuredMapKeys")));
@property (readonly) NSString *classDiscriminator __attribute__((swift_name("classDiscriminator")));
@property (readonly) BOOL coerceInputValues __attribute__((swift_name("coerceInputValues")));
@property (readonly) BOOL encodeDefaults __attribute__((swift_name("encodeDefaults")));
@property (readonly) BOOL explicitNulls __attribute__((swift_name("explicitNulls")));
@property (readonly) BOOL ignoreUnknownKeys __attribute__((swift_name("ignoreUnknownKeys")));
@property (readonly) BOOL isLenient __attribute__((swift_name("isLenient")));
@property (readonly) BOOL prettyPrint __attribute__((swift_name("prettyPrint")));
@property (readonly) NSString *prettyPrintIndent __attribute__((swift_name("prettyPrintIndent")));
@property (readonly) BOOL useAlternativeNames __attribute__((swift_name("useAlternativeNames")));
@property (readonly) BOOL useArrayPolymorphism __attribute__((swift_name("useArrayPolymorphism")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJson.Default")))
@interface Mobile_multiplatformKotlinx_serialization_jsonJsonDefault : Mobile_multiplatformKotlinx_serialization_jsonJson
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithConfiguration:(Mobile_multiplatformKotlinx_serialization_jsonJsonConfiguration *)configuration serializersModule:(Mobile_multiplatformKotlinx_serialization_coreSerializersModule *)serializersModule __attribute__((swift_name("init(configuration:serializersModule:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)default_ __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKotlinx_serialization_jsonJsonDefault *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement")))
@interface Mobile_multiplatformKotlinx_serialization_jsonJsonElement : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) Mobile_multiplatformKotlinx_serialization_jsonJsonElementCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineConfig")))
@interface Mobile_multiplatformKtor_client_coreHttpClientEngineConfig : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property BOOL pipelining __attribute__((swift_name("pipelining")));
@property Mobile_multiplatformKtor_client_coreProxyConfig * _Nullable proxy __attribute__((swift_name("proxy")));
@property (readonly) Mobile_multiplatformKotlinNothing *response __attribute__((swift_name("response"))) __attribute__((unavailable("Response config is deprecated. See [HttpPlainText] feature for charset configuration")));
@property int32_t threadsCount __attribute__((swift_name("threadsCount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientConfig")))
@interface Mobile_multiplatformKtor_client_coreHttpClientConfig<T> : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (Mobile_multiplatformKtor_client_coreHttpClientConfig<T> *)clone __attribute__((swift_name("clone()")));
- (void)engineBlock:(void (^)(T))block __attribute__((swift_name("engine(block:)")));
- (void)installClient:(Mobile_multiplatformKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
- (void)installFeature:(id<Mobile_multiplatformKtor_client_coreHttpClientFeature>)feature configure:(void (^)(id))configure __attribute__((swift_name("install(feature:configure:)")));
- (void)installKey:(NSString *)key block:(void (^)(Mobile_multiplatformKtor_client_coreHttpClient *))block __attribute__((swift_name("install(key:block:)")));
- (void)plusAssignOther:(Mobile_multiplatformKtor_client_coreHttpClientConfig<T> *)other __attribute__((swift_name("plusAssign(other:)")));
@property BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property BOOL expectSuccess __attribute__((swift_name("expectSuccess")));
@property BOOL followRedirects __attribute__((swift_name("followRedirects")));
@property BOOL useDefaultTransformers __attribute__((swift_name("useDefaultTransformers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientCall")))
@interface Mobile_multiplatformKtor_client_coreHttpClientCall : Mobile_multiplatformBase <Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope>
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_client_coreHttpClientCallCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getResponseContentWithCompletionHandler:(void (^)(id<Mobile_multiplatformKtor_ioByteReadChannel> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getResponseContent(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveInfo:(Mobile_multiplatformKtor_client_coreTypeInfo *)info completionHandler:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(info:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveInfo:(id<Mobile_multiplatformKtor_utilsTypeInfo>)info completionHandler_:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(info:completionHandler_:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowDoubleReceive __attribute__((swift_name("allowDoubleReceive")));
@property (readonly) id<Mobile_multiplatformKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpClient * _Nullable client __attribute__((swift_name("client")));
@property (readonly) id<Mobile_multiplatformKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) id<Mobile_multiplatformKtor_client_coreHttpRequest> request __attribute__((swift_name("request")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpResponse *response __attribute__((swift_name("response")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineCapability")))
@protocol Mobile_multiplatformKtor_client_coreHttpClientEngineCapability
@required
@end;

__attribute__((swift_name("Ktor_utilsAttributes")))
@protocol Mobile_multiplatformKtor_utilsAttributes
@required
- (id)computeIfAbsentKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key block:(id (^)(void))block __attribute__((swift_name("computeIfAbsent(key:block:)")));
- (BOOL)containsKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("contains(key:)")));
- (id)getKey_:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("get(key_:)")));
- (id _Nullable)getOrNullKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getOrNull(key:)")));
- (void)putKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key value:(id)value __attribute__((swift_name("put(key:value:)")));
- (void)removeKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("remove(key:)")));
- (id)takeKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("take(key:)")));
- (id _Nullable)takeOrNullKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("takeOrNull(key:)")));
@property (readonly) NSArray<Mobile_multiplatformKtor_utilsAttributeKey<id> *> *allKeys __attribute__((swift_name("allKeys")));
@end;

__attribute__((swift_name("Ktor_utilsPipeline")))
@interface Mobile_multiplatformKtor_utilsPipeline<TSubject, TContext> : Mobile_multiplatformBase
- (instancetype)initWithPhase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<Mobile_multiplatformKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhases:(Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer));
- (void)addPhasePhase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase __attribute__((swift_name("addPhase(phase:)")));
- (void)afterIntercepted __attribute__((swift_name("afterIntercepted()")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeContext:(TContext)context subject:(TSubject)subject completionHandler:(void (^)(TSubject _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(context:subject:completionHandler:)")));
- (void)insertPhaseAfterReference:(Mobile_multiplatformKtor_utilsPipelinePhase *)reference phase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseAfter(reference:phase:)")));
- (void)insertPhaseBeforeReference:(Mobile_multiplatformKtor_utilsPipelinePhase *)reference phase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseBefore(reference:phase:)")));
- (void)interceptPhase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase block:(id<Mobile_multiplatformKotlinSuspendFunction2>)block __attribute__((swift_name("intercept(phase:block:)")));
- (void)mergeFrom:(Mobile_multiplatformKtor_utilsPipeline<TSubject, TContext> *)from __attribute__((swift_name("merge(from:)")));
@property (readonly) id<Mobile_multiplatformKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property (readonly) BOOL isEmpty __attribute__((swift_name("isEmpty")));
@property (readonly) NSArray<Mobile_multiplatformKtor_utilsPipelinePhase *> *items __attribute__((swift_name("items")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline")))
@interface Mobile_multiplatformKtor_client_coreHttpReceivePipeline : Mobile_multiplatformKtor_utilsPipeline<Mobile_multiplatformKtor_client_coreHttpResponse *, Mobile_multiplatformKtor_client_coreHttpClientCall *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<Mobile_multiplatformKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_client_coreHttpReceivePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline")))
@interface Mobile_multiplatformKtor_client_coreHttpRequestPipeline : Mobile_multiplatformKtor_utilsPipeline<id, Mobile_multiplatformKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<Mobile_multiplatformKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_client_coreHttpRequestPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline")))
@interface Mobile_multiplatformKtor_client_coreHttpResponsePipeline : Mobile_multiplatformKtor_utilsPipeline<Mobile_multiplatformKtor_client_coreHttpResponseContainer *, Mobile_multiplatformKtor_client_coreHttpClientCall *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<Mobile_multiplatformKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_client_coreHttpResponsePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline")))
@interface Mobile_multiplatformKtor_client_coreHttpSendPipeline : Mobile_multiplatformKtor_utilsPipeline<id, Mobile_multiplatformKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(Mobile_multiplatformKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<Mobile_multiplatformKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_client_coreHttpSendPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((swift_name("Ktor_httpHttpAuthHeader")))
@interface Mobile_multiplatformKtor_httpHttpAuthHeader : Mobile_multiplatformBase
- (instancetype)initWithAuthScheme:(NSString *)authScheme __attribute__((swift_name("init(authScheme:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpHttpAuthHeaderCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)render __attribute__((swift_name("render()")));
- (NSString *)renderEncoding:(Mobile_multiplatformKtor_httpHeaderValueEncoding *)encoding __attribute__((swift_name("render(encoding:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *authScheme __attribute__((swift_name("authScheme")));
@end;

__attribute__((swift_name("Ktor_httpHttpMessage")))
@protocol Mobile_multiplatformKtor_httpHttpMessage
@required
@property (readonly) id<Mobile_multiplatformKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpResponse")))
@interface Mobile_multiplatformKtor_client_coreHttpResponse : Mobile_multiplatformBase <Mobile_multiplatformKtor_httpHttpMessage, Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) id<Mobile_multiplatformKtor_ioByteReadChannel> content __attribute__((swift_name("content")));
@property (readonly) Mobile_multiplatformKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) Mobile_multiplatformKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *status __attribute__((swift_name("status")));
@property (readonly) Mobile_multiplatformKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_authAuth.Feature")))
@interface Mobile_multiplatformKtor_client_authAuthFeature : Mobile_multiplatformBase <Mobile_multiplatformKtor_client_coreHttpClientFeature>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)feature __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_client_authAuthFeature *shared __attribute__((swift_name("shared")));
- (void)installFeature:(Mobile_multiplatformKtor_client_authAuth *)feature scope:(Mobile_multiplatformKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(feature:scope:)")));
- (Mobile_multiplatformKtor_client_authAuth *)prepareBlock:(void (^)(Mobile_multiplatformKtor_client_authAuth *))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) Mobile_multiplatformKtor_utilsAttributeKey<Mobile_multiplatformKtor_client_authAuth *> *key __attribute__((swift_name("key")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestData")))
@interface Mobile_multiplatformKtor_client_coreHttpRequestData : Mobile_multiplatformBase
- (instancetype)initWithUrl:(Mobile_multiplatformKtor_httpUrl *)url method:(Mobile_multiplatformKtor_httpHttpMethod *)method headers:(id<Mobile_multiplatformKtor_httpHeaders>)headers body:(Mobile_multiplatformKtor_httpOutgoingContent *)body executionContext:(id<Mobile_multiplatformKotlinx_coroutines_coreJob>)executionContext attributes:(id<Mobile_multiplatformKtor_utilsAttributes>)attributes __attribute__((swift_name("init(url:method:headers:body:executionContext:attributes:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)getCapabilityOrNullKey:(id<Mobile_multiplatformKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<Mobile_multiplatformKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) Mobile_multiplatformKtor_httpOutgoingContent *body __attribute__((swift_name("body")));
@property (readonly) id<Mobile_multiplatformKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) id<Mobile_multiplatformKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) Mobile_multiplatformKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseData")))
@interface Mobile_multiplatformKtor_client_coreHttpResponseData : Mobile_multiplatformBase
- (instancetype)initWithStatusCode:(Mobile_multiplatformKtor_httpHttpStatusCode *)statusCode requestTime:(Mobile_multiplatformKtor_utilsGMTDate *)requestTime headers:(id<Mobile_multiplatformKtor_httpHeaders>)headers version:(Mobile_multiplatformKtor_httpHttpProtocolVersion *)version body:(id)body callContext:(id<Mobile_multiplatformKotlinCoroutineContext>)callContext __attribute__((swift_name("init(statusCode:requestTime:headers:version:body:callContext:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id body __attribute__((swift_name("body")));
@property (readonly) id<Mobile_multiplatformKotlinCoroutineContext> callContext __attribute__((swift_name("callContext")));
@property (readonly) id<Mobile_multiplatformKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) Mobile_multiplatformKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) Mobile_multiplatformKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *statusCode __attribute__((swift_name("statusCode")));
@property (readonly) Mobile_multiplatformKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol Mobile_multiplatformKotlinx_serialization_coreEncoder
@required
- (id<Mobile_multiplatformKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreEncoder>)encodeInlineInlineDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("encodeInline(inlineDescriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) Mobile_multiplatformKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<Mobile_multiplatformKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<Mobile_multiplatformKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) Mobile_multiplatformKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol Mobile_multiplatformKotlinx_serialization_coreDecoder
@required
- (id<Mobile_multiplatformKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<Mobile_multiplatformKotlinx_serialization_coreDecoder>)decodeInlineInlineDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("decodeInline(inlineDescriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (Mobile_multiplatformKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) Mobile_multiplatformKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol Mobile_multiplatformKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol Mobile_multiplatformKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol Mobile_multiplatformKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<Mobile_multiplatformKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextKey")))
@interface Mobile_multiplatformKotlinAbstractCoroutineContextKey<B, E> : Mobile_multiplatformBase <Mobile_multiplatformKotlinCoroutineContextKey>
- (instancetype)initWithBaseKey:(id<Mobile_multiplatformKotlinCoroutineContextKey>)baseKey safeCast:(E _Nullable (^)(id<Mobile_multiplatformKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher.Key")))
@interface Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcherKey : Mobile_multiplatformKotlinAbstractCoroutineContextKey<id<Mobile_multiplatformKotlinContinuationInterceptor>, Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcher *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithBaseKey:(id<Mobile_multiplatformKotlinCoroutineContextKey>)baseKey safeCast:(id<Mobile_multiplatformKotlinCoroutineContextElement> _Nullable (^)(id<Mobile_multiplatformKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)key __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKotlinx_coroutines_coreCoroutineDispatcherKey *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreRunnable")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreRunnable
@required
- (void)run __attribute__((swift_name("run()")));
@end;

__attribute__((swift_name("Ktor_utilsStringValuesBuilder")))
@interface Mobile_multiplatformKtor_utilsStringValuesBuilder : Mobile_multiplatformBase
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer));
- (void)appendName:(NSString *)name value:(NSString *)value __attribute__((swift_name("append(name:value:)")));
- (void)appendAllStringValues:(id<Mobile_multiplatformKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendAll(stringValues:)")));
- (void)appendAllName:(NSString *)name values:(id)values __attribute__((swift_name("appendAll(name:values:)")));
- (void)appendMissingStringValues:(id<Mobile_multiplatformKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendMissing(stringValues:)")));
- (void)appendMissingName:(NSString *)name values:(id)values __attribute__((swift_name("appendMissing(name:values:)")));
- (id<Mobile_multiplatformKtor_utilsStringValues>)build __attribute__((swift_name("build()")));
- (void)clear __attribute__((swift_name("clear()")));
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<Mobile_multiplatformKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
- (void)removeName:(NSString *)name __attribute__((swift_name("remove(name:)")));
- (BOOL)removeName:(NSString *)name value:(NSString *)value __attribute__((swift_name("remove(name:value:)")));
- (void)removeKeysWithNoEntries __attribute__((swift_name("removeKeysWithNoEntries()")));
- (void)setName:(NSString *)name value:(NSString *)value __attribute__((swift_name("set(name:value:)")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@property BOOL built __attribute__((swift_name("built")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@property (readonly) Mobile_multiplatformMutableDictionary<NSString *, NSMutableArray<NSString *> *> *values __attribute__((swift_name("values")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeadersBuilder")))
@interface Mobile_multiplatformKtor_httpHeadersBuilder : Mobile_multiplatformKtor_utilsStringValuesBuilder
- (instancetype)initWithSize:(int32_t)size __attribute__((swift_name("init(size:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<Mobile_multiplatformKtor_httpHeaders>)build __attribute__((swift_name("build()")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder.Companion")))
@interface Mobile_multiplatformKtor_client_coreHttpRequestBuilderCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_client_coreHttpRequestBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder")))
@interface Mobile_multiplatformKtor_httpURLBuilder : Mobile_multiplatformBase
- (instancetype)initWithProtocol:(Mobile_multiplatformKtor_httpURLProtocol *)protocol host:(NSString *)host port:(int32_t)port user:(NSString * _Nullable)user password:(NSString * _Nullable)password encodedPath:(NSString *)encodedPath parameters:(Mobile_multiplatformKtor_httpParametersBuilder *)parameters fragment:(NSString *)fragment trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:port:user:password:encodedPath:parameters:fragment:trailingQuery:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpURLBuilderCompanion *companion __attribute__((swift_name("companion")));
- (Mobile_multiplatformKtor_httpUrl *)build __attribute__((swift_name("build()")));
- (NSString *)buildString __attribute__((swift_name("buildString()")));
- (Mobile_multiplatformKtor_httpURLBuilder *)pathComponents:(Mobile_multiplatformKotlinArray<NSString *> *)components __attribute__((swift_name("path(components:)")));
- (Mobile_multiplatformKtor_httpURLBuilder *)pathComponents_:(NSArray<NSString *> *)components __attribute__((swift_name("path(components_:)")));
@property NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property NSString *fragment __attribute__((swift_name("fragment")));
@property NSString *host __attribute__((swift_name("host")));
@property (readonly) Mobile_multiplatformKtor_httpParametersBuilder *parameters __attribute__((swift_name("parameters")));
@property NSString * _Nullable password __attribute__((swift_name("password")));
@property int32_t port __attribute__((swift_name("port")));
@property Mobile_multiplatformKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreJob")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreJob <Mobile_multiplatformKotlinCoroutineContextElement>
@required
- (id<Mobile_multiplatformKotlinx_coroutines_coreChildHandle>)attachChildChild:(id<Mobile_multiplatformKotlinx_coroutines_coreChildJob>)child __attribute__((swift_name("attachChild(child:)")));
- (void)cancelCause_:(Mobile_multiplatformKotlinCancellationException * _Nullable)cause __attribute__((swift_name("cancel(cause_:)")));
- (Mobile_multiplatformKotlinCancellationException *)getCancellationException __attribute__((swift_name("getCancellationException()")));
- (id<Mobile_multiplatformKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionOnCancelling:(BOOL)onCancelling invokeImmediately:(BOOL)invokeImmediately handler:(void (^)(Mobile_multiplatformKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(onCancelling:invokeImmediately:handler:)")));
- (id<Mobile_multiplatformKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionHandler:(void (^)(Mobile_multiplatformKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(handler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)joinWithCompletionHandler:(void (^)(Mobile_multiplatformKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("join(completionHandler:)")));
- (id<Mobile_multiplatformKotlinx_coroutines_coreJob>)plusOther_:(id<Mobile_multiplatformKotlinx_coroutines_coreJob>)other __attribute__((swift_name("plus(other_:)"))) __attribute__((unavailable("Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")));
- (BOOL)start __attribute__((swift_name("start()")));
@property (readonly) id<Mobile_multiplatformKotlinSequence> children __attribute__((swift_name("children")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@property (readonly) BOOL isCancelled __attribute__((swift_name("isCancelled")));
@property (readonly) BOOL isCompleted __attribute__((swift_name("isCompleted")));
@property (readonly) id<Mobile_multiplatformKotlinx_coroutines_coreSelectClause0> onJoin __attribute__((swift_name("onJoin")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod")))
@interface Mobile_multiplatformKtor_httpHttpMethod : Mobile_multiplatformBase
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpHttpMethodCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (Mobile_multiplatformKtor_httpHttpMethod *)doCopyValue:(NSString *)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory")))
@interface Mobile_multiplatformKtor_ioMemory : Mobile_multiplatformBase
- (instancetype)initWithPointer:(void *)pointer size:(int64_t)size __attribute__((swift_name("init(pointer:size:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioMemoryCompanion *companion __attribute__((swift_name("companion")));
- (void)doCopyToDestination:(Mobile_multiplatformKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length destinationOffset:(int32_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset:)")));
- (void)doCopyToDestination:(Mobile_multiplatformKtor_ioMemory *)destination offset:(int64_t)offset length:(int64_t)length destinationOffset_:(int64_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset_:)")));
- (int8_t)loadAtIndex:(int32_t)index __attribute__((swift_name("loadAt(index:)")));
- (int8_t)loadAtIndex_:(int64_t)index __attribute__((swift_name("loadAt(index_:)")));
- (Mobile_multiplatformKtor_ioMemory *)sliceOffset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("slice(offset:length:)")));
- (Mobile_multiplatformKtor_ioMemory *)sliceOffset:(int64_t)offset length_:(int64_t)length __attribute__((swift_name("slice(offset:length_:)")));
- (void)storeAtIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("storeAt(index:value:)")));
- (void)storeAtIndex:(int64_t)index value_:(int8_t)value __attribute__((swift_name("storeAt(index:value_:)")));
@property (readonly) void *pointer __attribute__((swift_name("pointer")));
@property (readonly) int64_t size __attribute__((swift_name("size")));
@property (readonly) int32_t size32 __attribute__((swift_name("size32")));
@end;

__attribute__((swift_name("Ktor_ioBuffer")))
@interface Mobile_multiplatformKtor_ioBuffer : Mobile_multiplatformBase
- (instancetype)initWithMemory:(Mobile_multiplatformKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioBufferCompanion *companion __attribute__((swift_name("companion")));
- (void)commitWrittenCount:(int32_t)count __attribute__((swift_name("commitWritten(count:)")));
- (int32_t)discardCount:(int32_t)count __attribute__((swift_name("discard(count:)"))) __attribute__((unavailable("Use discardExact instead.")));
- (int64_t)discardCount_:(int64_t)count __attribute__((swift_name("discard(count_:)"))) __attribute__((unavailable("Use discardExact instead.")));
- (void)discardExactCount:(int32_t)count __attribute__((swift_name("discardExact(count:)")));
- (Mobile_multiplatformKtor_ioBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)duplicateToCopy:(Mobile_multiplatformKtor_ioBuffer *)copy __attribute__((swift_name("duplicateTo(copy:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (void)reserveEndGapEndGap:(int32_t)endGap __attribute__((swift_name("reserveEndGap(endGap:)")));
- (void)reserveStartGapStartGap:(int32_t)startGap __attribute__((swift_name("reserveStartGap(startGap:)")));
- (void)reset __attribute__((swift_name("reset()")));
- (void)resetForRead __attribute__((swift_name("resetForRead()")));
- (void)resetForWrite __attribute__((swift_name("resetForWrite()")));
- (void)resetForWriteLimit:(int32_t)limit __attribute__((swift_name("resetForWrite(limit:)")));
- (void)rewindCount:(int32_t)count __attribute__((swift_name("rewind(count:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (int32_t)tryPeekByte __attribute__((swift_name("tryPeekByte()")));
- (int32_t)tryReadByte __attribute__((swift_name("tryReadByte()")));
- (void)writeByteValue:(int8_t)value __attribute__((swift_name("writeByte(value:)")));
@property id _Nullable attachment __attribute__((swift_name("attachment"))) __attribute__((deprecated("Will be removed. Inherit Buffer and add required fields instead.")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@property (readonly) int32_t endGap __attribute__((swift_name("endGap")));
@property (readonly) int32_t limit __attribute__((swift_name("limit")));
@property (readonly) Mobile_multiplatformKtor_ioMemory *memory __attribute__((swift_name("memory")));
@property (readonly) int32_t readPosition __attribute__((swift_name("readPosition")));
@property (readonly) int32_t readRemaining __attribute__((swift_name("readRemaining")));
@property (readonly) int32_t startGap __attribute__((swift_name("startGap")));
@property (readonly) int32_t writePosition __attribute__((swift_name("writePosition")));
@property (readonly) int32_t writeRemaining __attribute__((swift_name("writeRemaining")));
@end;

__attribute__((swift_name("Ktor_ioChunkBuffer")))
@interface Mobile_multiplatformKtor_ioChunkBuffer : Mobile_multiplatformKtor_ioBuffer
- (instancetype)initWithMemory:(Mobile_multiplatformKtor_ioMemory *)memory origin:(Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)origin parentPool:(id<Mobile_multiplatformKtor_ioObjectPool> _Nullable)parentPool __attribute__((swift_name("init(memory:origin:parentPool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMemory:(Mobile_multiplatformKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioChunkBufferCompanion *companion __attribute__((swift_name("companion")));
- (Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)cleanNext __attribute__((swift_name("cleanNext()")));
- (Mobile_multiplatformKtor_ioChunkBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)releasePool:(id<Mobile_multiplatformKtor_ioObjectPool>)pool __attribute__((swift_name("release(pool:)")));
- (void)reset __attribute__((swift_name("reset()")));
@property (getter=next_) Mobile_multiplatformKtor_ioChunkBuffer * _Nullable next __attribute__((swift_name("next")));
@property (readonly) Mobile_multiplatformKtor_ioChunkBuffer * _Nullable origin __attribute__((swift_name("origin")));
@property (readonly) int32_t referenceCount __attribute__((swift_name("referenceCount")));
@end;

__attribute__((swift_name("Ktor_ioInput")))
@protocol Mobile_multiplatformKtor_ioInput <Mobile_multiplatformKtor_ioCloseable>
@required
- (int64_t)discardN:(int64_t)n __attribute__((swift_name("discard(n:)")));
- (int64_t)peekToDestination:(Mobile_multiplatformKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
@property Mobile_multiplatformKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default. Use readXXXLittleEndian or readXXX then X.reverseByteOrder() instead.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@end;

__attribute__((swift_name("KotlinAppendable")))
@protocol Mobile_multiplatformKotlinAppendable
@required
- (id<Mobile_multiplatformKotlinAppendable>)appendValue:(unichar)value __attribute__((swift_name("append(value:)")));
- (id<Mobile_multiplatformKotlinAppendable>)appendValue_:(id _Nullable)value __attribute__((swift_name("append(value_:)")));
- (id<Mobile_multiplatformKotlinAppendable>)appendValue:(id _Nullable)value startIndex:(int32_t)startIndex endIndex:(int32_t)endIndex __attribute__((swift_name("append(value:startIndex:endIndex:)")));
@end;

__attribute__((swift_name("Ktor_ioOutput")))
@protocol Mobile_multiplatformKtor_ioOutput <Mobile_multiplatformKotlinAppendable, Mobile_multiplatformKtor_ioCloseable>
@required
- (id<Mobile_multiplatformKotlinAppendable>)appendCsq:(Mobile_multiplatformKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("append(csq:start:end:)")));
- (void)flush __attribute__((swift_name("flush()")));
- (void)writeByteV:(int8_t)v __attribute__((swift_name("writeByte(v:)")));
@property Mobile_multiplatformKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((deprecated("Write with writeXXXLittleEndian or do X.reverseByteOrder() and then writeXXX instead.")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioIoBuffer")))
@interface Mobile_multiplatformKtor_ioIoBuffer : Mobile_multiplatformKtor_ioChunkBuffer <Mobile_multiplatformKtor_ioInput, Mobile_multiplatformKtor_ioOutput>
- (instancetype)initWithMemory:(Mobile_multiplatformKtor_ioMemory *)memory origin:(Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)origin __attribute__((swift_name("init(memory:origin:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use Buffer instead.")));
- (instancetype)initWithContent:(void *)content contentCapacity:(int32_t)contentCapacity __attribute__((swift_name("init(content:contentCapacity:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use Buffer instead.")));
- (instancetype)initWithMemory:(Mobile_multiplatformKtor_ioMemory *)memory origin:(Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)origin parentPool:(id<Mobile_multiplatformKtor_ioObjectPool> _Nullable)parentPool __attribute__((swift_name("init(memory:origin:parentPool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioIoBufferCompanion *companion __attribute__((swift_name("companion")));
- (id<Mobile_multiplatformKotlinAppendable>)appendValue:(unichar)c __attribute__((swift_name("append(value:)")));
- (id<Mobile_multiplatformKotlinAppendable>)appendCsq:(Mobile_multiplatformKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("append(csq:start:end:)")));
- (id<Mobile_multiplatformKotlinAppendable>)appendValue_:(id _Nullable)csq __attribute__((swift_name("append(value_:)")));
- (id<Mobile_multiplatformKotlinAppendable>)appendValue:(id _Nullable)csq startIndex:(int32_t)start endIndex:(int32_t)end __attribute__((swift_name("append(value:startIndex:endIndex:)")));
- (int32_t)appendCharsCsq:(Mobile_multiplatformKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("appendChars(csq:start:end:)")));
- (int32_t)appendCharsCsq:(id)csq start:(int32_t)start end_:(int32_t)end __attribute__((swift_name("appendChars(csq:start:end_:)")));
- (void)close __attribute__((swift_name("close()")));
- (Mobile_multiplatformKtor_ioIoBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)flush __attribute__((swift_name("flush()")));
- (Mobile_multiplatformKtor_ioIoBuffer *)makeView __attribute__((swift_name("makeView()")));
- (int64_t)peekToDestination:(Mobile_multiplatformKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (int32_t)readDirectBlock:(Mobile_multiplatformInt *(^)(id))block __attribute__((swift_name("readDirect(block:)")));
- (void)releasePool_:(id<Mobile_multiplatformKtor_ioObjectPool>)pool __attribute__((swift_name("release(pool_:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
- (int32_t)writeDirectBlock:(Mobile_multiplatformInt *(^)(id))block __attribute__((swift_name("writeDirect(block:)")));
@property Mobile_multiplatformKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface Mobile_multiplatformKotlinByteArray : Mobile_multiplatformBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(Mobile_multiplatformByte *(^)(Mobile_multiplatformInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (Mobile_multiplatformKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Ktor_ioAbstractInput")))
@interface Mobile_multiplatformKtor_ioAbstractInput : Mobile_multiplatformBase <Mobile_multiplatformKtor_ioInput>
- (instancetype)initWithHead:(Mobile_multiplatformKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<Mobile_multiplatformKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("AbstractInput is deprecated and will be merged with Input in 2.0.0")));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioAbstractInputCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)canRead __attribute__((swift_name("canRead()")));
- (void)close __attribute__((swift_name("close()")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (int32_t)discardN_:(int32_t)n __attribute__((swift_name("discard(n_:)")));
- (int64_t)discardN:(int64_t)n __attribute__((swift_name("discard(n:)")));
- (void)discardExactN:(int32_t)n __attribute__((swift_name("discardExact(n:)")));
- (Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)ensureNextHeadCurrent:(Mobile_multiplatformKtor_ioChunkBuffer *)current __attribute__((swift_name("ensureNextHead(current:)")));
- (Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(Mobile_multiplatformKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (void)fixGapAfterReadCurrent:(Mobile_multiplatformKtor_ioChunkBuffer *)current __attribute__((swift_name("fixGapAfterRead(current:)")));
- (BOOL)hasBytesN:(int32_t)n __attribute__((swift_name("hasBytes(n:)")));
- (void)markNoMoreChunksAvailable __attribute__((swift_name("markNoMoreChunksAvailable()")));
- (int64_t)peekToDestination:(Mobile_multiplatformKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)prepareReadHeadMinSize:(int32_t)minSize __attribute__((swift_name("prepareReadHead(minSize:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (NSString *)readTextMin:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(min:max:)")));
- (int32_t)readTextOut:(id<Mobile_multiplatformKotlinAppendable>)out min:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(out:min:max:)")));
- (NSString *)readTextExactExactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(exactCharacters:)")));
- (void)readTextExactOut:(id<Mobile_multiplatformKotlinAppendable>)out exactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(out:exactCharacters:)")));
- (void)release_ __attribute__((swift_name("release()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
- (void)updateHeadRemainingRemaining:(int32_t)remaining __attribute__((swift_name("updateHeadRemaining(remaining:)"))) __attribute__((unavailable("Not supported anymore.")));
@property Mobile_multiplatformKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@property (readonly) id<Mobile_multiplatformKtor_ioObjectPool> pool __attribute__((swift_name("pool")));
@property (readonly) int64_t remaining __attribute__((swift_name("remaining")));
@end;

__attribute__((swift_name("Ktor_ioByteReadPacketBase")))
@interface Mobile_multiplatformKtor_ioByteReadPacketBase : Mobile_multiplatformKtor_ioAbstractInput
- (instancetype)initWithHead:(Mobile_multiplatformKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<Mobile_multiplatformKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Will be removed in the future releases. Use Input or AbstractInput instead.")));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioByteReadPacketBaseCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((swift_name("Ktor_ioByteReadPacketPlatformBase")))
@interface Mobile_multiplatformKtor_ioByteReadPacketPlatformBase : Mobile_multiplatformKtor_ioByteReadPacketBase
- (instancetype)initWithHead:(Mobile_multiplatformKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<Mobile_multiplatformKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable("Will be removed in future releases.")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket")))
@interface Mobile_multiplatformKtor_ioByteReadPacket : Mobile_multiplatformKtor_ioByteReadPacketPlatformBase <Mobile_multiplatformKtor_ioInput>
- (instancetype)initWithHead:(Mobile_multiplatformKtor_ioChunkBuffer *)head pool:(id<Mobile_multiplatformKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:pool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithHead:(Mobile_multiplatformKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<Mobile_multiplatformKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioByteReadPacketCompanion *companion __attribute__((swift_name("companion")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (Mobile_multiplatformKtor_ioByteReadPacket *)doCopy __attribute__((swift_name("doCopy()")));
- (Mobile_multiplatformKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(Mobile_multiplatformKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_ioReadSession")))
@protocol Mobile_multiplatformKtor_ioReadSession
@required
- (int32_t)discardN_:(int32_t)n __attribute__((swift_name("discard(n_:)")));
- (Mobile_multiplatformKtor_ioIoBuffer * _Nullable)requestAtLeast:(int32_t)atLeast __attribute__((swift_name("request(atLeast:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@end;

__attribute__((swift_name("KotlinFunction")))
@protocol Mobile_multiplatformKotlinFunction
@required
@end;

__attribute__((swift_name("KotlinSuspendFunction1")))
@protocol Mobile_multiplatformKotlinSuspendFunction1 <Mobile_multiplatformKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteOrder")))
@interface Mobile_multiplatformKtor_ioByteOrder : Mobile_multiplatformKotlinEnum<Mobile_multiplatformKtor_ioByteOrder *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioByteOrderCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) Mobile_multiplatformKtor_ioByteOrder *bigEndian __attribute__((swift_name("bigEndian")));
@property (class, readonly) Mobile_multiplatformKtor_ioByteOrder *littleEndian __attribute__((swift_name("littleEndian")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_ioByteOrder *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol Mobile_multiplatformKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<Mobile_multiplatformKotlinKClass>)kClass provider:(id<Mobile_multiplatformKotlinx_serialization_coreKSerializer> (^)(NSArray<id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<Mobile_multiplatformKotlinKClass>)kClass serializer:(id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<Mobile_multiplatformKotlinKClass>)baseClass actualClass:(id<Mobile_multiplatformKotlinKClass>)actualClass actualSerializer:(id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<Mobile_multiplatformKotlinKClass>)baseClass defaultDeserializerProvider:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultDeserializerBaseClass:(id<Mobile_multiplatformKotlinKClass>)baseClass defaultDeserializerProvider:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefaultDeserializer(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultSerializerBaseClass:(id<Mobile_multiplatformKotlinKClass>)baseClass defaultSerializerProvider:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy> _Nullable (^)(id))defaultSerializerProvider __attribute__((swift_name("polymorphicDefaultSerializer(baseClass:defaultSerializerProvider:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement.Companion")))
@interface Mobile_multiplatformKotlinx_serialization_jsonJsonElementCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKotlinx_serialization_jsonJsonElementCompanion *shared __attribute__((swift_name("shared")));
- (id<Mobile_multiplatformKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreProxyConfig")))
@interface Mobile_multiplatformKtor_client_coreProxyConfig : Mobile_multiplatformBase
- (instancetype)initWithUrl:(Mobile_multiplatformKtor_httpUrl *)url __attribute__((swift_name("init(url:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface Mobile_multiplatformKotlinNothing : Mobile_multiplatformBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientCall.Companion")))
@interface Mobile_multiplatformKtor_client_coreHttpClientCallCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_client_coreHttpClientCallCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_utilsAttributeKey<id> *CustomResponse __attribute__((swift_name("CustomResponse"))) __attribute__((deprecated("This is going to be removed. Please file a ticket with clarification why and what for do you need it.")));
@end;

__attribute__((swift_name("Ktor_utilsTypeInfo")))
@protocol Mobile_multiplatformKtor_utilsTypeInfo
@required
@property (readonly) id<Mobile_multiplatformKotlinKType> _Nullable kotlinType __attribute__((swift_name("kotlinType")));
@property (readonly) id<Mobile_multiplatformKotlinKType> reifiedType __attribute__((swift_name("reifiedType")));
@property (readonly) id<Mobile_multiplatformKotlinKClass> type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreTypeInfo")))
@interface Mobile_multiplatformKtor_client_coreTypeInfo : Mobile_multiplatformBase <Mobile_multiplatformKtor_utilsTypeInfo>
- (instancetype)initWithType:(id<Mobile_multiplatformKotlinKClass>)type reifiedType:(id<Mobile_multiplatformKotlinKType>)reifiedType kotlinType:(id<Mobile_multiplatformKotlinKType> _Nullable)kotlinType __attribute__((swift_name("init(type:reifiedType:kotlinType:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("This was moved to another package.")));
- (id<Mobile_multiplatformKotlinKClass>)component1 __attribute__((swift_name("component1()")));
- (id<Mobile_multiplatformKotlinKType>)component2 __attribute__((swift_name("component2()")));
- (id<Mobile_multiplatformKotlinKType> _Nullable)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformKtor_client_coreTypeInfo *)doCopyType:(id<Mobile_multiplatformKotlinKClass>)type reifiedType:(id<Mobile_multiplatformKotlinKType>)reifiedType kotlinType:(id<Mobile_multiplatformKotlinKType> _Nullable)kotlinType __attribute__((swift_name("doCopy(type:reifiedType:kotlinType:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<Mobile_multiplatformKotlinKType> _Nullable kotlinType __attribute__((swift_name("kotlinType")));
@property (readonly) id<Mobile_multiplatformKotlinKType> reifiedType __attribute__((swift_name("reifiedType")));
@property (readonly) id<Mobile_multiplatformKotlinKClass> type __attribute__((swift_name("type")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpRequest")))
@protocol Mobile_multiplatformKtor_client_coreHttpRequest <Mobile_multiplatformKtor_httpHttpMessage, Mobile_multiplatformKotlinx_coroutines_coreCoroutineScope>
@required
@property (readonly) id<Mobile_multiplatformKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) Mobile_multiplatformKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) Mobile_multiplatformKtor_httpOutgoingContent *content __attribute__((swift_name("content")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) Mobile_multiplatformKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsPipelinePhase")))
@interface Mobile_multiplatformKtor_utilsPipelinePhase : Mobile_multiplatformBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol Mobile_multiplatformKotlinSuspendFunction2 <Mobile_multiplatformKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 p2:(id _Nullable)p2 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:p2:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline.Phases")))
@interface Mobile_multiplatformKtor_client_coreHttpReceivePipelinePhases : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_client_coreHttpReceivePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline.Phases")))
@interface Mobile_multiplatformKtor_client_coreHttpRequestPipelinePhases : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_client_coreHttpRequestPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Render __attribute__((swift_name("Render")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Send __attribute__((swift_name("Send")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline.Phases")))
@interface Mobile_multiplatformKtor_client_coreHttpResponsePipelinePhases : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_client_coreHttpResponsePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Parse __attribute__((swift_name("Parse")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseContainer")))
@interface Mobile_multiplatformKtor_client_coreHttpResponseContainer : Mobile_multiplatformBase
- (instancetype)initWithExpectedType:(id<Mobile_multiplatformKtor_utilsTypeInfo>)expectedType response:(id)response __attribute__((swift_name("init(expectedType:response:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithExpectedType:(Mobile_multiplatformKtor_client_coreTypeInfo *)expectedType response_:(id)response __attribute__((swift_name("init(expectedType:response_:)"))) __attribute__((objc_designated_initializer));
- (Mobile_multiplatformKtor_client_coreTypeInfo *)component1 __attribute__((swift_name("component1()")));
- (id)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformKtor_client_coreHttpResponseContainer *)doCopyExpectedType:(Mobile_multiplatformKtor_client_coreTypeInfo *)expectedType response:(id)response __attribute__((swift_name("doCopy(expectedType:response:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformKtor_client_coreTypeInfo *expectedType __attribute__((swift_name("expectedType")));
@property (readonly) id response __attribute__((swift_name("response")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline.Phases")))
@interface Mobile_multiplatformKtor_client_coreHttpSendPipelinePhases : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_client_coreHttpSendPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Engine __attribute__((swift_name("Engine")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Monitoring __attribute__((swift_name("Monitoring")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) Mobile_multiplatformKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpAuthHeader.Companion")))
@interface Mobile_multiplatformKtor_httpHttpAuthHeaderCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpHttpAuthHeaderCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_httpHttpAuthHeaderParameterized *)basicAuthChallengeRealm:(NSString *)realm charset:(Mobile_multiplatformKtor_ioCharset * _Nullable)charset __attribute__((swift_name("basicAuthChallenge(realm:charset:)")));
- (Mobile_multiplatformKtor_httpHttpAuthHeaderParameterized *)digestAuthChallengeRealm:(NSString *)realm nonce:(NSString *)nonce domain:(NSArray<NSString *> *)domain opaque:(NSString * _Nullable)opaque stale:(Mobile_multiplatformBoolean * _Nullable)stale algorithm:(NSString *)algorithm __attribute__((swift_name("digestAuthChallenge(realm:nonce:domain:opaque:stale:algorithm:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueEncoding")))
@interface Mobile_multiplatformKtor_httpHeaderValueEncoding : Mobile_multiplatformKotlinEnum<Mobile_multiplatformKtor_httpHeaderValueEncoding *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Mobile_multiplatformKtor_httpHeaderValueEncoding *quotedWhenRequired __attribute__((swift_name("quotedWhenRequired")));
@property (class, readonly) Mobile_multiplatformKtor_httpHeaderValueEncoding *quotedAlways __attribute__((swift_name("quotedAlways")));
@property (class, readonly) Mobile_multiplatformKtor_httpHeaderValueEncoding *uriEncode __attribute__((swift_name("uriEncode")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_httpHeaderValueEncoding *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("Ktor_utilsStringValues")))
@protocol Mobile_multiplatformKtor_utilsStringValues
@required
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<Mobile_multiplatformKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (void)forEachBody:(void (^)(NSString *, NSArray<NSString *> *))body __attribute__((swift_name("forEach(body:)")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty_ __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@end;

__attribute__((swift_name("Ktor_httpHeaders")))
@protocol Mobile_multiplatformKtor_httpHeaders <Mobile_multiplatformKtor_utilsStringValues>
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate")))
@interface Mobile_multiplatformKtor_utilsGMTDate : Mobile_multiplatformBase <Mobile_multiplatformKotlinComparable>
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_utilsGMTDateCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(Mobile_multiplatformKtor_utilsGMTDate *)other __attribute__((swift_name("compareTo(other:)")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformKtor_utilsWeekDay *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (Mobile_multiplatformKtor_utilsMonth *)component7 __attribute__((swift_name("component7()")));
- (int32_t)component8 __attribute__((swift_name("component8()")));
- (int64_t)component9 __attribute__((swift_name("component9()")));
- (Mobile_multiplatformKtor_utilsGMTDate *)doCopySeconds:(int32_t)seconds minutes:(int32_t)minutes hours:(int32_t)hours dayOfWeek:(Mobile_multiplatformKtor_utilsWeekDay *)dayOfWeek dayOfMonth:(int32_t)dayOfMonth dayOfYear:(int32_t)dayOfYear month:(Mobile_multiplatformKtor_utilsMonth *)month year:(int32_t)year timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(seconds:minutes:hours:dayOfWeek:dayOfMonth:dayOfYear:month:year:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t dayOfMonth __attribute__((swift_name("dayOfMonth")));
@property (readonly) Mobile_multiplatformKtor_utilsWeekDay *dayOfWeek __attribute__((swift_name("dayOfWeek")));
@property (readonly) int32_t dayOfYear __attribute__((swift_name("dayOfYear")));
@property (readonly) int32_t hours __attribute__((swift_name("hours")));
@property (readonly) int32_t minutes __attribute__((swift_name("minutes")));
@property (readonly) Mobile_multiplatformKtor_utilsMonth *month __attribute__((swift_name("month")));
@property (readonly) int32_t seconds __attribute__((swift_name("seconds")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@property (readonly) int32_t year __attribute__((swift_name("year")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode")))
@interface Mobile_multiplatformKtor_httpHttpStatusCode : Mobile_multiplatformBase
- (instancetype)initWithValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("init(value:description:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpHttpStatusCodeCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformKtor_httpHttpStatusCode *)doCopyValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("doCopy(value:description:)")));
- (Mobile_multiplatformKtor_httpHttpStatusCode *)descriptionValue:(NSString *)value __attribute__((swift_name("description(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *description_ __attribute__((swift_name("description_")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion")))
@interface Mobile_multiplatformKtor_httpHttpProtocolVersion : Mobile_multiplatformBase
- (instancetype)initWithName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("init(name:major:minor:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpHttpProtocolVersionCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (Mobile_multiplatformKtor_httpHttpProtocolVersion *)doCopyName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("doCopy(name:major:minor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t major __attribute__((swift_name("major")));
@property (readonly) int32_t minor __attribute__((swift_name("minor")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl")))
@interface Mobile_multiplatformKtor_httpUrl : Mobile_multiplatformBase
- (instancetype)initWithProtocol:(Mobile_multiplatformKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<Mobile_multiplatformKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpUrlCompanion *companion __attribute__((swift_name("companion")));
- (Mobile_multiplatformKtor_httpURLProtocol *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (id<Mobile_multiplatformKtor_httpParameters>)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (BOOL)component9 __attribute__((swift_name("component9()")));
- (Mobile_multiplatformKtor_httpUrl *)doCopyProtocol:(Mobile_multiplatformKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<Mobile_multiplatformKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("doCopy(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property (readonly) NSString *fragment __attribute__((swift_name("fragment")));
@property (readonly) NSString *host __attribute__((swift_name("host")));
@property (readonly) id<Mobile_multiplatformKtor_httpParameters> parameters __attribute__((swift_name("parameters")));
@property (readonly) NSString * _Nullable password __attribute__((swift_name("password")));
@property (readonly) int32_t port __attribute__((swift_name("port")));
@property (readonly) Mobile_multiplatformKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property (readonly) int32_t specifiedPort __attribute__((swift_name("specifiedPort")));
@property (readonly) BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property (readonly) NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((swift_name("Ktor_httpOutgoingContent")))
@interface Mobile_multiplatformKtor_httpOutgoingContent : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id _Nullable)getPropertyKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getProperty(key:)")));
- (void)setPropertyKey:(Mobile_multiplatformKtor_utilsAttributeKey<id> *)key value:(id _Nullable)value __attribute__((swift_name("setProperty(key:value:)")));
@property (readonly) Mobile_multiplatformLong * _Nullable contentLength __attribute__((swift_name("contentLength")));
@property (readonly) Mobile_multiplatformKtor_httpContentType * _Nullable contentType __attribute__((swift_name("contentType")));
@property (readonly) id<Mobile_multiplatformKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol Mobile_multiplatformKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<Mobile_multiplatformKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) Mobile_multiplatformKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol Mobile_multiplatformKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface Mobile_multiplatformKotlinx_serialization_coreSerialKind : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol Mobile_multiplatformKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<Mobile_multiplatformKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<Mobile_multiplatformKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<Mobile_multiplatformKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) Mobile_multiplatformKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("KotlinMapEntry")))
@protocol Mobile_multiplatformKotlinMapEntry
@required
@property (readonly) id _Nullable key __attribute__((swift_name("key")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol")))
@interface Mobile_multiplatformKtor_httpURLProtocol : Mobile_multiplatformBase
- (instancetype)initWithName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("init(name:defaultPort:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpURLProtocolCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformKtor_httpURLProtocol *)doCopyName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("doCopy(name:defaultPort:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t defaultPort __attribute__((swift_name("defaultPort")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpParametersBuilder")))
@interface Mobile_multiplatformKtor_httpParametersBuilder : Mobile_multiplatformKtor_utilsStringValuesBuilder
- (instancetype)initWithSize:(int32_t)size urlEncodingOption:(Mobile_multiplatformKtor_httpUrlEncodingOption *)urlEncodingOption __attribute__((swift_name("init(size:urlEncodingOption:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<Mobile_multiplatformKtor_httpParameters>)build __attribute__((swift_name("build()")));
@property Mobile_multiplatformKtor_httpUrlEncodingOption *urlEncodingOption __attribute__((swift_name("urlEncodingOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder.Companion")))
@interface Mobile_multiplatformKtor_httpURLBuilderCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpURLBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreDisposableHandle")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreDisposableHandle
@required
- (void)dispose __attribute__((swift_name("dispose()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildHandle")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreChildHandle <Mobile_multiplatformKotlinx_coroutines_coreDisposableHandle>
@required
- (BOOL)childCancelledCause:(Mobile_multiplatformKotlinThrowable *)cause __attribute__((swift_name("childCancelled(cause:)")));
@property (readonly) id<Mobile_multiplatformKotlinx_coroutines_coreJob> _Nullable parent __attribute__((swift_name("parent")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildJob")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreChildJob <Mobile_multiplatformKotlinx_coroutines_coreJob>
@required
- (void)parentCancelledParentJob:(id<Mobile_multiplatformKotlinx_coroutines_coreParentJob>)parentJob __attribute__((swift_name("parentCancelled(parentJob:)")));
@end;

__attribute__((swift_name("KotlinSequence")))
@protocol Mobile_multiplatformKotlinSequence
@required
- (id<Mobile_multiplatformKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectClause0")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreSelectClause0
@required
- (void)registerSelectClause0Select:(id<Mobile_multiplatformKotlinx_coroutines_coreSelectInstance>)select block:(id<Mobile_multiplatformKotlinSuspendFunction0>)block __attribute__((swift_name("registerSelectClause0(select:block:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod.Companion")))
@interface Mobile_multiplatformKtor_httpHttpMethodCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpHttpMethodCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_httpHttpMethod *)parseMethod:(NSString *)method __attribute__((swift_name("parse(method:)")));
@property (readonly) NSArray<Mobile_multiplatformKtor_httpHttpMethod *> *DefaultMethods __attribute__((swift_name("DefaultMethods")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *Delete __attribute__((swift_name("Delete")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *Get __attribute__((swift_name("Get")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *Head __attribute__((swift_name("Head")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *Options __attribute__((swift_name("Options")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *Patch __attribute__((swift_name("Patch")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *Post __attribute__((swift_name("Post")));
@property (readonly) Mobile_multiplatformKtor_httpHttpMethod *Put __attribute__((swift_name("Put")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory.Companion")))
@interface Mobile_multiplatformKtor_ioMemoryCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioMemoryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_ioMemory *Empty __attribute__((swift_name("Empty")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioBuffer.Companion")))
@interface Mobile_multiplatformKtor_ioBufferCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_ioBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((swift_name("Ktor_ioObjectPool")))
@protocol Mobile_multiplatformKtor_ioObjectPool <Mobile_multiplatformKtor_ioCloseable>
@required
- (id)borrow __attribute__((swift_name("borrow()")));
- (void)dispose __attribute__((swift_name("dispose()")));
- (void)recycleInstance:(id)instance __attribute__((swift_name("recycle(instance:)")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioChunkBuffer.Companion")))
@interface Mobile_multiplatformKtor_ioChunkBufferCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioChunkBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_ioChunkBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) id<Mobile_multiplatformKtor_ioObjectPool> EmptyPool __attribute__((swift_name("EmptyPool")));
@property (readonly) id<Mobile_multiplatformKtor_ioObjectPool> Pool __attribute__((swift_name("Pool")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinCharArray")))
@interface Mobile_multiplatformKotlinCharArray : Mobile_multiplatformBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(id (^)(Mobile_multiplatformInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (unichar)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (Mobile_multiplatformKotlinCharIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(unichar)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioIoBuffer.Companion")))
@interface Mobile_multiplatformKtor_ioIoBufferCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioIoBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_ioIoBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) id<Mobile_multiplatformKtor_ioObjectPool> EmptyPool __attribute__((swift_name("EmptyPool")));
@property (readonly) id<Mobile_multiplatformKtor_ioObjectPool> NoPool __attribute__((swift_name("NoPool")));
@property (readonly) id<Mobile_multiplatformKtor_ioObjectPool> Pool __attribute__((swift_name("Pool")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((swift_name("KotlinByteIterator")))
@interface Mobile_multiplatformKotlinByteIterator : Mobile_multiplatformBase <Mobile_multiplatformKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (Mobile_multiplatformByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioAbstractInput.Companion")))
@interface Mobile_multiplatformKtor_ioAbstractInputCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioAbstractInputCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacketBase.Companion")))
@interface Mobile_multiplatformKtor_ioByteReadPacketBaseCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioByteReadPacketBaseCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_ioByteReadPacket *Empty __attribute__((swift_name("Empty"))) __attribute__((unavailable("Use ByteReadPacket.Empty instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket.Companion")))
@interface Mobile_multiplatformKtor_ioByteReadPacketCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioByteReadPacketCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_ioByteReadPacket *Empty __attribute__((swift_name("Empty")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteOrder.Companion")))
@interface Mobile_multiplatformKtor_ioByteOrderCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioByteOrderCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_ioByteOrder *)nativeOrder __attribute__((swift_name("nativeOrder()")));
@end;

__attribute__((swift_name("KotlinKType")))
@protocol Mobile_multiplatformKotlinKType
@required
@property (readonly) NSArray<Mobile_multiplatformKotlinKTypeProjection *> *arguments __attribute__((swift_name("arguments")));
@property (readonly) id<Mobile_multiplatformKotlinKClassifier> _Nullable classifier __attribute__((swift_name("classifier")));
@property (readonly) BOOL isMarkedNullable __attribute__((swift_name("isMarkedNullable")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpAuthHeader.Parameterized")))
@interface Mobile_multiplatformKtor_httpHttpAuthHeaderParameterized : Mobile_multiplatformKtor_httpHttpAuthHeader
- (instancetype)initWithAuthScheme:(NSString *)authScheme parameters:(NSDictionary<NSString *, NSString *> *)parameters encoding:(Mobile_multiplatformKtor_httpHeaderValueEncoding *)encoding __attribute__((swift_name("init(authScheme:parameters:encoding:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithAuthScheme:(NSString *)authScheme parameters:(NSArray<Mobile_multiplatformKtor_httpHeaderValueParam *> *)parameters encoding_:(Mobile_multiplatformKtor_httpHeaderValueEncoding *)encoding __attribute__((swift_name("init(authScheme:parameters:encoding_:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithAuthScheme:(NSString *)authScheme __attribute__((swift_name("init(authScheme:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString * _Nullable)parameterName:(NSString *)name __attribute__((swift_name("parameter(name:)")));
- (NSString *)render __attribute__((swift_name("render()")));
- (NSString *)renderEncoding:(Mobile_multiplatformKtor_httpHeaderValueEncoding *)encoding __attribute__((swift_name("render(encoding:)")));
- (Mobile_multiplatformKtor_httpHttpAuthHeaderParameterized *)withParameterName:(NSString *)name value:(NSString *)value __attribute__((swift_name("withParameter(name:value:)")));
- (Mobile_multiplatformKtor_httpHttpAuthHeaderParameterized *)withReplacedParameterName:(NSString *)name value:(NSString *)value __attribute__((swift_name("withReplacedParameter(name:value:)")));
@property (readonly) Mobile_multiplatformKtor_httpHeaderValueEncoding *encoding __attribute__((swift_name("encoding")));
@property (readonly) NSArray<Mobile_multiplatformKtor_httpHeaderValueParam *> *parameters __attribute__((swift_name("parameters")));
@end;

__attribute__((swift_name("Ktor_ioCharset")))
@interface Mobile_multiplatformKtor_ioCharset : Mobile_multiplatformBase
- (instancetype)initWith_name:(NSString *)_name __attribute__((swift_name("init(_name:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_ioCharsetCompanion *companion __attribute__((swift_name("companion")));
- (Mobile_multiplatformKtor_ioCharsetDecoder *)doNewDecoder __attribute__((swift_name("doNewDecoder()")));
- (Mobile_multiplatformKtor_ioCharsetEncoder *)doNewEncoder __attribute__((swift_name("doNewEncoder()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate.Companion")))
@interface Mobile_multiplatformKtor_utilsGMTDateCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_utilsGMTDateCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) Mobile_multiplatformKtor_utilsGMTDate *START __attribute__((swift_name("START")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay")))
@interface Mobile_multiplatformKtor_utilsWeekDay : Mobile_multiplatformKotlinEnum<Mobile_multiplatformKtor_utilsWeekDay *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_utilsWeekDayCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) Mobile_multiplatformKtor_utilsWeekDay *monday __attribute__((swift_name("monday")));
@property (class, readonly) Mobile_multiplatformKtor_utilsWeekDay *tuesday __attribute__((swift_name("tuesday")));
@property (class, readonly) Mobile_multiplatformKtor_utilsWeekDay *wednesday __attribute__((swift_name("wednesday")));
@property (class, readonly) Mobile_multiplatformKtor_utilsWeekDay *thursday __attribute__((swift_name("thursday")));
@property (class, readonly) Mobile_multiplatformKtor_utilsWeekDay *friday __attribute__((swift_name("friday")));
@property (class, readonly) Mobile_multiplatformKtor_utilsWeekDay *saturday __attribute__((swift_name("saturday")));
@property (class, readonly) Mobile_multiplatformKtor_utilsWeekDay *sunday __attribute__((swift_name("sunday")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_utilsWeekDay *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth")))
@interface Mobile_multiplatformKtor_utilsMonth : Mobile_multiplatformKotlinEnum<Mobile_multiplatformKtor_utilsMonth *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_utilsMonthCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *january __attribute__((swift_name("january")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *february __attribute__((swift_name("february")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *march __attribute__((swift_name("march")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *april __attribute__((swift_name("april")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *may __attribute__((swift_name("may")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *june __attribute__((swift_name("june")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *july __attribute__((swift_name("july")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *august __attribute__((swift_name("august")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *september __attribute__((swift_name("september")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *october __attribute__((swift_name("october")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *november __attribute__((swift_name("november")));
@property (class, readonly) Mobile_multiplatformKtor_utilsMonth *december __attribute__((swift_name("december")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_utilsMonth *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode.Companion")))
@interface Mobile_multiplatformKtor_httpHttpStatusCodeCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpHttpStatusCodeCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_httpHttpStatusCode *)fromValueValue:(int32_t)value __attribute__((swift_name("fromValue(value:)")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Accepted __attribute__((swift_name("Accepted")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *BadGateway __attribute__((swift_name("BadGateway")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *BadRequest __attribute__((swift_name("BadRequest")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Conflict __attribute__((swift_name("Conflict")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Continue __attribute__((swift_name("Continue")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Created __attribute__((swift_name("Created")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *ExpectationFailed __attribute__((swift_name("ExpectationFailed")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *FailedDependency __attribute__((swift_name("FailedDependency")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Forbidden __attribute__((swift_name("Forbidden")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Found __attribute__((swift_name("Found")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *GatewayTimeout __attribute__((swift_name("GatewayTimeout")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Gone __attribute__((swift_name("Gone")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *InsufficientStorage __attribute__((swift_name("InsufficientStorage")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *InternalServerError __attribute__((swift_name("InternalServerError")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *LengthRequired __attribute__((swift_name("LengthRequired")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Locked __attribute__((swift_name("Locked")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *MethodNotAllowed __attribute__((swift_name("MethodNotAllowed")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *MovedPermanently __attribute__((swift_name("MovedPermanently")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *MultiStatus __attribute__((swift_name("MultiStatus")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *MultipleChoices __attribute__((swift_name("MultipleChoices")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *NoContent __attribute__((swift_name("NoContent")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *NonAuthoritativeInformation __attribute__((swift_name("NonAuthoritativeInformation")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *NotAcceptable __attribute__((swift_name("NotAcceptable")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *NotFound __attribute__((swift_name("NotFound")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *NotImplemented __attribute__((swift_name("NotImplemented")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *NotModified __attribute__((swift_name("NotModified")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *OK __attribute__((swift_name("OK")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *PartialContent __attribute__((swift_name("PartialContent")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *PayloadTooLarge __attribute__((swift_name("PayloadTooLarge")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *PaymentRequired __attribute__((swift_name("PaymentRequired")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *PermanentRedirect __attribute__((swift_name("PermanentRedirect")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *PreconditionFailed __attribute__((swift_name("PreconditionFailed")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Processing __attribute__((swift_name("Processing")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *ProxyAuthenticationRequired __attribute__((swift_name("ProxyAuthenticationRequired")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *RequestHeaderFieldTooLarge __attribute__((swift_name("RequestHeaderFieldTooLarge")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *RequestTimeout __attribute__((swift_name("RequestTimeout")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *RequestURITooLong __attribute__((swift_name("RequestURITooLong")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *RequestedRangeNotSatisfiable __attribute__((swift_name("RequestedRangeNotSatisfiable")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *ResetContent __attribute__((swift_name("ResetContent")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *SeeOther __attribute__((swift_name("SeeOther")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *ServiceUnavailable __attribute__((swift_name("ServiceUnavailable")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *SwitchProxy __attribute__((swift_name("SwitchProxy")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *SwitchingProtocols __attribute__((swift_name("SwitchingProtocols")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *TemporaryRedirect __attribute__((swift_name("TemporaryRedirect")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *TooManyRequests __attribute__((swift_name("TooManyRequests")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *Unauthorized __attribute__((swift_name("Unauthorized")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *UnprocessableEntity __attribute__((swift_name("UnprocessableEntity")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *UnsupportedMediaType __attribute__((swift_name("UnsupportedMediaType")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *UpgradeRequired __attribute__((swift_name("UpgradeRequired")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *UseProxy __attribute__((swift_name("UseProxy")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *VariantAlsoNegotiates __attribute__((swift_name("VariantAlsoNegotiates")));
@property (readonly) Mobile_multiplatformKtor_httpHttpStatusCode *VersionNotSupported __attribute__((swift_name("VersionNotSupported")));
@property (readonly) NSArray<Mobile_multiplatformKtor_httpHttpStatusCode *> *allStatusCodes __attribute__((swift_name("allStatusCodes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion.Companion")))
@interface Mobile_multiplatformKtor_httpHttpProtocolVersionCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpHttpProtocolVersionCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_httpHttpProtocolVersion *)fromValueName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("fromValue(name:major:minor:)")));
- (Mobile_multiplatformKtor_httpHttpProtocolVersion *)parseValue:(id)value __attribute__((swift_name("parse(value:)")));
@property (readonly) Mobile_multiplatformKtor_httpHttpProtocolVersion *HTTP_1_0 __attribute__((swift_name("HTTP_1_0")));
@property (readonly) Mobile_multiplatformKtor_httpHttpProtocolVersion *HTTP_1_1 __attribute__((swift_name("HTTP_1_1")));
@property (readonly) Mobile_multiplatformKtor_httpHttpProtocolVersion *HTTP_2_0 __attribute__((swift_name("HTTP_2_0")));
@property (readonly) Mobile_multiplatformKtor_httpHttpProtocolVersion *QUIC __attribute__((swift_name("QUIC")));
@property (readonly) Mobile_multiplatformKtor_httpHttpProtocolVersion *SPDY_3 __attribute__((swift_name("SPDY_3")));
@end;

__attribute__((swift_name("Ktor_httpParameters")))
@protocol Mobile_multiplatformKtor_httpParameters <Mobile_multiplatformKtor_utilsStringValues>
@required
@property (readonly) Mobile_multiplatformKtor_httpUrlEncodingOption *urlEncodingOption __attribute__((swift_name("urlEncodingOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl.Companion")))
@interface Mobile_multiplatformKtor_httpUrlCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpUrlCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Ktor_httpHeaderValueWithParameters")))
@interface Mobile_multiplatformKtor_httpHeaderValueWithParameters : Mobile_multiplatformBase
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<Mobile_multiplatformKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpHeaderValueWithParametersCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)parameterName:(NSString *)name __attribute__((swift_name("parameter(name:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSArray<Mobile_multiplatformKtor_httpHeaderValueParam *> *parameters __attribute__((swift_name("parameters")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType")))
@interface Mobile_multiplatformKtor_httpContentType : Mobile_multiplatformKtor_httpHeaderValueWithParameters
- (instancetype)initWithContentType:(NSString *)contentType contentSubtype:(NSString *)contentSubtype parameters:(NSArray<Mobile_multiplatformKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(contentType:contentSubtype:parameters:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<Mobile_multiplatformKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) Mobile_multiplatformKtor_httpContentTypeCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)matchPattern:(Mobile_multiplatformKtor_httpContentType *)pattern __attribute__((swift_name("match(pattern:)")));
- (BOOL)matchPattern_:(NSString *)pattern __attribute__((swift_name("match(pattern_:)")));
- (Mobile_multiplatformKtor_httpContentType *)withParameterName:(NSString *)name value:(NSString *)value __attribute__((swift_name("withParameter(name:value:)")));
- (Mobile_multiplatformKtor_httpContentType *)withoutParameters __attribute__((swift_name("withoutParameters()")));
@property (readonly) NSString *contentSubtype __attribute__((swift_name("contentSubtype")));
@property (readonly) NSString *contentType __attribute__((swift_name("contentType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol.Companion")))
@interface Mobile_multiplatformKtor_httpURLProtocolCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpURLProtocolCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_httpURLProtocol *)createOrDefaultName:(NSString *)name __attribute__((swift_name("createOrDefault(name:)")));
@property (readonly) Mobile_multiplatformKtor_httpURLProtocol *HTTP __attribute__((swift_name("HTTP")));
@property (readonly) Mobile_multiplatformKtor_httpURLProtocol *HTTPS __attribute__((swift_name("HTTPS")));
@property (readonly) Mobile_multiplatformKtor_httpURLProtocol *SOCKS __attribute__((swift_name("SOCKS")));
@property (readonly) Mobile_multiplatformKtor_httpURLProtocol *WS __attribute__((swift_name("WS")));
@property (readonly) Mobile_multiplatformKtor_httpURLProtocol *WSS __attribute__((swift_name("WSS")));
@property (readonly) NSDictionary<NSString *, Mobile_multiplatformKtor_httpURLProtocol *> *byName __attribute__((swift_name("byName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrlEncodingOption")))
@interface Mobile_multiplatformKtor_httpUrlEncodingOption : Mobile_multiplatformKotlinEnum<Mobile_multiplatformKtor_httpUrlEncodingOption *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Mobile_multiplatformKtor_httpUrlEncodingOption *default_ __attribute__((swift_name("default_")));
@property (class, readonly) Mobile_multiplatformKtor_httpUrlEncodingOption *keyOnly __attribute__((swift_name("keyOnly")));
@property (class, readonly) Mobile_multiplatformKtor_httpUrlEncodingOption *valueOnly __attribute__((swift_name("valueOnly")));
@property (class, readonly) Mobile_multiplatformKtor_httpUrlEncodingOption *noEncoding __attribute__((swift_name("noEncoding")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformKtor_httpUrlEncodingOption *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreParentJob")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreParentJob <Mobile_multiplatformKotlinx_coroutines_coreJob>
@required
- (Mobile_multiplatformKotlinCancellationException *)getChildJobCancellationCause __attribute__((swift_name("getChildJobCancellationCause()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectInstance")))
@protocol Mobile_multiplatformKotlinx_coroutines_coreSelectInstance
@required
- (void)disposeOnSelectHandle:(id<Mobile_multiplatformKotlinx_coroutines_coreDisposableHandle>)handle __attribute__((swift_name("disposeOnSelect(handle:)")));
- (id _Nullable)performAtomicTrySelectDesc:(Mobile_multiplatformKotlinx_coroutines_coreAtomicDesc *)desc __attribute__((swift_name("performAtomicTrySelect(desc:)")));
- (void)resumeSelectWithExceptionException:(Mobile_multiplatformKotlinThrowable *)exception __attribute__((swift_name("resumeSelectWithException(exception:)")));
- (BOOL)trySelect __attribute__((swift_name("trySelect()")));
- (id _Nullable)trySelectOtherOtherOp:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp * _Nullable)otherOp __attribute__((swift_name("trySelectOther(otherOp:)")));
@property (readonly) id<Mobile_multiplatformKotlinContinuation> completion __attribute__((swift_name("completion")));
@property (readonly) BOOL isSelected __attribute__((swift_name("isSelected")));
@end;

__attribute__((swift_name("KotlinSuspendFunction0")))
@protocol Mobile_multiplatformKotlinSuspendFunction0 <Mobile_multiplatformKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeWithCompletionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(completionHandler:)")));
@end;

__attribute__((swift_name("KotlinCharIterator")))
@interface Mobile_multiplatformKotlinCharIterator : Mobile_multiplatformBase <Mobile_multiplatformKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id)next __attribute__((swift_name("next()")));
- (unichar)nextChar __attribute__((swift_name("nextChar()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection")))
@interface Mobile_multiplatformKotlinKTypeProjection : Mobile_multiplatformBase
- (instancetype)initWithVariance:(Mobile_multiplatformKotlinKVariance * _Nullable)variance type:(id<Mobile_multiplatformKotlinKType> _Nullable)type __attribute__((swift_name("init(variance:type:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) Mobile_multiplatformKotlinKTypeProjectionCompanion *companion __attribute__((swift_name("companion")));
- (Mobile_multiplatformKotlinKVariance * _Nullable)component1 __attribute__((swift_name("component1()")));
- (id<Mobile_multiplatformKotlinKType> _Nullable)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformKotlinKTypeProjection *)doCopyVariance:(Mobile_multiplatformKotlinKVariance * _Nullable)variance type:(id<Mobile_multiplatformKotlinKType> _Nullable)type __attribute__((swift_name("doCopy(variance:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<Mobile_multiplatformKotlinKType> _Nullable type __attribute__((swift_name("type")));
@property (readonly) Mobile_multiplatformKotlinKVariance * _Nullable variance __attribute__((swift_name("variance")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueParam")))
@interface Mobile_multiplatformKtor_httpHeaderValueParam : Mobile_multiplatformBase
- (instancetype)initWithName:(NSString *)name value:(NSString *)value __attribute__((swift_name("init(name:value:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (Mobile_multiplatformKtor_httpHeaderValueParam *)doCopyName:(NSString *)name value:(NSString *)value __attribute__((swift_name("doCopy(name:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioCharset.Companion")))
@interface Mobile_multiplatformKtor_ioCharsetCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_ioCharsetCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_ioCharset *)forNameName:(NSString *)name __attribute__((swift_name("forName(name:)")));
@end;

__attribute__((swift_name("Ktor_ioCharsetDecoder")))
@interface Mobile_multiplatformKtor_ioCharsetDecoder : Mobile_multiplatformBase
- (instancetype)initWith_charset:(Mobile_multiplatformKtor_ioCharset *)_charset __attribute__((swift_name("init(_charset:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Ktor_ioCharsetEncoder")))
@interface Mobile_multiplatformKtor_ioCharsetEncoder : Mobile_multiplatformBase
- (instancetype)initWith_charset:(Mobile_multiplatformKtor_ioCharset *)_charset __attribute__((swift_name("init(_charset:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay.Companion")))
@interface Mobile_multiplatformKtor_utilsWeekDayCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_utilsWeekDayCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_utilsWeekDay *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (Mobile_multiplatformKtor_utilsWeekDay *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth.Companion")))
@interface Mobile_multiplatformKtor_utilsMonthCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_utilsMonthCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_utilsMonth *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (Mobile_multiplatformKtor_utilsMonth *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueWithParameters.Companion")))
@interface Mobile_multiplatformKtor_httpHeaderValueWithParametersCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpHeaderValueWithParametersCompanion *shared __attribute__((swift_name("shared")));
- (id _Nullable)parseValue:(NSString *)value init:(id _Nullable (^)(NSString *, NSArray<Mobile_multiplatformKtor_httpHeaderValueParam *> *))init __attribute__((swift_name("parse(value:init:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType.Companion")))
@interface Mobile_multiplatformKtor_httpContentTypeCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKtor_httpContentTypeCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKtor_httpContentType *)parseValue:(NSString *)value __attribute__((swift_name("parse(value:)")));
@property (readonly) Mobile_multiplatformKtor_httpContentType *Any __attribute__((swift_name("Any")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicDesc")))
@interface Mobile_multiplatformKotlinx_coroutines_coreAtomicDesc : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)prepareOp:(Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
@property Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreOpDescriptor")))
@interface Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)isEarlierThanThat:(Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor *)that __attribute__((swift_name("isEarlierThan(that:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> * _Nullable atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.PrepareOp")))
@interface Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp : Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor
- (instancetype)initWithAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)next desc:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *)desc __attribute__((swift_name("init(affected:next:desc:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishPrepare __attribute__((swift_name("finishPrepare()")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *affected __attribute__((swift_name("affected")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *desc __attribute__((swift_name("desc")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *next __attribute__((swift_name("next")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKVariance")))
@interface Mobile_multiplatformKotlinKVariance : Mobile_multiplatformKotlinEnum<Mobile_multiplatformKotlinKVariance *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) Mobile_multiplatformKotlinKVariance *invariant __attribute__((swift_name("invariant")));
@property (class, readonly) Mobile_multiplatformKotlinKVariance *in __attribute__((swift_name("in")));
@property (class, readonly) Mobile_multiplatformKotlinKVariance *out __attribute__((swift_name("out")));
+ (Mobile_multiplatformKotlinArray<Mobile_multiplatformKotlinKVariance *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection.Companion")))
@interface Mobile_multiplatformKotlinKTypeProjectionCompanion : Mobile_multiplatformBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) Mobile_multiplatformKotlinKTypeProjectionCompanion *shared __attribute__((swift_name("shared")));
- (Mobile_multiplatformKotlinKTypeProjection *)contravariantType:(id<Mobile_multiplatformKotlinKType>)type __attribute__((swift_name("contravariant(type:)")));
- (Mobile_multiplatformKotlinKTypeProjection *)covariantType:(id<Mobile_multiplatformKotlinKType>)type __attribute__((swift_name("covariant(type:)")));
- (Mobile_multiplatformKotlinKTypeProjection *)invariantType:(id<Mobile_multiplatformKotlinKType>)type __attribute__((swift_name("invariant(type:)")));
@property (readonly) Mobile_multiplatformKotlinKTypeProjection *STAR __attribute__((swift_name("STAR")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicOp")))
@interface Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<__contravariant T> : Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeAffected:(T _Nullable)affected failure:(id _Nullable)failure __attribute__((swift_name("complete(affected:failure:)")));
- (id _Nullable)decideDecision:(id _Nullable)decision __attribute__((swift_name("decide(decision:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (id _Nullable)prepareAffected:(T _Nullable)affected __attribute__((swift_name("prepare(affected:)")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) id _Nullable consensus __attribute__((swift_name("consensus")));
@property (readonly) BOOL isDecided __attribute__((swift_name("isDecided")));
@property (readonly) int64_t opSequence __attribute__((swift_name("opSequence")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode")))
@interface Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode : Mobile_multiplatformBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addLastNode:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addLast(node:)")));
- (BOOL)addLastIfNode:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)node condition:(Mobile_multiplatformBoolean *(^)(void))condition __attribute__((swift_name("addLastIf(node:condition:)")));
- (BOOL)addLastIfPrevNode:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(Mobile_multiplatformBoolean *(^)(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate __attribute__((swift_name("addLastIfPrev(node:predicate:)")));
- (BOOL)addLastIfPrevAndIfNode:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(Mobile_multiplatformBoolean *(^)(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate condition:(Mobile_multiplatformBoolean *(^)(void))condition __attribute__((swift_name("addLastIfPrevAndIf(node:predicate:condition:)")));
- (BOOL)addOneIfEmptyNode:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addOneIfEmpty(node:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeAddLastNode:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("describeAddLast(node:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeRemoveFirst __attribute__((swift_name("describeRemoveFirst()")));
- (void)helpRemove __attribute__((swift_name("helpRemove()")));
- (Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)nextIfRemoved __attribute__((swift_name("nextIfRemoved()")));
- (BOOL)remove __attribute__((swift_name("remove()")));
- (id _Nullable)removeFirstIfIsInstanceOfOrPeekIfPredicate:(Mobile_multiplatformBoolean *(^)(id _Nullable))predicate __attribute__((swift_name("removeFirstIfIsInstanceOfOrPeekIf(predicate:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)removeFirstOrNull __attribute__((swift_name("removeFirstOrNull()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isRemoved __attribute__((swift_name("isRemoved")));
@property (readonly, getter=next_) id next __attribute__((swift_name("next")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *nextNode __attribute__((swift_name("nextNode")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *prevNode __attribute__((swift_name("prevNode")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.AbstractAtomicDesc")))
@interface Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc : Mobile_multiplatformKotlinx_coroutines_coreAtomicDesc
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)failureAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (id _Nullable)onPreparePrepareOp:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("onPrepare(prepareOp:)")));
- (void)onRemovedAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("onRemoved(affected:)")));
- (id _Nullable)prepareOp:(Mobile_multiplatformKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
- (BOOL)retryAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc")))
@interface Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T> : Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)queue node:(T)node __attribute__((swift_name("init(queue:node:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishOnSuccessAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) T node __attribute__((swift_name("node")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *originalNext __attribute__((swift_name("originalNext")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc")))
@interface Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T> : Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)queue __attribute__((swift_name("init(queue:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (id _Nullable)failureAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(Mobile_multiplatformKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@property (readonly) Mobile_multiplatformKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@property (readonly) T _Nullable result __attribute__((swift_name("result")));
@end;

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
