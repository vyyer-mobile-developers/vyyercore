#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class Scan_processingKotlinByteArray, Scan_processingKotlinArray<T>, Scan_processingUsdlBarcode, Scan_processingKotlinByteIterator;

@protocol Scan_processingParcelable, Scan_processingKotlinIterator;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

__attribute__((swift_name("KotlinBase")))
@interface Scan_processingBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface Scan_processingBase (Scan_processingBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface Scan_processingMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface Scan_processingMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorScan_processingKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface Scan_processingNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface Scan_processingByte : Scan_processingNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface Scan_processingUByte : Scan_processingNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface Scan_processingShort : Scan_processingNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface Scan_processingUShort : Scan_processingNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface Scan_processingInt : Scan_processingNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface Scan_processingUInt : Scan_processingNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface Scan_processingLong : Scan_processingNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface Scan_processingULong : Scan_processingNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface Scan_processingFloat : Scan_processingNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface Scan_processingDouble : Scan_processingNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface Scan_processingBoolean : Scan_processingNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DateFormatter")))
@interface Scan_processingDateFormatter : Scan_processingBase
- (instancetype)initWithFormat:(NSString *)format __attribute__((swift_name("init(format:)"))) __attribute__((objc_designated_initializer));
- (BOOL)isADateSource:(NSString *)source __attribute__((swift_name("isADate(source:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DateFormatter.Companion")))
@interface Scan_processingDateFormatterCompanion : Scan_processingBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) NSString *dayTwoDigits __attribute__((swift_name("dayTwoDigits")));
@property (readonly) NSString *monthTwoDigits __attribute__((swift_name("monthTwoDigits")));
@property (readonly) NSString *yearFourDigits __attribute__((swift_name("yearFourDigits")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FormatUtils")))
@interface Scan_processingFormatUtils : Scan_processingBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)formatUtils __attribute__((swift_name("init()")));
- (NSString *)capitalizeWordsSource:(NSString *)source __attribute__((swift_name("capitalizeWords(source:)")));
- (NSString *)capitalizeWordsLongerThan2CharsSource:(NSString *)source __attribute__((swift_name("capitalizeWordsLongerThan2Chars(source:)")));
- (NSString *)formatAddressAddressOrDash:(NSString *)addressOrDash city:(NSString *)city stateShort:(NSString *)stateShort zip:(NSString *)zip __attribute__((swift_name("formatAddress(addressOrDash:city:stateShort:zip:)")));
- (NSString *)reformatDateSourceDateOfBirth:(NSString * _Nullable)sourceDateOfBirth __attribute__((swift_name("reformatDate(sourceDateOfBirth:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HashCalculator")))
@interface Scan_processingHashCalculator : Scan_processingBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)hashCalculator __attribute__((swift_name("init()")));
- (NSString *)sha256Source:(NSString *)source __attribute__((swift_name("sha256(source:)")));
@end;

__attribute__((swift_name("Parcelable")))
@protocol Scan_processingParcelable
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsdlBarcode")))
@interface Scan_processingUsdlBarcode : Scan_processingBase <Scan_processingParcelable>
- (instancetype)initWithRawData:(Scan_processingKotlinByteArray *)rawData fullName:(NSString *)fullName nameParts:(Scan_processingKotlinArray<NSString *> *)nameParts licenseNumber:(NSString *)licenseNumber address:(NSString *)address gender:(NSString *)gender eyes:(NSString *)eyes height:(NSString *)height heightRaw:(NSString *)heightRaw __attribute__((swift_name("init(rawData:fullName:nameParts:licenseNumber:address:gender:eyes:height:heightRaw:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@property (readonly) NSString *eyes __attribute__((swift_name("eyes")));
@property (readonly) NSString *fullName __attribute__((swift_name("fullName")));
@property (readonly) NSString *gender __attribute__((swift_name("gender")));
@property (readonly) NSString *height __attribute__((swift_name("height")));
@property (readonly) NSString *heightRaw __attribute__((swift_name("heightRaw")));
@property (readonly) NSString *licenseNumber __attribute__((swift_name("licenseNumber")));
@property (readonly) Scan_processingKotlinArray<NSString *> *nameParts __attribute__((swift_name("nameParts")));
@property (readonly) Scan_processingKotlinByteArray *rawData __attribute__((swift_name("rawData")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsdlBarcode.Companion")))
@interface Scan_processingUsdlBarcodeCompanion : Scan_processingBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (Scan_processingUsdlBarcode *)fromByteArrayRawData:(Scan_processingKotlinByteArray *)rawData __attribute__((swift_name("fromByteArray(rawData:)")));
@property (readonly) NSString *EMPTY_VALUE __attribute__((swift_name("EMPTY_VALUE")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsdlObfuscator")))
@interface Scan_processingUsdlObfuscator : Scan_processingBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)usdlObfuscator __attribute__((swift_name("init()")));
- (Scan_processingKotlinByteArray *)obfuscateRawBarcodeDataArray:(Scan_processingKotlinByteArray *)rawBarcodeDataArray __attribute__((swift_name("obfuscate(rawBarcodeDataArray:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsdlObfuscator.DataForRule")))
@interface Scan_processingUsdlObfuscatorDataForRule : Scan_processingBase
- (instancetype)initWithData:(NSString *)data obfuscated:(NSString *)obfuscated __attribute__((swift_name("init(data:obfuscated:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *data __attribute__((swift_name("data")));
@property NSString *obfuscated __attribute__((swift_name("obfuscated")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UsdlUIDCalculator")))
@interface Scan_processingUsdlUIDCalculator : Scan_processingBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)usdlUIDCalculator __attribute__((swift_name("init()")));
- (NSString *)calculateUsdlBarcode:(Scan_processingUsdlBarcode *)usdlBarcode __attribute__((swift_name("calculate(usdlBarcode:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StringUtilsKt")))
@interface Scan_processingStringUtilsKt : Scan_processingBase
+ (NSString *)capitalizeWords:(NSString *)receiver __attribute__((swift_name("capitalizeWords(_:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CharUtilsKt")))
@interface Scan_processingCharUtilsKt : Scan_processingBase
+ (BOOL)isDigit:(unichar)receiver __attribute__((swift_name("isDigit(_:)")));
+ (BOOL)isLetter:(unichar)receiver __attribute__((swift_name("isLetter(_:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface Scan_processingKotlinByteArray : Scan_processingBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(Scan_processingByte *(^)(Scan_processingInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (Scan_processingKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface Scan_processingKotlinArray<T> : Scan_processingBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(Scan_processingInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<Scan_processingKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol Scan_processingKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("KotlinByteIterator")))
@interface Scan_processingKotlinByteIterator : Scan_processingBase <Scan_processingKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (Scan_processingByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end;

#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
